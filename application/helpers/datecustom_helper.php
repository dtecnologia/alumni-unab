<?php
/**
 * 
 * Cambia el formato de la fecha (origen) y lo transforma al nuevo formato.
 * 
 * @param type $source valos de la fecha
 * @param type $format formato con la que viene la fecha
 * @param type $newFormat transformar la fecha en el nuevo formato
 * @return devuelve falso si no tiene valor $source, si esta bien, devuelve la fecha en el formato especificado en la variable $newFormat
 */
function change_format_date($source = NULL, $format='yyyy/mm/dd', $newFormat = 'dd/mm/yyyy'){
    if(!empty($source)){
        $myDateTime = DateTime::createFromFormat($format, $source);
        return $myDateTime->format($newFormat);
    }
    return false;
}

