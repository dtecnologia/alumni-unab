<?php
class Tipo_cargo_model extends CI_Model {

    private $idtipocargo;
    private $nombre;
        
    private $tabla = 'tipo_cargo';
    
    function __construct(){
        parent::__construct();
    }
    
    /* GETTER */
    function getIdTipoCargo(){    return $this->idtipocargo;    }
    function getNombre(){   return $this->nombre;   }

    /* SETTER */
    function setIdTipoCargo($in){ $this->idtipocargo = $in; }
    function setNombre($in){    $this->nombre = $in;    }
    
    /* inicializar la clase */
    function initialize($data = array()){
        if(!empty($data)){
            $this->idtipocargo = (!empty($data["idtipocargo"])) ? $data["idtipocargo"] : NULL;
            $this->nombre = (!empty($data["nombre"])) ? $data["nombre"] : NULL;
         }
    }
    
    /* OBTIENE USUARIOS SEGÚN LO DATOS DE LA CLASE */
    function get($return = false, $datos = NULL){
        
        if(!empty($datos) && is_array($datos))
            $this->initialize($datos);
            
        if(!empty($this->idtipocargo))
            $this->db->where('idtipocargo', $this->idtipocargo);
        
        if(!empty($this->nombre))
            $this->db->where('nombre', $this->nombre);
        
        $query = $this->db->get($this->tabla);
        if ($query->num_rows() > 0){
            $registros = array();
            foreach ($query->result_array() as $row){
                switch ($return){
                    case 'all':
                    case 'object':
                        $tmp = new Tipo_cargo_model();
                        $tmp->idtipocargo = $row["idtipocargo"];
                        $tmp->nombre = $row["nombre"];
                        if($return === 'object')
                            return $tmp;
                        $registros[] = $tmp;
                    break;
        
                    
                    case 'id':
                        return $row["idtipocargo"];
                    break;
                    
                    case 'boolean':
                        return true;
                    break;
                    
                    default:
                        $this->idtipocargo = $row["idtipocargo"];
                        $this->nombre = $row["nombre"];
                        return true;
                    break;
                    
                }
           }
           return $registros;
        }else{
            #echo $this->db->last_query();
            return false;
        }
    }
    
    /* INSERTAR UN USUARIO A LA BD, RETORNA EL ID DEL USUARIO INSERTADO; EN CASO DE NO HABER INSERTADO ID = 0 */
    function insert($datos = array()){
        #SIN ID USUARIO.
        if(!empty($datos)){
            $data["nombre"] = (!empty($datos["nombre"])) ? $datos["nombre"] : NULL;
        }else{
            return false;
        }
        $this->db->insert($this->tabla, $data); 
        return $this->db->insert_id();
    }
    
    function update($values = array()){
        if(!empty($values)){
            
            if(!empty($values["nombre"]))
                $this->db->set('nombre', $values["nombre"]);    
                
            if(!empty($this->idtipocargo))
                $this->db->where('idtipocargo', $this->idtipocargo); 
            else
                $this->db->where('idtipocargo', $values["idtipocargo"]);
            
            return $this->db->update($this->tabla);
        }else
            return false;
        
    }

    function delete($idtipocargo = NULL){
        if(empty($idtipocargo))
            $idtipocargo = $this->idtipocargo;
            
        if(!empty($idtipocargo)){           
        	$this->db->where('idtipocargo', $idtipocargo);
            if($this->db->delete($this->tabla))
                return true;
            else
                return false;
        }else
            return false;
    }

}