<?php
class Tipo_carrera_model extends CI_Model {

    private $idtipocarrera;
    private $nombre;
        
    private $tabla = 'tipo_carreras';
    
    function __construct(){
        parent::__construct();
    }
    
    /* GETTER */
    function getIdTipoCarrera(){    return $this->idtipocarrera;    }
    function getNombre(){   return $this->nombre;   }

    /* SETTER */
    function setIdTipoCarrera($in){ $this->idtipocarrera = $in; }
    function setNombre($in){    $this->nombre = $in;    }
    
    /* inicializar la clase */
    function initialize($data = array()){
        if(!empty($data)){
            $this->idtipocarrera = (!empty($data["idtipocarrera"])) ? $data["idtipocarrera"] : NULL;
            $this->nombre = (!empty($data["nombre"])) ? $data["nombre"] : NULL;
         }
    }
    
    /* OBTIENE USUARIOS SEGÚN LO DATOS DE LA CLASE */
    function get($return = false, $datos = NULL){
        
        if(!empty($datos) && is_array($datos))
            $this->initialize($datos);
            
        if(!empty($this->idtipocarrera))
            $this->db->where('idtipocarrera', $this->idtipocarrera);
        
        if(!empty($this->nombre))
            $this->db->where('nombre', $this->nombre);
        
        $query = $this->db->get($this->tabla);
        if ($query->num_rows() > 0){
            $registros = array();
            foreach ($query->result_array() as $row){
                switch ($return){
                    case 'all':
                    case 'object':
                        $tmp = new Tipo_carrera_model();
                        $tmp->idtipocarrera = $row["idtipocarrera"];
                        $tmp->nombre = $row["nombre"];
                        if($return === 'object')
                            return $tmp;
                        $registros[] = $tmp;
                    break;
        
                    
                    case 'id':
                        return $row["idtipocarrera"];
                    break;
                    
                    case 'boolean':
                        return true;
                    break;
                    
                    default:
                        $this->idtipocarrera = $row["idtipocarrera"];
                        $this->nombre = $row["nombre"];
                        return true;
                    break;
                    
                }
           }
           return $registros;
        }else{
            #echo $this->db->last_query();
            return false;
        }
    }
    
    /* INSERTAR UN USUARIO A LA BD, RETORNA EL ID DEL USUARIO INSERTADO; EN CASO DE NO HABER INSERTADO ID = 0 */
    function insert($datos = array()){
        #SIN ID USUARIO.
        if(!empty($datos)){
            $data["nombre"] = (!empty($datos["nombre"])) ? $datos["nombre"] : NULL;
        }else{
            return false;
        }
        $this->db->insert($this->tabla, $data); 
        return $this->db->insert_id();
    }
    
    function update($values = array()){
        if(!empty($values)){
            
            if(!empty($values["nombre"]))
                $this->db->set('nombre', $values["nombre"]);    
                
            if(!empty($this->idtipocarrera))
                $this->db->where('idtipocarrera', $this->idtipocarrera); 
            else
                $this->db->where('idtipocarrera', $values["idtipocarrera"]);
            
            return $this->db->update($this->tabla);
        }else
            return false;
        
    }

    function delete($idtipocarrera = NULL){
        if(empty($idtipocarrera))
            $idtipocarrera = $this->idtipocarrera;
            
        if(!empty($idtipocarrera)){           
        	$this->db->where('idtipocarrera', $idtipocarrera);
            if($this->db->delete($this->tabla))
                return true;
            else
                return false;
        }else
            return false;
    }

}