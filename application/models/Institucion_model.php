<?php
class Institucion_model extends CI_Model {

    private $idinstitucion;
    private $idtipo_institucion;
    private $nombre;
    private $direccion;
    private $idcomuna;
        
    private $tabla = 'institucion';
    
    function __construct(){
        parent::__construct();
    }
    
    /* GETTER */
    function getIdInstitucion(){    return $this->idinstitucion;    }
    function getNombre(){   return $this->nombre;   }
    function getIdTipoInstitucion(){ return $this->idtipo_institucion; }
    function getDireccion(){ return $this->direccion; }
    function getIdComuna(){   return $this->idcomuna;   }
    /* SETTER */
    function setIdInstitucion($in){ $this->idinstitucion = $in; }
    function setNombre($in){    $this->nombre = $in;    }
    function setIdTipoInstitucion($in){  $this->idtipo_institucion = $in;  }
    function setDireccion($in){  $this->direccion = $in;  }
    function setIdComuna($in){    $this->idcomuna = $in;    }
    
    /* inicializar la clase */
    function initialize($data = array()){
        if(!empty($data)){
            $this->idinstitucion = (!empty($data["idinstitucion"])) ? $data["idinstitucion"] : NULL;
            $this->nombre = (!empty($data["nombre"])) ? $data["nombre"] : NULL;
            $this->idtipo_institucion = (!empty($data["idtipo_institucion"])) ? $data["idtipo_institucion"] : NULL;
            $this->direccion = (!empty($data["direccion"])) ? $data["direccion"] : NULL;
            $this->idcomuna = (is_numeric($data["idcomuna"])) ? $data["idcomuna"] : NULL;
         }
    }
    
    /* OBTIENE USUARIOS SEGÚN LO DATOS DE LA CLASE */
    function get($return = false, $datos = NULL){
        
        if(!empty($datos) && is_array($datos))
            $this->initialize($datos);
            
        if(!empty($this->idinstitucion))
            $this->db->where('idinstitucion', $this->idinstitucion);
        
        if(!empty($this->nombre))
            $this->db->where('nombre', $this->nombre);
        
        if(!empty($this->idtipo_institucion))
            $this->db->where('idtipo_institucion', $this->idtipo_institucion);
            
        if(!empty($this->direccion))
            $this->db->where('direccion', $this->direccion);
            
        if(is_numeric($this->idcomuna))
            $this->db->where('idcomuna', $this->idcomuna);
        
        
        $query = $this->db->get($this->tabla);
        if ($query->num_rows() > 0){
            $institcuion = array();
            foreach ($query->result_array() as $row){
                switch ($return){
                    case 'all':
                    case 'object':
                        $tmp = new Institucion_model();
                        $tmp->idinstitucion = $row["idinstitucion"];
                        $tmp->nombre = $row["nombre"];
                        $tmp->idtipo_institucion = $row["idtipo_institucion"];
                        $tmp->direccion = $row["direccion"];
                        $tmp->idcomuna = $row["idcomuna"];
                        if($return === 'object')
                            return $tmp;
                        $institucion[] = $tmp;
                    break;
        
                    
                    case 'id':
                        return $row["idinstitucion"];
                    break;
                    
                    case 'boolean':
                        return true;
                    break;
                    
                    default:
                        $this->idinstitucion = $row["idinstitucion"];
                        $this->nombre = $row["nombre"];
                        $this->idtipo_institucion = $row["idtipo_institucion"];
                        $this->direccion = $row["direccion"];
                        $this->idcomuna = $row["idcomuna"];
                        return true;
                    break;
                    
                }
           }
           return $institucion;
        }else{
            #echo $this->db->last_query();
            return false;
        }
    }
    
    /* INSERTAR UN USUARIO A LA BD, RETORNA EL ID DEL USUARIO INSERTADO; EN CASO DE NO HABER INSERTADO ID = 0 */
    function insert($datos = array()){
        #SIN ID USUARIO.
        if(!empty($datos)){
            $data["nombre"] = (!empty($datos["nombre"])) ? $datos["nombre"] : NULL;
            $data["idtipo_institucion"] = (!empty($datos["idtipo_institucion"])) ? $datos["idtipo_institucion"] : NULL;
            $data["direccion"] = (!empty($datos["direccion"])) ? $datos["direccion"] : NULL;
            $data["idcomuna"] = (!is_null($datos["idcomuna"])) ? $datos["idcomuna"] : NULL;
        }else{
            return false;
        }
        $this->db->insert($this->tabla, $data); 
        return $this->db->insert_id();
    }
    
    function update($values = array()){
        if(!empty($values)){
            
            if(!empty($values["idtipo_institucion"]))
                $this->db->set('idtipo_institucion', $values["idtipo_institucion"]);
            
            if(!empty($values["nombre"]))
                $this->db->set('nombre', $values["nombre"]);    
                
            if(!empty($values["direccion"]))
                $this->db->set('direccion', $values["direccion"]);
                
            if(!empty($values["idcomuna"]))       
                $this->db->set('idcomuna', $values["idcomuna"]);
            
            if(!empty($this->idinstitucion))
                $this->db->where('idinstitucion', $this->idinstitucion); 
            else
                $this->db->where('idinstitucion', $values["idinstitucion"]);
            
            return $this->db->update($this->tabla);
        }else
            return false;
        
    }

    function delete($idinstitucion = NULL){
        if(empty($idinstitucion))
            $idinstitucion = $this->idinstitucion;
            
        if(!empty($idinstitucion)){           
            $this->db->where('idinstitucion', $idinstitucion);
            if($this->db->delete($this->tabla))
                return true;
            else
                return false;
        }else
            return false;
    }

}