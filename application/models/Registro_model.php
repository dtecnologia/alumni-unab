<?php
class Registro_model extends CI_Model {

    private $idregistro;
    private $rut;
    private $nombre;
    private $nombre_completo;
    private $apaterno;
    private $amaterno;
    private $email_alumni;
    private $solicitado;
    private $solicitud_mail;
    private $estado;
    private $fechanacimiento;
    private $sexo;
    private $nacionalidad;
    private $direccion;
    private $idcomuna;
    private $postal;
    private $email;
    private $telefono;
    private $celular;
    private $password;
    private $fechaactivacion;
    private $fechaingreso;
    private $fechamodificacion;
    private $fecharegistro;
    private $fechasolicitud;
    private $fechaeliminacion;
    private $activo;
    private $registrado;
        
    private $tabla = 'registro';
    
    function __construct(){
        parent::__construct();
    }
    
    /* GETTER */
    function getIdRegistro(){    return $this->idregistro;    }
    function getNombre(){   return $this->nombre;   }
    function getNombreCompleto(){   return $this->nombre_completo;   }
    function getRut(){ return $this->rut; }
    function getApaterno(){ return $this->apaterno; }
    function getAmaterno(){ return $this->amaterno; }
    function getEmailAlumni(){   return $this->email_alumni;   }
    function getSolicitado(){   return $this->solicitado;   }
    function getSolicitudMail(){   return $this->solicitud_mail;   }
    function getEstado(){   return $this->estado;   }
    function getFechaNacimiento(){   return $this->fechanacimiento;   }
    function getSexo(){   return $this->sexo;   }
    function getNacionalidad(){   return $this->nacionalidad;   }
    function getDireccion(){   return $this->direccion;   }
    function getIdComuna(){   return $this->idcomuna;   }
    function getPostal(){   return $this->postal;   }
    function getEmail(){   return $this->email;   }
    function getTelefono(){   return $this->telefono;   }
    function getCelular(){   return $this->celular;   }
    function getPassword(){   return $this->password;   }
    function getFechaActivacion(){   return $this->fechaactivacion;   }
    function getFechaIngreso(){   return $this->fechaingreso;   }
    function getFechaModificacion(){   return $this->fechamodificacion;   }
    function getFechaRegistro(){   return $this->fecharegistro;   }
    function getFechaSolicitud(){   return $this->fechasolicitud;   }
    function getFechaEliminacion(){   return $this->fechaeliminacion;   }
    function getActivo(){   return $this->activo;   }
    function getRegistrado(){   return $this->registrado;   }
    

    /* SETTER */
    function setIdRegistro($in){ $this->idregistro = $in; }
    function setNombre($in){    $this->nombre = $in;    }
    function setNombreCompleto($in){    $this->nombre_completo = $in;    }
    function setRut($in){  $this->rut = $in;  }
    function setApaterno($in){  $this->apaterno = $in;  }
    function setAmaterno($in){    $this->amaterno = $in;    }
    function setEmailAlumni(){    $this->email_alumni;   }
    function setSolicitado(){    $this->solicitado;   }
    function setSolicitudMail(){    $this->solicitud_mail;   }
    function setEstado(){    $this->estado;   }
    function setFechaNacimiento(){    $this->fechanacimiento;   }
    function setSexo(){    $this->sexo;   }
    function setNacionalidad(){    $this->nacionalidad;   }
    function setDireccion(){    $this->direccion = $in;   }
    function setIdComuna(){    $this->idcomuna = $in;   }
    function setPostal(){    $this->postal = $in;   }
    function setEmail(){    $this->email = $in;   }
    function setTelefono(){    $this->telefono = $in;   }
    function setCelular(){    $this->celular = $in;   }
    function setPassword(){    $this->password = $in;   }
    function setFechaActivacion(){    $this->fechaactivacion = $in;   }
    function setFechaIngreso(){    $this->fechaingreso = $in;   }
    function setFechaModificacion(){    $this->fechamodificacion = $in;   }
    function setFechaRegistro(){    $this->fecharegistro = $in;   }
    function setFechaSolicitud(){    $this->fechasolicitud = $in;   }
    function setFechaEliminacion(){    $this->fechaeliminacion = $in;   }
    function setActivo(){    $this->activo = $in;   }
    function setRegistrado(){    $this->registrado = $in;   }
    
    /* inicializar la clase */
    function initialize($data = array()){
        if(!empty($data)){
            $this->idregistro = (!empty($data["idregistro"])) ? $data["idregistro"] : NULL;
            $this->nombre = (!empty($data["nombre"])) ? $data["nombre"] : NULL;
            $this->nombre_completo = (!empty($data["nombre_completo"])) ? $data["nombre_completo"] : NULL;
            $this->rut = (!empty($data["rut"])) ? $data["rut"] : NULL;
            $this->apaterno = (!empty($data["apaterno"])) ? $data["apaterno"] : NULL;
            $this->amaterno = (!empty($data["amaterno"])) ? $data["amaterno"] : NULL;
            $this->email_alumni = (!empty($data["email_alumni"])) ? $data["email_alumni"] : NULL;
            $this->solicitado = (!is_null($data["solicitado"])) ? $data["solicitado"] : NULL;
            $this->solicitud_mail = (!is_null($data["solicitud_mail"])) ? $data["solicitud_mail"] : NULL;
            $this->estado = (!is_null($data["estado"])) ? $data["estado"] : NULL;
            $this->fechanacimiento = (!empty($data["fechanacimiento"])) ? $data["fechanacimiento"] : NULL;
            $this->sexo = (!empty($data["sexo"])) ? $data["sexo"] : NULL;
            $this->nacionalidad = (!empty($data["nacionalidad"])) ? $data["nacionalidad"] : NULL;
            $this->direccion = (!empty($data["direccion"])) ? $data["direccion"] : NULL;
            $this->idcomuna = (!empty($data["idcomuna"])) ? $data["idcomuna"] : NULL;
            $this->postal = (!empty($data["postal"])) ? $data["postal"] : NULL;
            $this->email = (!empty($data["email"])) ? $data["email"] : NULL;
            $this->telefono = (!empty($data["telefono"])) ? $data["telefono"] : NULL;
            $this->celular = (!empty($data["celular"])) ? $data["celular"] : NULL;
            $this->password = (!empty($data["password"])) ? $data["password"] : NULL;
            $this->fechaactivacion = (!empty($data["fechaactivacion"])) ? $data["fechaactivacion"] : NULL;
            $this->fechaingreso = (!empty($data["fechaingreso"])) ? $data["fechaingreso"] : NULL;
            $this->fechamodificacion = (!empty($data["fechamodificacion"])) ? $data["fechamodificacion"] : NULL;
            $this->fecharegistro = (!empty($data["fecharegistro"])) ? $data["fecharegistro"] : NULL;
            $this->fechasolicitud = (!empty($data["fechasolicitud"])) ? $data["fechasolicitud"] : NULL;
            $this->fechaeliminacion = (!empty($data["fechaeliminacion"])) ? $data["fechaeliminacion"] : NULL;
            $this->activo = (!is_null($data["activo"])) ? $data["activo"] : NULL;
            $this->registrado = (!is_null($data["registrado"])) ? $data["registrado"] : NULL;
         }
    }
    
    /* OBTIENE USUARIOS SEGÚN LO DATOS DE LA CLASE */
    function get($return = false, $datos = NULL, $custom = NULL){
        
        if(!empty($datos) && is_array($datos))
            $this->initialize($datos);
            
        if(!empty($this->idregistro))
            $this->db->where('idregistro', $this->idregistro);
        
        if(!empty($this->nombre))
            $this->db->where('nombre', $this->nombre);
        
        if(!empty($this->nombre_completo))
            $this->db->like('nombre_completo', $this->nombre_completo);
        
        if(!empty($this->rut))
            $this->db->where('rut', $this->limpiarRut($this->rut));
            
        if(!empty($this->apaterno))
            $this->db->where('apaterno', $this->apaterno);
            
        if(!empty($this->amaterno))
            $this->db->where('amaterno', $this->amaterno);
        
        if(!empty($this->password))
            $this->db->where('password', $this->password);

        if(!empty($this->email))
            $this->db->where('email', $this->email);

        if(!is_null($this->estado))
            $this->db->where('estado', $this->estado);

        if(!is_null($this->solicitado))
            $this->db->where('solicitado', $this->solicitado);

        if(!is_null($this->registrado))
            $this->db->where('registrado', $this->registrado);
        
        if(!is_null($this->activo))
            $this->db->where('registro.activo', $this->activo);
        else
            $this->db->where('registro.activo != -1', NULL, false);
        
        /**
         * Filtros avanzados academicos
         * Filtros academicos
         */
        if(!is_null($datos["idtipocarrera"]) || !is_null($datos["idcarrera"]) || !is_null($datos["idcampus"])){
            $this->db->join('registro_academico', 'registro.idregistro = registro_academico.idregistro');
            if(!is_null($datos["idtipocarrera"])){
                $this->db->join('carreras', 'registro_academico.idcarrera = carreras.idcarrera');
                $this->db->where('carreras.idtipocarrera',$datos["idtipocarrera"]);
            }
            if(!is_null($datos["idcarrera"])){
                $this->db->where('registro_academico.idcarrera',$datos["idcarrera"]);
            }
            if(!is_null($datos["idcampus"])){
                $this->db->where('registro_academico.idcampus',$datos["idcampus"]);
            }
            
            
        }
        /**
         * Filtros geograficos
         */
        if(!is_null($datos["idpais"]) || !is_null($datos["idregion"]) || !is_null($datos["idciudad"]) || !is_null($datos["idcomuna"])){
            $this->db->join('comuna', 'registro.idcomuna = comuna.idcomuna');
            if(!is_null($datos["idpais"])){
                $this->db->where('registro.nacionalidad',$datos["idpais"]);
            }
            if(!is_null($datos["idregion"])){
                $this->db->join('ciudad', 'comuna.idciudad = ciudad.idciudad');
                $this->db->where('ciudad.idregion',$datos["idregion"]);
            }
            if(!is_null($datos["idciudad"])){
                $this->db->where('comuna.idciudad',$datos["idciudad"]);
            }
            if(!is_null($datos["idcomuna"])){
                $this->db->where('registro.idcomuna',$datos["idcomuna"]);
            }
        }
        /**
         * Filtros laborales
         */
        if(!is_null($datos["tipo_cargo"]) || !is_null($datos["rubro"])){
            
            $this->db->join('registro_laboral', 'registro_laboral.idregistro = registro.idregistro');
            if(!is_null($datos["tipo_cargo"])){
                $this->db->where('registro_laboral.idtipocargo',$datos["tipo_cargo"]);
            }
            if(!is_null($datos["rubro"])){
                $this->db->where('registro_laboral.idrubro',$datos["rubro"]);
            }
        }
        /**
         * Filtros de registro
         */
        if(!is_null($datos["desdeRegistro"])){
            $this->db->where('fecharegistro >=', $datos["desdeRegistro"]);
        }
        
        if(!is_null($datos["hastaRegistro"])){
            $this->db->where('fecharegistro <=', $datos["hastaRegistro"]);
        }
        
        if(!empty($custom) && is_array($custom)) {
            if(!empty($custom["order"]))
                $this->db->order_by($custom["order"]); 
            if(is_numeric($custom["limit"]) && is_numeric($custom["pagina"]))
                $this->db->limit($custom["limit"], (($custom["limit"] * $custom["pagina"]) - $custom["limit"]));
            if(is_numeric($custom["limit"]))
                $this->db->limit($custom["limit"]);
        }

        $query = $this->db->get($this->tabla);
        if ($query->num_rows() > 0){
            if($return == 'count')
            {
                return $query->num_rows();
            }
            $registros = array();
            foreach ($query->result_array() as $row){
                switch ($return){
                    case 'all':
                    case 'object':
                        $tmp = new Registro_model();
                        $tmp->idregistro = $row["idregistro"];
                        $tmp->nombre = $row["nombre"];
                        $tmp->nombre_completo = $row["nombre_completo"];
                        $tmp->rut = $row["rut"];
                        $tmp->apaterno = $row["apaterno"];
                        $tmp->amaterno = $row["amaterno"];
                        $tmp->email_alumni = $row["email_alumni"];
                        $tmp->solicitado = $row["solicitado"];
                        $tmp->solicitud_mail = $row["solicitud_mail"];
                        $tmp->estado = $row["estado"];
                        $tmp->fechanacimiento = $row["fechanacimiento"];
                        $tmp->sexo = $row["sexo"];
                        $tmp->nacionalidad = $row["nacionalidad"];
                        $tmp->direccion = $row["direccion"];
                        $tmp->idcomuna = $row["idcomuna"];
                        $tmp->postal = $row["postal"];
                        $tmp->email = $row["email"];
                        $tmp->telefono = $row["telefono"];
                        $tmp->celular = $row["celular"];
                        $tmp->password = $row["password"];
                        $tmp->fechaactivacion = $row["fechaactivacion"];
                        $tmp->fechaingreso = $row["fechaingreso"];
                        $tmp->fechamodificacion = $row["fechamodificacion"];
                        $tmp->fecharegistro = $row["fecharegistro"];
                        $tmp->fechasolicitud = $row["fechasolicitud"];
                        $tmp->fechaeliminacion = $row["fechaeliminacion"];
                        $tmp->activo = $row["activo"];
                        $tmp->registrado = $row["registrado"];
                        if($return === 'object')
                            return $tmp;
                        $registros[] = $tmp;
                    break;
        
                    
                    case 'id':
                        return $row["idregistro"];
                    break;
                    
                    case 'boolean':
                        return true;
                    break;
                    
                    default:
                        $this->idregistro = $row["idregistro"];
                        $this->nombre = $row["nombre"];
                        $this->nombre_completo = $row["nombre_completo"];
                        $this->rut = $row["rut"];
                        $this->apaterno = $row["apaterno"];
                        $this->amaterno = $row["amaterno"];
                        $this->email_alumni = $row["email_alumni"];
                        $this->solicitado = $row["solicitado"];
                        $this->solicitud_mail = $row["solicitud_mail"];
                        $this->estado = $row["estado"];
                        $this->fechanacimiento = $row["fechanacimiento"];
                        $this->sexo = $row["sexo"];
                        $this->nacionalidad = $row["nacionalidad"];
                        $this->direccion = $row["direccion"];
                        $this->idcomuna = $row["idcomuna"];
                        $this->postal = $row["postal"];
                        $this->email = $row["email"];
                        $this->telefono = $row["telefono"];
                        $this->celular = $row["celular"];
                        $this->password = $row["password"];
                        $this->fechaactivacion = $row["fechaactivacion"];
                        $this->fechaingreso = $row["fechaingreso"];
                        $this->fechamodificacion = $row["fechamodificacion"];
                        $this->fecharegistro = $row["fecharegistro"];if(!empty($this->nombre))
                        $this->db->where('nombre', $this->nombre);
                        $this->fechasolicitud = $row["fechasolicitud"];
                        $this->fechaeliminacion = $row["fechaeliminacion"];
                        $this->activo = $row["activo"];
                        $this->registrado = $row["registrado"];
                        return true;
                    break;
                    
                }
           }
           return $registros;
        }else{
            return false;
        }
    }
    
    

    /* INSERTAR UN USUARIO A LA BD, RETORNA EL ID DEL USUARIO INSERTADO; EN CASO DE NO HABER INSERTADO ID = 0 */
    function insert($datos = array()){
        #SIN ID .        
        if(empty($datos) || !is_array($datos))
        {
            return false;
        }else{
            if(!empty($datos["rut"])){
                $datos["rut"] = $this->limpiarRut($datos["rut"]);
            }else{
                return false;
            }
        }

        $this->db->insert($this->tabla, $datos); 
        return $this->db->insert_id();
    }
    
    
    function update($values = array()){
        if(!empty($values)){
            
            if(!empty($values["rut"]))
                $this->db->set('rut', $this->limpiarRut($values["rut"]));
            
            if(!empty($values["nombre_completo"]))
                $this->db->set('nombre_completo', $values["nombre_completo"]);    

            if(!empty($values["nombre"]))
                $this->db->set('nombre', $values["nombre"]);    
                
            if(!empty($values["apaterno"]))
                $this->db->set('apaterno', $values["apaterno"]);
                
            if(!empty($values["amaterno"]))       
                $this->db->set('amaterno', $values["amaterno"]);
            
            if(!empty($values["email_alumni"]))       
                $this->db->set('email_alumni', $values["email_alumni"]);

            if(!is_null($values["solicitado"]))       
                $this->db->set('solicitado', $values["solicitado"]);

            if(!is_null($values["solicitud_mail"]))       
                $this->db->set('solicitud_mail', $values["solicitud_mail"]);

            if(!is_null($values["estado"]))       
                $this->db->set('estado', $values["estado"]);

            if(!empty($values["fechanacimiento"]))       
                $this->db->set('fechanacimiento', $values["fechanacimiento"]);

            if(!empty($values["sexo"]))       
                $this->db->set('sexo', $values["sexo"]);

            if(!empty($values["nacionalidad"]))       
                $this->db->set('nacionalidad', $values["nacionalidad"]);

            if(!empty($values["direccion"]))       
                $this->db->set('direccion', $values["direccion"]);

            if(!empty($values["idcomuna"]))       
                $this->db->set('idcomuna', $values["idcomuna"]);

            if(!empty($values["postal"]))       
                $this->db->set('postal', $values["postal"]);

            if(!empty($values["email"]))       
                $this->db->set('email', $values["email"]);

            if(!empty($values["telefono"]))       
                $this->db->set('telefono', $values["telefono"]);

            if(!empty($values["celular"]))       
                $this->db->set('celular', $values["celular"]);

            if(!empty($values["password"]))       
                $this->db->set('password', $values["password"]);

            if(!empty($values["fechaactivacion"]))       
                $this->db->set('fechaactivacion', $values["fechaactivacion"]);

            if(!empty($values["fechaingreso"]))       
                $this->db->set('fechaingreso', $values["fechaingreso"]);

            if(!empty($values["fechamodificacion"]))       
                $this->db->set('fechamodificacion', $values["fechamodificacion"]);

            if(!empty($values["fecharegistro"]))       
                $this->db->set('fecharegistro', $values["fecharegistro"]);

            if(!empty($values["fechasolicitud"]))       
                $this->db->set('fechasolicitud', $values["fechasolicitud"]);

            if(!empty($values["fechaeliminacion"]))       
                $this->db->set('fechaeliminacion', $values["fechaeliminacion"]);

            if(!is_null($values["activo"]))       
                $this->db->set('activo', $values["activo"]);

            if(!is_null($values["registrado"]))       
                $this->db->set('registrado', $values["registrado"]);


            #ID KEY
            if(!empty($this->idregistro))
                $this->db->where('idregistro', $this->idregistro); 
            else
                $this->db->where('idregistro', $values["idregistro"]);
            
            return $this->db->update($this->tabla);
        }else
            return false;
        if(!empty($datos) && is_array($datos))
            $this->initialize($datos);
    }

    function delete($idregistro = NULL){
        if(empty($idregistro))
            $idregistro = $this->idregistro;
            
        if(!empty($idregistro)){           
            $values = array('activo' => -1, 'fechaeliminacion' => date('d-m-Y h:i:s'), 'idregistro' => $idregistro);
            if($this->update($values))
                return true;
            else
                return false;
        }else
            return false;
    }

    private function limpiarRut($rut){
        $rut = str_replace(".","", $rut);
        $rut = str_replace("-","", $rut);
        return $rut;
    }
}