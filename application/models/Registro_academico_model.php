<?php
class Registro_academico_model extends CI_Model {

    private $idregistro_academico;
    private $idregistro;
    private $descripcion;
    private $idcarrera;
    private $idcampus;
    private $idinstitucion;
    private $fecha_inicio;
    private $fecha_termino;
    private $activo;
        
    private $tabla = 'registro_academico';
    
    function __construct(){
        parent::__construct();
    }
    
    /* GETTER */
    function getIdRegistroAcademico(){    return $this->idregistro_academico;    }
    function getDescripcion(){   return $this->descripcion;   }
    function getIdRegistro(){ return $this->idregistro; }
    function getIdCarrera(){ return $this->idcarrera; }
    function getIdCampus(){ return $this->idcampus; }
    function getIdInstitucion(){   return $this->idinstitucion;   }
    function getFechaInicio(){   return $this->fecha_inicio;   }
    function getFechaTermino(){   return $this->fecha_termino;   }
    function getActivo(){   return $this->activo;   }

    /* SETTER */
    function setIdRegistroAcademico($in){ $this->idregistro_academico = $in; }
    function setDescripcion($in){    $this->descripcion = $in;    }
    function setIdRegistro($in){  $this->idregistro = $in;  }
    function setIdCarrera($in){  $this->idcarrera = $in;  }
    function setIdCampus($in){    $this->idcampus = $in;    }
    function setIdInstitucion(){    $this->idinstitucion;   }
    function setFechaInicio(){    $this->fecha_inicio = $in;   }
    function setFechaTermino(){    $this->fecha_termino = $in;   }
    function setActivo(){    $this->activo = $in;   }
    
    /* inicializar la clase */
    function initialize($data = array()){
        if(!empty($data)){
            $this->idregistro_academico = (!is_null($data["idregistro_academico"])) ? $data["idregistro_academico"] : NULL;
            $this->descripcion = (!empty($data["descripcion"])) ? $data["descripcion"] : NULL;
            $this->idregistro = (!empty($data["idregistro"])) ? $data["idregistro"] : NULL;
            $this->idcarrera = (!empty($data["idcarrera"])) ? $data["idcarrera"] : NULL;
            $this->idcampus = (!empty($data["idcampus"])) ? $data["idcampus"] : NULL;
            $this->idinstitucion = (!empty($data["idinstitucion"])) ? $data["idinstitucion"] : NULL;
            $this->fecha_inicio = (!empty($data["fecha_inicio"])) ? $data["fecha_inicio"] : NULL;
            $this->fecha_termino = (!empty($data["fecha_termino"])) ? $data["fecha_termino"] : NULL;
            $this->activo = (!is_null($data["activo"])) ? $data["activo"] : NULL;
         }
    }
    
    /* OBTIENE USUARIOS SEGÚN LO DATOS DE LA CLASE */
    function get($return = false, $datos = NULL){
        
        if(!empty($datos) && is_array($datos))
            $this->initialize($datos);
            
        if(!empty($this->idregistro_academico))
            $this->db->where('idregistro_academico', $this->idregistro_academico);
        
        if(!empty($this->descripcion))
            $this->db->where('descripcion', $this->descripcion);
        
        if(!empty($this->idregistro))
            $this->db->where('idregistro', $this->idregistro);
            
        if(!empty($this->idcarrera))
            $this->db->where('idcarrera', $this->idcarrera);
            
        if(!empty($this->idcampus))
            $this->db->where('idcampus', $this->idcampus);
        
        if(!is_null($this->activo))
            $this->db->where('activo', $this->activo);

        $query = $this->db->get($this->tabla);
        if ($query->num_rows() > 0){
            $registros = array();
            foreach ($query->result_array() as $row){
                switch ($return){
                    case 'all':
                    case 'object':
                        $tmp = new Registro_academico_model();
                        $tmp->idregistro_academico = $row["idregistro_academico"];
                        $tmp->descripcion = $row["descripcion"];
                        $tmp->idregistro = $row["idregistro"];
                        $tmp->idcarrera = $row["idcarrera"];
                        $tmp->idcampus = $row["idcampus"];
                        $tmp->idinstitucion = $row["idinstitucion"];
                        $tmp->fecha_inicio = $row["fecha_inicio"];
                        $tmp->fecha_termino = $row["fecha_termino"];
                        $tmp->activo = $row["activo"];
                        if($return === 'object')
                            return $tmp;
                        $registros[] = $tmp;
                    break;
        
                    
                    case 'id':
                        return $row["idregistro_academico"];
                    break;
                    
                    case 'boolean':
                        return true;
                    break;
                    
                    default:
                        $this->idregistro_academico = $row["idregistro_academico"];
                        $this->descripcion = $row["descripcion"];
                        $this->idregistro = $row["idregistro"];
                        $this->idcarrera = $row["idcarrera"];
                        $this->idcampus = $row["idcampus"];
                        $this->idinstitucion = $row["idinstitucion"];
                        $this->fecha_inicio = $row["fecha_inicio"];
                        $this->fecha_termino = $row["fecha_termino"];
                        $this->activo = $row["activo"];
                        return true;
                    break;
                    
                }
           }
           return $registros;
        }else{
            #echo $this->db->last_query();
            return false;
        }
    }
    
    /* INSERTAR UN USUARIO A LA BD, RETORNA EL ID DEL USUARIO INSERTADO; EN CASO DE NO HABER INSERTADO ID = 0 */
    function insert($datos = array()){
        #SIN ID USUARIO.
        if(!empty($datos)){
            $data["descripcion"] = (!empty($datos["descripcion"])) ? $datos["descripcion"] : NULL;
            $data["idregistro"] = (!empty($datos["idregistro"])) ? $datos["idregistro"] : NULL;
            $data["idcarrera"] = (!empty($datos["idcarrera"])) ? $datos["idcarrera"] : NULL;
            $data["idcampus"] = (!is_null($datos["idcampus"])) ? $datos["idcampus"] : NULL;
            $data["idinstitucion"] = (!empty($datos["idinstitucion"])) ? $datos["idinstitucion"] : NULL;
            $data["fecha_inicio"] = (!empty($datos["fecha_inicio"])) ? $datos["fecha_inicio"] : NULL;
            $data["fecha_termino"] = (!empty($datos["fecha_termino"])) ? $datos["fecha_termino"] : NULL;
            $data["activo"] = (!is_null($datos["activo"])) ? $datos["activo"] : 1;
        }else{
            return false;
        }
        $this->db->insert($this->tabla, $data); 
        return $this->db->insert_id();
    }
    
    function update($values = array()){
        if(!empty($values)){
            
            if(!empty($values["idregistro"]))
                $this->db->set('idregistro', $values["idregistro"]);
            
            if(!empty($values["descripcion"]))
                $this->db->set('descripcion', $values["descripcion"]);    
                
            if(!empty($values["idcarrera"]))
                $this->db->set('idcarrera', $values["idcarrera"]);
                
            if(!empty($values["idcampus"]))       
                $this->db->set('idcampus', $values["idcampus"]);
            
            if(!empty($values["idinstitucion"]))       
                $this->db->set('idinstitucion', $values["idinstitucion"]);

            if(!empty($values["fecha_inicio"]))       
                $this->db->set('fecha_inicio', $values["fecha_inicio"]);

            if(!empty($values["fecha_termino"]))       
                $this->db->set('fecha_termino', $values["fecha_termino"]);

            if(!is_null($values["activo"]))       
                $this->db->set('activo', $values["activo"]);


            #ID KEY
            if(!empty($this->idregistro_academico))
                $this->db->where('idregistro_academico', $this->idregistro_academico); 
            else
                $this->db->where('idregistro_academico', $values["idregistro_academico"]);
            
            return $this->db->update($this->tabla);
        }else
            return false;
        
    }

    function delete($idregistro_academico = NULL){
        if(empty($idregistro_academico))
            $idregistro_academico = $this->idregistro_academico;
            
        if(!empty($idregistro_academico)){           
            $values = array('activo' => -1, 'idregistro_academico' => $idregistro_academico);
            if($this->update($values))
                return true;
            else
                return false;
        }else
            return false;
    }

}