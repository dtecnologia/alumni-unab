<?php
class Campus_model extends CI_Model {

    private $idcampus;
    private $idinstitucion;
    private $nombre;
    private $direccion;
    private $activo;
        
    private $tabla = 'campus';
    
    function __construct(){
        parent::__construct();
    }
    
    /* GETTER */
    function getIdCampus(){    return $this->idcampus;    }
    function getNombre(){   return $this->nombre;   }
    function getIdInstitucion(){ return $this->idinstitucion; }
    function getDireccion(){ return $this->direccion; }
    function getActivo(){   return $this->activo;   }
    /* SETTER */
    function setIdCampus($in){ $this->idcampus = $in; }
    function setNombre($in){    $this->nombre = $in;    }
    function setIdInstitucion($in){  $this->idinstitucion = $in;  }
    function setDireccion($in){  $this->direccion = $in;  }
    function setActivo($in){    $this->activo = $in;    }
    
    /* inicializar la clase */
    function initialize($data = array()){
        if(!empty($data)){
            $this->idcampus = (!empty($data["idcampus"])) ? $data["idcampus"] : NULL;
            $this->nombre = (!empty($data["nombre"])) ? $data["nombre"] : NULL;
            $this->idinstitucion = (!empty($data["idinstitucion"])) ? $data["idinstitucion"] : NULL;
            $this->direccion = (!empty($data["direccion"])) ? $data["direccion"] : NULL;
            $this->activo = (is_numeric($data["activo"])) ? $data["activo"] : NULL;
         }
    }
    
    /* OBTIENE USUARIOS SEGÚN LO DATOS DE LA CLASE */
    function get($return = false, $datos = NULL){
        
        if(!empty($datos) && is_array($datos))
            $this->initialize($datos);
            
        if(!is_null($this->idcampus)){
            if(is_array($this->idcampus)) {
                $this->db->where_in('idcampus', $this->idcampus);
            } else {
                $this->db->where('idcampus', $this->idcampus);
            
            }
        }
        
        if(!is_null($this->nombre))
            $this->db->where('nombre', $this->nombre);
        
        if(!is_null($this->idinstitucion))
            $this->db->where('idinstitucion', $this->idinstitucion);
            
        if(!empty($this->direccion))
            $this->db->where('direccion', $this->direccion);
            
        if(is_numeric($this->activo))
            $this->db->where('activo', $this->activo);
        
        
        $query = $this->db->get($this->tabla);
        if ($query->num_rows() > 0){
            $campus = array();
            foreach ($query->result_array() as $row){
                switch ($return){
                    case 'all':
                    case 'object':
                        $tmp = new Campus_model();
                        $tmp->idcampus = $row["idcampus"];
                        $tmp->nombre = $row["nombre"];
                        $tmp->idinstitucion = $row["idinstitucion"];
                        $tmp->direccion = $row["direccion"];
                        $tmp->activo = $row["activo"];
                        if($return === 'object')
                            return $tmp;
                        $campus[] = $tmp;
                    break;
        
                    
                    case 'id':
                        return $row["idcampus"];
                    break;
                    
                    case 'boolean':
                        return true;
                    break;
                    
                    default:
                        $this->idcampus = $row["idcampus"];
                        $this->nombre = $row["nombre"];
                        $this->idinstitucion = $row["idinstitucion"];
                        $this->direccion = $row["direccion"];
                        $this->activo = $row["activo"];
                        return true;
                    break;
                    
                }
           }
           return $campus;
        }else{
            #echo $this->db->last_query();
            return false;
        }
    }
    
    /* INSERTAR UN USUARIO A LA BD, RETORNA EL ID DEL USUARIO INSERTADO; EN CASO DE NO HABER INSERTADO ID = 0 */
    function insert($datos = array()){
        #SIN ID USUARIO.
        if(!empty($datos)){
            $data["nombre"] = (!empty($datos["nombre"])) ? $datos["nombre"] : NULL;
            $data["idinstitucion"] = (!empty($datos["idinstitucion"])) ? $datos["idinstitucion"] : NULL;
            $data["direccion"] = (!empty($datos["direccion"])) ? $datos["direccion"] : NULL;
            $data["activo"] = (!is_null($datos["activo"])) ? $datos["activo"] : NULL;
        }else{
            return false;
        }
        $this->db->insert($this->tabla, $data); 
        return $this->db->insert_id();
    }
    
    function update($values = array()){
        if(!empty($values)){
            
            if(!empty($values["idinstitucion"]))
                $this->db->set('idinstitucion', $values["idinstitucion"]);
            
            if(!empty($values["nombre"]))
                $this->db->set('nombre', $values["nombre"]);    
                
            if(!empty($values["direccion"]))
                $this->db->set('direccion', $values["direccion"]);
                
            if(!empty($values["activo"]))       
                $this->db->set('activo', $values["activo"]);
            
            if(!empty($this->idcampus))
                $this->db->where('idcampus', $this->idcampus); 
            else
                $this->db->where('idcampus', $values["idcampus"]);
            
            return $this->db->update($this->tabla);
        }else
            return false;
        
    }

    function delete($idcampus = NULL){
        if(empty($idcampus))
            $idcampus = $this->idcampus;
            
        if(!empty($idcampus)){           
            $this->db->where('idcampus', $idcampus);
            if($this->db->delete($this->tabla))
                return true;
            else
                return false;
        }else
            return false;
    }

}