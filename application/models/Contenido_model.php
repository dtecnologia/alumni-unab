<?php
class Contenido_model extends CI_Model {

    private $idcontenido;
    private $idregistro;
    private $tipo_contenido;
    private $titulo;
    private $bajada;
    private $cuerpo;
    private $estado;
    private $fecha_ing;
    private $fecha_mod;
    private $idusuario_ing;
    private $idregistro_ing;
        
    private $tabla = 'contenido';
    
    function __construct(){
        parent::__construct();
    }
    
    /* GETTER */
    function getIdContenido(){    return $this->idcontenido;    }
    function getTipoContenido(){    return $this->tipo_contenido;    }
    function getTitulo(){   return $this->titulo;   }
    function getBajada(){   return $this->bajada;   }
    function getCuerpo(){ return $this->cuerpo; }
    function getEstado(){ return $this->estado; }
    function getFechaIng(){   return $this->fecha_ing;   }
    function getFechaMod(){   return $this->fecha_mod;   }
    function getIdUsuarioIng(){   return $this->idusuario_ing;   }
    function getIdRegistroIng(){   return $this->idregistro_ing;   }
    /* SETTER */
    function setIdContenido($in){ $this->idcontenido = $in; }
    function setTipoContenido($in){ $this->tipo_contenido = $in; }
    function setBajada($in){    $this->bajada = $in;    }    
    function setCuerpo($in){  $this->cuerpo = $in;  }
    function setEstado($in){    $this->estado = $in;    }
    function setFechaIng($in){    $this->fecha_ing = $in;    }
    function setFechaMod($in){    $this->fecha_mod = $in;    }
    function setIdUsuarioIng($in){    $this->idusuario_ing = $in;    }
    function setIdRegistroIng($in){    $this->idregistro_ing = $in;    }
    
    /* inicializar la clase */
    function initialize($data = array()){
        if(!empty($data)){
            if(!empty($data["idcontenido"])) $this->idcontenido = $data["idcontenido"];
            if(!empty($data["tipo_contenido"])) $this->tipo_contenido =  $data["tipo_contenido"];
            if(!empty($data["bajada"])) $this->bajada = $data["bajada"];
            if(!empty($data["titulo"])) $this->titulo = $data["titulo"];
            if(!empty($data["cuerpo"])) $this->cuerpo = $data["cuerpo"];
            if(!is_null($data["estado"])) $this->estado = $data["estado"];
            if(!empty($data["fecha_ing"]))  $this->fecha_ing = $data["fecha_ing"];
            if(!empty($data["fecha_mod"])) $this->fecha_mod = $data["fecha_mod"];
            if(!empty($data["idusuario_ing"])) $this->idusuario_ing = $data["idusuario_ing"];
            if(!empty($data["idregistro_ing"])) $this->idregistro_ing = $data["idregistro_ing"];
            
         }
    }
    
    /* OBTIENE USUARIOS SEGÚN LO DATOS DE LA CLASE */
    function get($return = false, $datos = NULL, $custom = NULL){
        
        if(!empty($datos) && is_array($datos))
            $this->initialize($datos);
            
        if(!empty($this->idcontenido))
            $this->db->where('idcontenido', $this->idcontenido);

        if(!empty($this->tipo_contenido))
            $this->db->where('tipo_contenido', $this->tipo_contenido);
        
        if(!empty($this->titulo))
            $this->db->where('titulo', $this->titulo);

        if(!empty($this->bajada))
            $this->db->where('bajada', $this->bajada);
            
        if(!empty($this->cuerpo))
            $this->db->where('cuerpo', $this->cuerpo);
            
        if(!is_null($this->estado))
            $this->db->where('estado', $this->estado);
        
        if(!empty($custom) && is_array($custom))
        {
            if(!empty($custom["order"]))
                $this->db->order_by($custom["order"]); 
            if(is_numeric($custom["limit"]) && is_numeric($custom["pagina"]))
                $this->db->limit($custom["limit"], (($custom["limit"] * $custom["pagina"]) - $custom["limit"]));
            if(is_numeric($custom["limit"]))
                $this->db->limit($custom["limit"]);
        }
        
        $query = $this->db->get($this->tabla);
        if ($query->num_rows() > 0){
            if($return == 'count')
            {
                return $query->num_rows();
            }
            $solicitudes = array();
            foreach ($query->result_array() as $row){
                switch ($return){
                    case 'all':
                    case 'object':
                        $tmp = new Contenido_model();
                        $tmp->idcontenido = $row["idcontenido"];
                        $tmp->tipo_contenido = $row["tipo_contenido"];
                        $tmp->bajada = $row["bajada"];
                        $tmp->titulo = $row["titulo"];
                        $tmp->idregistro_ing = $row["idregistro_ing"];
                        $tmp->cuerpo = $row["cuerpo"];
                        $tmp->estado = $row["estado"];
                        $tmp->fecha_mod = $row["fecha_mod"];
                        $tmp->fecha_ing = $row["fecha_ing"];
                        $tmp->idusuario_ing = $row["idusuario_ing"];
                        if($return === 'object'){
                            return $tmp;
                        }
                        $solicitudes[] = $tmp;
                    break;
        
                    
                    case 'id':
                        return $row["idcontenido"];
                    break;
                    
                    case 'boolean':
                        return true;
                    break;
                    
                    default:
                        $this->idcontenido = $row["idcontenido"];
                        $this->tipo_contenido = $row["tipo_contenido"];
                        $this->bajada = $row["bajada"];
                        $this->titulo = $row["titulo"];
                        $this->idregistro_ing = $row["idregistro_ing"];
                        $this->cuerpo = $row["cuerpo"];
                        $this->estado = $row["estado"];
                        $this->fecha_ing = $row["fecha_ing"];
                        $this->fecha_mod = $row["fecha_mod"];
                        $this->idusuario_ing = $row["idusuario_ing"];
                        return true;
                    break;
                    
                }
           }
           return $solicitudes;
        }else{
            #echo $this->db->last_query();
            return false;
        }
    }
    
    /* INSERTAR UN USUARIO A LA BD, RETORNA EL ID DEL USUARIO INSERTADO; EN CASO DE NO HABER INSERTADO ID = 0 */
    function insert($datos = array()){
        #SIN ID USUARIO.
        if(!empty($datos)){
            $data["tipo_contenido"] = (!empty($datos["tipo_contenido"])) ? $datos["tipo_contenido"] : NULL;
            $data["titulo"] = (!empty($datos["titulo"])) ? $datos["titulo"] : NULL;
            $data["bajada"] = (!empty($datos["bajada"])) ? $datos["bajada"] : NULL;
            $data["cuerpo"] = (!empty($datos["cuerpo"])) ? $datos["cuerpo"] : NULL;
            $data["estado"] = 1;
            $data["fecha_mod"] = (!empty($datos["fecha_mod"])) ? $datos["fecha_mod"] : NULL;
            $data["fecha_ing"] = date('Y-m-d H:i:s');
            $data["idusuario_ing"] = (!empty($datos["idusuario_ing"])) ? $datos["idusuario_ing"] : NULL;
            $data["idregistro_ing"] = (!empty($datos["idregistro_ing"])) ? $datos["idregistro_ing"] : NULL;
        }else{
            return false;
        }
        $this->db->insert($this->tabla, $data); 
        return $this->db->insert_id();
    }
    
    function update($values = array()){
        if(!empty($values)){
            
            if(!empty($values["tipo_contenido"]))
                $this->db->set('tipo_contenido', $values["tipo_contenido"]);

            if(!empty($values["idregistro_ing"]))
                $this->db->set('idregistro_ing', $values["idregistro_ing"]);
            
            if(!empty($values["bajada"]))
                $this->db->set('bajada', $values["bajada"]);    

             if(!empty($values["titulo"]))
                $this->db->set('titulo', $values["titulo"]);    
                
            if(!empty($values["cuerpo"]))
                $this->db->set('cuerpo', $values["cuerpo"]);
                
            if(!empty($values["estado"]))       
                $this->db->set('estado', $values["estado"]);

            if(!empty($values["fecha_ing"]))       
                $this->db->set('fecha_ing', $values["fecha_ing"]);

            if(!empty($values["idusuario_ing"]))       
                $this->db->set('idusuario_ing', $values["idusuario_ing"]);

            $this->db->set('fecha_mod', date('Y-m-d H:i:s'));
            
            if(!empty($this->idcontenido))
                $this->db->where('idcontenido', $this->idcontenido); 
            else
                $this->db->where('idcontenido', $values["idcontenido"]);
            
            return $this->db->update($this->tabla);
        }else
            return false;
        
    }

    function delete($idcontenido = NULL){
        if(empty($idcontenido))
            $idcontenido = $this->idcontenido;
            
        if(!empty($idcontenido)){           
            $this->db->where('idcontenido', $idcontenido);
            if($this->db->delete($this->tabla))
                return true;
            else
                return false;
        }else
            return false;
    }

}