<?php
class Campus_carrera_model extends CI_Model {

    private $idcampus_carrera;
    private $idcampus;
    private $idcarrera;
            
    private $tabla = 'campus_carrera';
    
    function __construct(){
        parent::__construct();
    }
    
    /* GETTER */
    function getIdCampusCarrera(){    return $this->idcampus_carrera;    }
    function getIdCarrera(){   return $this->idcarrera;   }
    function getIdCampus(){ return $this->idcampus; }
    
    /* SETTER */
    function setIdCampusCarrera($in){ $this->idcampus_carrera = $in; }
    function setIdCarrera($in){    $this->idcarrera = $in;    }
    function setIdCampus($in){  $this->idcampus = $in;  }
    
    /* inicializar la clase */
    function initialize($data = array()){
        if(!empty($data)){
            $this->idcampus_carrera = (!empty($data["idcampus_carrera"])) ? $data["idcampus_carrera"] : NULL;
            $this->idcarrera = (!empty($data["idcarrera"])) ? $data["idcarrera"] : NULL;
            $this->idcampus = (!empty($data["idcampus"])) ? $data["idcampus"] : NULL;
         }
    }
    
    /* OBTIENE USUARIOS SEGÚN LO DATOS DE LA CLASE */
    function get($return = false, $datos = NULL){
        
        if(!empty($datos) && is_array($datos))
            $this->initialize($datos);
            
        if(!is_null($this->idcampus_carrera))
            $this->db->where('idcampus_carrera', $this->idcampus_carrera);
        
        if(!is_null($this->idcarrera))
            $this->db->where('idcarrera', $this->idcarrera);
        
        if(!is_null($this->idcampus))
            $this->db->where('idcampus', $this->idcampus);
        
        $query = $this->db->get($this->tabla);
        if ($query->num_rows() > 0){
            $campus_carrera = array();
            foreach ($query->result_array() as $row){
                switch ($return){
                    case 'all':
                    case 'object':
                        $tmp = new Campus_carrera_model();
                        $tmp->idcampus_carrera = $row["idcampus_carrera"];
                        $tmp->idcarrera = $row["idcarrera"];
                        $tmp->idcampus = $row["idcampus"];                        
                        if($return === 'object')
                            return $tmp;
                        $campus_carrera[] = $tmp;
                    break;
        
                    
                    case 'id':
                        return $row["idcampus_carrera"];
                    break;
                    
                    case 'boolean':
                        return true;
                    break;
                    
                    default:
                        $this->idcampus_carrera = $row["idcampus_carrera"];
                        $this->idcarrera = $row["idcarrera"];
                        $this->idcampus = $row["idcampus"];
                        return true;
                    break;
                    
                }
           }
           return $campus_carrera;
        }else{
            #echo $this->db->last_query();
            return false;
        }
    }
    
    /* INSERTAR UN USUARIO A LA BD, RETORNA EL ID DEL USUARIO INSERTADO; EN CASO DE NO HABER INSERTADO ID = 0 */
    function insert($datos = array()){
        #SIN ID USUARIO.
        if(!empty($datos)){
            $data["idcarrera"] = (!empty($datos["idcarrera"])) ? $datos["idcarrera"] : NULL;
            $data["idcampus"] = (!empty($datos["idcampus"])) ? $datos["idcampus"] : NULL;
        }else
            return false;
        $this->db->insert($this->tabla, $data); 
        return $this->db->insert_id();
    }
    
    function update($values = array()){
        if(!empty($values)){
            
            if(!empty($values["idcampus"]))
                $this->db->set('idcampus', $values["idcampus"]);
            
            if(!empty($values["idcarrera"]))
                $this->db->set('idcarrera', $values["idcarrera"]);    
            
            if(!empty($this->idcampus_carrera))
                $this->db->where('idcampus_carrera', $this->idcampus_carrera); 
            else
                $this->db->where('idcampus_carrera', $values["idcampus_carrera"]);
            
            return $this->db->update($this->tabla);
        }else
            return false;
        
    }

    function delete($idcampus_carrera = NULL){
        if(empty($idcampus_carrera))
            $idcampus_carrera = $this->idcampus_carrera;
            
        if(!empty($idcampus_carrera)){           
            $this->db->where('idcampus_carrera', $idcampus_carrera);
            if($this->db->delete($this->tabla))
                return true;
            else
                return false;
        }else
            return false;
    }

}