<?php
class Registro_laboral_model extends CI_Model {

    private $idregistro_laboral;
    private $idregistro;
    private $descripcion;
    private $idrubro;
    private $idtipocargo;
    private $empresa;
    private $cargo;
    private $direccion;
    private $idcomuna;
    private $postal;
    private $email;
    private $telefono;
    private $celular;
    private $fecha_inicio;
    private $fecha_termino;
    private $activo;
        
    private $tabla = 'registro_laboral';
    
    function __construct(){
        parent::__construct();
    }
    
    /* GETTER */
    function getIdRegistroLaboral(){    return $this->idregistro_laboral;    }
    function getDescripcion(){   return $this->descripcion;   }
    function getIdRegistro(){ return $this->idregistro; }
    function getIdRubro(){ return $this->idrubro; }
    function getIdTipoCargo(){ return $this->idtipocargo; }
    function getEmpresa(){   return $this->empresa;   }
    function getCargo(){   return $this->cargo;   }
    function getDireccion(){   return $this->direccion;   }
    function getIdComuna(){   return $this->idcomuna;   }
    function getPostal(){   return $this->postal;   }
    function getEmail(){   return $this->email;   }
    function getTelefono(){   return $this->telefono;   }
    function getCelular(){   return $this->celular;   }
    function getFechaInicio(){   return $this->fecha_inicio;   }
    function getFechaTermino(){   return $this->fecha_termino;   }
    function getActivo(){   return $this->activo;   }

    /* SETTER */
    function setIdRegistroLaboral($in){ $this->idregistro_laboral = $in; }
    function setDescripcion($in){    $this->descripcion = $in;    }
    function setIdRegistro($in){  $this->idregistro = $in;  }
    function setIdRubro($in){  $this->idrubro = $in;  }
    function setIdTipoCargo($in){    $this->idtipocargo = $in;    }
    function setEmpresa(){    $this->empresa;   }
    function setCargo(){    $this->cargo;   }
    function setDireccion(){    $this->direccion = $in;   }
    function setIdComuna(){    $this->idcomuna = $in;   }
    function setPostal(){    $this->postal = $in;   }
    function setEmail(){    $this->email = $in;   }
    function setTelefono(){    $this->telefono = $in;   }
    function setCelular(){    $this->celular = $in;   }
    function setFechaInicio(){    $this->fecha_inicio = $in;   }
    function setFechaTermino(){    $this->fecha_termino = $in;   }
    function setActivo(){    $this->activo = $in;   }
    
    /* inicializar la clase */
    function initialize($data = array()){
        if(!empty($data)){
            $this->idregistro_laboral = (!is_null($data["idregistro_laboral"])) ? $data["idregistro_laboral"] : NULL;
            $this->descripcion = (!empty($data["descripcion"])) ? $data["descripcion"] : NULL;
            $this->idregistro = (!empty($data["idregistro"])) ? $data["idregistro"] : NULL;
            $this->idrubro = (!empty($data["idrubro"])) ? $data["idrubro"] : NULL;
            $this->idtipocargo = (!empty($data["idtipocargo"])) ? $data["idtipocargo"] : NULL;
            $this->empresa = (!empty($data["empresa"])) ? $data["empresa"] : NULL;
            $this->cargo = (!empty($data["cargo"])) ? $data["cargo"] : NULL;
            $this->direccion = (!empty($data["direccion"])) ? $data["direccion"] : NULL;
            $this->idcomuna = (!empty($data["idcomuna"])) ? $data["idcomuna"] : NULL;
            $this->postal = (!empty($data["postal"])) ? $data["postal"] : NULL;
            $this->email = (!empty($data["email"])) ? $data["email"] : NULL;
            $this->telefono = (!empty($data["telefono"])) ? $data["telefono"] : NULL;
            $this->celular = (!empty($data["celular"])) ? $data["celular"] : NULL;
            $this->fecha_inicio = (!empty($data["fecha_inicio"])) ? $data["fecha_inicio"] : NULL;
            $this->fecha_termino = (!empty($data["fecha_termino"])) ? $data["fecha_termino"] : NULL;
            $this->activo = (!is_null($data["activo"])) ? $data["activo"] : NULL;
         }
    }
    
    /* OBTIENE USUARIOS SEGÚN LO DATOS DE LA CLASE */
    function get($return = false, $datos = NULL){
        
        if(!empty($datos) && is_array($datos))
            $this->initialize($datos);
            
        if(!empty($this->idregistro_laboral))
            $this->db->where('idregistro_laboral', $this->idregistro_laboral);
        
        if(!empty($this->descripcion))
            $this->db->where('descripcion', $this->descripcion);
        
        if(!empty($this->idregistro))
            $this->db->where('idregistro', $this->idregistro);
            
        if(!empty($this->idrubro))
            $this->db->where('idrubro', $this->idrubro);
            
        if(!empty($this->idtipocargo))
            $this->db->where('idtipocargo', $this->idtipocargo);
        
        if(!is_null($this->activo))
            $this->db->where('activo', $this->activo);

        $this->db->where('activo != -1', NULL, false);
        $this->db->order_by('fecha_inicio', 'DESC');
        $query = $this->db->get($this->tabla);
        if ($query->num_rows() > 0){
            $registros = array();
            foreach ($query->result_array() as $row){
                switch ($return){
                    case 'all':
                    case 'object':
                        $tmp = new Registro_laboral_model();
                        $tmp->idregistro_laboral = $row["idregistro_laboral"];
                        $tmp->descripcion = $row["descripcion"];
                        $tmp->idregistro = $row["idregistro"];
                        $tmp->idrubro = $row["idrubro"];
                        $tmp->idtipocargo = $row["idtipocargo"];
                        $tmp->empresa = $row["empresa"];
                        $tmp->cargo = $row["cargo"];
                        $tmp->direccion = $row["direccion"];
                        $tmp->idcomuna = $row["idcomuna"];
                        $tmp->postal = $row["postal"];
                        $tmp->email = $row["email"];
                        $tmp->telefono = $row["telefono"];
                        $tmp->celular = $row["celular"];
                        $tmp->fecha_inicio = $row["fecha_inicio"];
                        $tmp->fecha_termino = $row["fecha_termino"];
                        $tmp->activo = $row["activo"];
                        if($return === 'object')
                            return $tmp;
                        $registros[] = $tmp;
                    break;
        
                    
                    case 'id':
                        return $row["idregistro_laboral"];
                    break;
                    
                    case 'boolean':
                        return true;
                    break;
                    
                    default:
                        $this->idregistro_laboral = $row["idregistro_laboral"];
                        $this->descripcion = $row["descripcion"];
                        $this->idregistro = $row["idregistro"];
                        $this->idrubro = $row["idrubro"];
                        $this->idtipocargo = $row["idtipocargo"];
                        $this->empresa = $row["empresa"];
                        $this->cargo = $row["cargo"];
                        $this->direccion = $row["direccion"];
                        $this->idcomuna = $row["idcomuna"];
                        $this->postal = $row["postal"];
                        $this->email = $row["email"];
                        $this->telefono = $row["telefono"];
                        $this->celular = $row["celular"];
                        $this->fecha_inicio = $row["fecha_inicio"];
                        $this->fecha_termino = $row["fecha_termino"];
                        $this->activo = $row["activo"];
                        return true;
                    break;
                    
                }
           }
           return $registros;
        }else{
            #echo $this->db->last_query();
            return false;
        }
    }

    function getByRubro($idrubro = NULL, $limit = 20){
        $sql = "SELECT count(*) as cantidad,rubro_empresarial.nombre 
                FROM registro 
                INNER JOIN registro_laboral ON registro_laboral.idregistro = registro.idregistro
                INNER JOIN rubro_empresarial ON rubro_empresarial.idrubro_empresarial = registro_laboral.idrubro 
                WHERE registro.activo = 1 AND registro.registrado = 1 AND registro_laboral.activo = 1
                GROUP BY rubro_empresarial.idrubro_empresarial 
                ORDER BY cantidad DESC
                LIMIT ".$limit;
        $query = $this->db->query($sql);
        $return = array();
        if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $return[] = $row;
            }
        }
        return $return;

    }
    
    /* INSERTAR UN USUARIO A LA BD, RETORNA EL ID DEL USUARIO INSERTADO; EN CASO DE NO HABER INSERTADO ID = 0 */
    function insert($datos = array()){
        #SIN ID USUARIO.
        if(!empty($datos)){
            if(!empty($datos["descripcion"])) 
                $data["descripcion"] = $datos["descripcion"];

            if(!empty($datos["idregistro"])) 
                $data["idregistro"] = $datos["idregistro"];

            if(!empty($datos["idrubro"])) 
                $data["idrubro"] = $datos["idrubro"];

            if(!empty($datos["idtipocargo"])) 
                $data["idtipocargo"] = $datos["idtipocargo"];

            if(!empty($datos["empresa"])) 
                $data["empresa"] = $datos["empresa"];

            if(!empty($datos["cargo"])) 
                $data["cargo"] = $datos["cargo"];

            if(!empty($datos["direccion"])) 
                $data["direccion"] = $datos["direccion"];

            if(!empty($datos["idcomuna"])) 
                $data["idcomuna"] = $datos["idcomuna"];

            if(!empty($datos["postal"])) 
                $data["postal"] = $datos["postal"];

            if(!empty($datos["email"])) 
                $data["email"] = $datos["email"];

            if(!empty($datos["telefono"])) 
                $data["telefono"] = $datos["telefono"];

            if(!empty($datos["celular"])) 
                $data["celular"] = $datos["celular"];

            if(!empty($datos["fecha_inicio"])) 
                $data["fecha_inicio"] = $datos["fecha_inicio"];

            if(!empty($datos["fecha_termino"])) 
                $data["fecha_termino"] = $datos["fecha_termino"];

            if(!empty($data)) 
                $data["activo"] = 1;

        }else{
            return false;
        }
        $this->db->insert($this->tabla, $data); 
        return $this->db->insert_id();
    }
    
    function update($values = array()){
        if(!empty($values)){
            
            if(!empty($values["idregistro"]))
                $this->db->set('idregistro', $values["idregistro"]);
            
            if(!empty($values["descripcion"]))
                $this->db->set('descripcion', $values["descripcion"]);    
                
            if(!empty($values["idrubro"]))
                $this->db->set('idrubro', $values["idrubro"]);
                
            if(!empty($values["idtipocargo"]))       
                $this->db->set('idtipocargo', $values["idtipocargo"]);
            
            if(!empty($values["empresa"]))       
                $this->db->set('empresa', $values["empresa"]);

            if(!empty($values["cargo"]))       
                $this->db->set('cargo', $values["cargo"]);

            if(!empty($values["direccion"]))       
                $this->db->set('direccion', $values["direccion"]);

            if(!empty($values["idcomuna"]))       
                $this->db->set('idcomuna', $values["idcomuna"]);

            if(!empty($values["postal"]))       
                $this->db->set('postal', $values["postal"]);

            if(!empty($values["email"]))       
                $this->db->set('email', $values["email"]);

            if(!empty($values["telefono"]))       
                $this->db->set('telefono', $values["telefono"]);

            if(!empty($values["celular"]))       
                $this->db->set('celular', $values["celular"]);

            if(!empty($values["fecha_inicio"]))       
                $this->db->set('fecha_inicio', $values["fecha_inicio"]);

            if(!empty($values["fecha_termino"]))       
                $this->db->set('fecha_termino', $values["fecha_termino"]);

            if(!is_null($values["activo"]))       
                $this->db->set('activo', $values["activo"]);


            #ID KEY
            if(!empty($this->idregistro_laboral))
                $this->db->where('idregistro_laboral', $this->idregistro_laboral); 
            else
                $this->db->where('idregistro_laboral', $values["idregistro_laboral"]);
            
            return $this->db->update($this->tabla);
        }else
            return false;
        
    }

    function delete($idregistro_laboral = NULL){
        if(empty($idregistro_laboral))
            $idregistro_laboral = $this->idregistro_laboral;
            
        if(!empty($idregistro_laboral)){           
            $values = array('activo' => -1, 'idregistro_laboral' => $idregistro_laboral);
            if($this->update($values))
                return true;
            else
                return false;
        }else
            return false;
    }

}