<?php
class Tipo_solicitud_model extends CI_Model {

    private $idtipo_solicitud;
    private $nombre;
    private $idusuario;
        
    private $tabla = 'tipo_solicitud';
    
    function __construct(){
        parent::__construct();
    }
    
    /* GETTER */
    function getIdTipoSolicitud(){    return $this->idtipo_solicitud;    }
    function getNombre(){   return $this->nombre;   }
    function getIdUsuario(){   return $this->idusuario;   }

    /* SETTER */
    function setIdTipoSolicitud($in){ $this->idtipo_solicitud = $in; }
    function setNombre($in){    $this->nombre = $in;    }
    function setIdUsuario($in){    $this->idusuario = $in;    }
    
    /* inicializar la clase */
    function initialize($data = array()){
        if(!empty($data)){
            $this->idtipo_solicitud = (!empty($data["idtipo_solicitud"])) ? $data["idtipo_solicitud"] : NULL;
            $this->nombre = (!empty($data["nombre"])) ? $data["nombre"] : NULL;
            $this->idusuario = (!empty($data["idusuario"])) ? $data["idusuario"] : NULL;
         }
    }
    
    /* OBTIENE USUARIOS SEGÚN LO DATOS DE LA CLASE */
    function get($return = false, $datos = NULL){
        
        if(!empty($datos) && is_array($datos))
            $this->initialize($datos);
            
        if(!empty($this->idtipo_solicitud))
            $this->db->where('idtipo_solicitud', $this->idtipo_solicitud);
        
        if(!empty($this->nombre))
            $this->db->where('nombre', $this->nombre);

        if(!empty($this->idusuario))
            $this->db->where('idusuario', $this->idusuario);
        
        $query = $this->db->get($this->tabla);
        if ($query->num_rows() > 0){
            $registros = array();
            foreach ($query->result_array() as $row){
                switch ($return){
                    case 'all':
                    case 'object':
                        $tmp = new Tipo_solicitud_model();
                        $tmp->idtipo_solicitud = $row["idtipo_solicitud"];
                        $tmp->nombre = $row["nombre"];
                        $tmp->idusuario = $row["idusuario"];
                        if($return === 'object')
                            return $tmp;
                        $registros[] = $tmp;
                    break;
        
                    
                    case 'id':
                        return $row["idtipo_solicitud"];
                    break;
                    
                    case 'boolean':
                        return true;
                    break;
                    
                    default:
                        $this->idtipo_solicitud = $row["idtipo_solicitud"];
                        $this->nombre = $row["nombre"];
                        $this->idusuario = $row["idusuario"];
                        return true;
                    break;
                    
                }
           }
           return $registros;
        }else{
            #echo $this->db->last_query();
            return false;
        }
    }
    
    /* INSERTAR UN USUARIO A LA BD, RETORNA EL ID DEL USUARIO INSERTADO; EN CASO DE NO HABER INSERTADO ID = 0 */
    function insert($datos = array()){
        #SIN ID USUARIO.
        if(!empty($datos)){
            $data["nombre"] = (!empty($datos["nombre"])) ? $datos["nombre"] : NULL;
            $data["idusuario"] = (!empty($datos["idusuario"])) ? $datos["idusuario"] : NULL;
        }else{
            return false;
        }
        $this->db->insert($this->tabla, $data); 
        return $this->db->insert_id();
    }
    
    function update($values = array()){
        if(!empty($values)){
            
            if(!empty($values["nombre"]))
                $this->db->set('nombre', $values["nombre"]);    

            if(!empty($values["idusuario"]))
                $this->db->set('idusuario', $values["idusuario"]);    
                
            if(!empty($this->idtipo_solicitud))
                $this->db->where('idtipo_solicitud', $this->idtipo_solicitud); 
            else
                $this->db->where('idtipo_solicitud', $values["idtipo_solicitud"]);
            
            return $this->db->update($this->tabla);
        }else
            return false;
        
    }

    function delete($idtipo_solicitud = NULL){
        if(empty($idtipo_solicitud))
            $idtipo_solicitud = $this->idtipo_solicitud;
            
        if(!empty($idtipo_solicitud)){           
        	$this->db->where('idtipo_solicitud', $idtipo_solicitud);
            if($this->db->delete($this->tabla))
                return true;
            else
                return false;
        }else
            return false;
    }

}