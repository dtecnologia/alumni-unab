<?php

class Comuna_model extends CI_Model {
	
	#PAIS
	private $idciudad;
	private $nombre;
	private $idcomuna;

	private $tabla = 'comuna';
	
    function __construct($data = array()){
		parent::__construct(); 
    }
	
	function initialize($data = array()){
		if(!empty($data)){
			$this->idcomuna = (is_numeric($data["idcomuna"])) ? $data["idcomuna"] : NULL;
			$this->nombre = (!empty($data["nombre"])) ? $data["nombre"] : NULL;
			$this->idciudad = (is_numeric($data["idciudad"])) ? $data["idciudad"] : NULL;
		}
	}
	
	function getIdComuna(){	return $this->idcomuna;	}
	function getNombre(){	return $this->nombre;	}
	function getIdCiudad(){	return $this->idciudad;	}
	
	function setIdComuna($in){	$this->idcomuna = $in;	}
	function setNombre($in){	$this->nombre = $in;	}
	function setIdCiudad($in){	$this->idciudad = $in;	}
	
	function get($return = false, $data = array()){
		if(!empty($data))
			$this->initialize($data);
		
		if(is_numeric($this->idcomuna))
			$this->db->where('idcomuna', $this->idcomuna);
			
		if(!empty($this->nombre))
			$this->db->where('nombre', $this->nombre);

		if(is_numeric($this->idciudad))
			$this->db->where('idciudad', $this->idciudad);
		
		$this->db->order_by('nombre', 'asc');
		$query = $this->db->get($this->tabla);
		if ($query->num_rows() > 0){
			$comunas = array();
		   	foreach ($query->result_array() as $row){
			   switch ($return){
					case 'all':
					case 'object':
						$tmp = new Comuna_model();
						$tmp->idciudad = $row["idciudad"];
						$tmp->idcomuna = $row["idcomuna"];
						$tmp->nombre = $row["nombre"];
						if($return == 'object')
							return $tmp;
						$comunas[] = $tmp;
		   			break;
					
					case 'array':
						return $row;
					break;
					
					case 'id':
						return $row["idciudad"];
					break;
					
					case 'boolean':
						return true;
					break;
					
					default:
						$this->idciudad = $row["idciudad"];
						$this->idcomuna = $row["idcomuna"];
						$this->nombre = $row["nombre"];
						return true;
					break;
					
				}
			}
			return $comunas;
		}
		return false;
	}
	
	
	
}