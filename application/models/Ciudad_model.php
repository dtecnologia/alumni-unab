<?php

class Ciudad_model extends CI_Model {
	
	#PAIS
	private $idciudad;
	private $nombre;
	private $idregion;

	private $tabla = 'ciudad';
	
    function __construct($data = array()){
		parent::__construct(); 
    }
	
	function initialize($data = array()){
		if(!empty($data)){
			$this->idregion = (!empty($data["idregion"])) ? $data["idregion"] : NULL;
			$this->nombre = (!empty($data["nombre"])) ? $data["nombre"] : NULL;
			$this->idciudad = (!empty($data["idciudad"])) ? $data["idciudad"] : NULL;
		}
	}
	
	function getIdRegion(){	return $this->idregion;	}
	function getNombre(){	return $this->nombre;	}
	function getIdCiudad(){	return $this->idciudad;	}
	
	function setIdRegion($in){	$this->idregion = $in;	}
	function setNombre($in){	$this->nombre = $in;	}
	function setIdCiudad($in){	$this->idciudad = $in;	}
	
	function get($return = false, $data = array()){
		if(!empty($data))
			$this->initialize($data);
		
		if(!empty($this->idregion))
			$this->db->where('idregion', $this->idregion);
			
		if(!empty($this->nombre))
			$this->db->where('nombre', $this->nombre);
			
		if(!empty($this->idciudad))
			$this->db->where('idciudad', $this->idciudad);
		
		$this->db->order_by('nombre', 'asc');
		$query = $this->db->get($this->tabla);
		if ($query->num_rows() > 0){
			$ciudades = array();
		   	foreach ($query->result_array() as $row){
			   switch ($return){
					case 'all':
					case 'object':
						$tmp = new Ciudad_model();
						$tmp->idciudad = $row["idciudad"];
						$tmp->idregion = $row["idregion"];
						$tmp->nombre = $row["nombre"];
						if($return == 'object')
							return $tmp;
						$ciudades[] = $tmp;
		   			break;
					
					case 'array':
						return $row;
					break;
					
					case 'id':
						return $row["idciudad"];
					break;
					
					case 'boolean':
						return true;
					break;
					
					default:
						$this->idciudad = $row["idciudad"];
						$this->idregion = $row["idregion"];
						$this->nombre = $row["nombre"];
						return true;
					break;
					
				}
			}
			return $ciudades;
		}
		return false;
	}
	
	
	
}