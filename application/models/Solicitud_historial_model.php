<?php
class Solicitud_historial_model extends CI_Model {

    private $idsolicitud_historial;
    private $idsolicitud;
    private $idusuario;
    private $estado;
    private $descripcion;
    private $fecha;
    private $idregistro;
        
    private $tabla = 'solicitud_historial';
    
    function __construct(){
        parent::__construct();
    }
    
    /* GETTER */
    function getIdSolicitudHistorial(){    return $this->idsolicitud_historial;    }
    function getIdUsuario(){    return $this->idusuario;    }
    function getEstado(){   return $this->estado;   }
    function getIdSolicitud(){ return $this->idsolicitud; }
    function getDescripcion(){ return $this->descripcion; }
    function getFecha(){   return $this->fecha;   }
    function getIdRegistro(){   return $this->idregistro;   }
    /* SETTER */
    function setIdSolicitudHistorial($in){ $this->idsolicitud_historial = $in; }
    function setIdUsuario($in){ $this->idusuario = $in; }
    function setEstado($in){    $this->estado = $in;    }
    function setIdSolicitud($in){  $this->idsolicitud = $in;  }
    function setDescripcion($in){  $this->descripcion = $in;  }
    function setFecha($in){    $this->fecha = $in;    }
    function setIdRegistro($in){    $this->idregistro = $in;    }
    
    /* inicializar la clase */
    function initialize($data = array()){
        if(!empty($data)){
            $this->idsolicitud_historial = (!empty($data["idsolicitud_historial"])) ? $data["idsolicitud_historial"] : NULL;
            $this->idusuario = (!empty($data["idusuario"])) ? $data["idusuario"] : NULL;
            $this->idsolicitud = (!empty($data["idsolicitud"])) ? $data["idsolicitud"] : NULL;
            $this->descripcion = (!empty($data["descripcion"])) ? $data["descripcion"] : NULL;
            $this->fecha = (!empty($data["fecha"])) ? $data["fecha"] : NULL;
            $this->idregistro = (!empty($data["idregistro"])) ? $data["idregistro"] : NULL;
            $this->estado = (!empty($data["estado"])) ? $data["estado"] : NULL;
         }
    }
    
    /* OBTIENE USUARIOS SEGÚN LO DATOS DE LA CLASE */
    function get($return = false, $datos = NULL){
        
        if(!empty($datos) && is_array($datos))
            $this->initialize($datos);
            
        if(!empty($this->idsolicitud_historial))
            $this->db->where('idsolicitud_historial', $this->idsolicitud_historial);

        if(!empty($this->idusuario))
            $this->db->where('idusuario', $this->idusuario);
      
        if(!empty($this->idregistro))
            $this->db->where('idregistro', $this->idregistro);

        if(!empty($this->idsolicitud))
            $this->db->where('idsolicitud', $this->idsolicitud);
            
        if(!empty($this->descripcion))
            $this->db->where('descripcion', $this->descripcion);

        if(!empty($this->estado))
            $this->db->where('estado', $this->estado);
        
        $query = $this->db->get($this->tabla);
        if ($query->num_rows() > 0){
            $solicitudes = array();
            foreach ($query->result_array() as $row){
                switch ($return){
                    case 'all':
                    case 'object':
                        $tmp = new Solicitud_historial_model();
                        $tmp->idsolicitud_historial = $row["idsolicitud_historial"];
                        $tmp->idusuario = $row["idusuario"];
                        $tmp->idsolicitud = $row["idsolicitud"];
                        $tmp->descripcion = $row["descripcion"];
                        $tmp->idregistro = $row["idregistro"];
                        $tmp->fecha = $row["fecha"];
                        $tmp->estado = $row["estado"];
                        if($return === 'object')
                            return $tmp;
                        $solicitudes[] = $tmp;
                    break;
        
                    
                    case 'id':
                        return $row["idsolicitud_historial"];
                    break;
                    
                    case 'boolean':
                        return true;
                    break;
                    
                    default:
                        $this->idsolicitud_historial = $row["idsolicitud_historial"];
                        $this->idusuario = $row["idusuario"];
                        $this->estado = $row["estado"];
                        $this->idsolicitud = $row["idsolicitud"];
                        $this->descripcion = $row["descripcion"];
                        $this->fecha = $row["fecha"];
                        $this->idregistro = $row["idregistro"];
                        return true;
                    break;
                    
                }
           }
           return $solicitudes;
        }else{
            #echo $this->db->last_query();
            return false;
        }
    }
    
    /* INSERTAR UN USUARIO A LA BD, RETORNA EL ID DEL USUARIO INSERTADO; EN CASO DE NO HABER INSERTADO ID = 0 */
    function insert($datos = array()){
        #SIN ID USUARIO.
        if(!empty($datos)){
            $data["idusuario"] = (!empty($datos["idusuario"])) ? $datos["idusuario"] : NULL;
            $data["estado"] = (!empty($datos["estado"])) ? $datos["estado"] : NULL;
            $data["idsolicitud"] = (!empty($datos["idsolicitud"])) ? $datos["idsolicitud"] : NULL;
            $data["descripcion"] = (!empty($datos["descripcion"])) ? $datos["descripcion"] : NULL;
            $data["idregistro"] = (!empty($datos["idregistro"])) ? $datos["idregistro"] : NULL;
            $data["fecha"] = (!empty($datos["fecha"])) ? $datos["fecha"] : NULL;
        }else{
            return false;
        }
        $this->db->insert($this->tabla, $data); 
        return $this->db->insert_id();
    }
    
    function update($values = array()){
        if(!empty($values)){
            
            if(!empty($values["idusuario"]))
                $this->db->set('idusuario', $values["idusuario"]);

            if(!empty($values["idsolicitud"]))
                $this->db->set('idsolicitud', $values["idsolicitud"]);
            
            if(!empty($values["estado"]))
                $this->db->set('estado', $values["estado"]);    
                
            if(!empty($values["descripcion"]))
                $this->db->set('descripcion', $values["descripcion"]);
            
            if(!empty($values["fecha"]))       
                $this->db->set('fecha', $values["fecha"]);

            if(!empty($values["idregistro"]))       
                $this->db->set('idregistro', $values["idregistro"]);

            if(!empty($this->idsolicitud_historial))
                $this->db->where('idsolicitud_historial', $this->idsolicitud_historial); 
            else
                $this->db->where('idsolicitud_historial', $values["idsolicitud_historial"]);
            
            return $this->db->update($this->tabla);
        }else
            return false;
        
    }

    function delete($idsolicitud_historial = NULL){
        if(empty($idsolicitud_historial))
            $idsolicitud_historial = $this->idsolicitud_historial;
            
        if(!empty($idsolicitud_historial)){           
            $this->db->where('idsolicitud_historial', $idsolicitud_historial);
            if($this->db->delete($this->tabla))
                return true;
            else
                return false;
        }else
            return false;
    }

}