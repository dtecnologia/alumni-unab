<?php
class Carrera_model extends CI_Model {

    private $idcarrera;
    private $nombre;
    private $idtipocarrera;
            
    private $tabla = 'carreras';
    
    function __construct(){
        parent::__construct();
    }
    
    /* GETTER */
    function getIdCarrera(){    return $this->idcarrera;    }
    function getIdTipoCarrera(){   return $this->idtipocarrera;   }
    function getNombre(){ return $this->nombre; }
    
    /* SETTER */
    function setIdCarrera($in){ $this->idcarrera = $in; }
    function setIdTipoCarrera($in){    $this->idtipocarrera = $in;    }
    function setNombre($in){  $this->nombre = $in;  }
    
    /* inicializar la clase */
    function initialize($data = array()){
        if(!empty($data)){
            $this->idcarrera = (!empty($data["idcarrera"])) ? $data["idcarrera"] : NULL;
            $this->idtipocarrera = (!empty($data["idtipocarrera"])) ? $data["idtipocarrera"] : NULL;
            $this->nombre = (!empty($data["nombre"])) ? $data["nombre"] : NULL;
         }
    }
    
    /* OBTIENE USUARIOS SEGÚN LO DATOS DE LA CLASE */
    function get($return = false, $datos = NULL){
        
        if(!empty($datos) && is_array($datos))
            $this->initialize($datos);
            
        if(!is_null($this->idcarrera))
            $this->db->where('idcarrera', $this->idcarrera);
        
        if(!is_null($this->idtipocarrera))
            $this->db->where('idtipocarrera', $this->idtipocarrera);
        
        if(!is_null($this->nombre))
            $this->db->where('nombre', $this->nombre);

        $this->db->order_by('nombre', 'asc');
        
        $query = $this->db->get($this->tabla);
        if ($query->num_rows() > 0){
            $carreras = array();
            foreach ($query->result_array() as $row){
                switch ($return){
                    case 'all':
                    case 'object':
                        $tmp = new Carrera_model();
                        $tmp->idcarrera = $row["idcarrera"];
                        $tmp->idtipocarrera = $row["idtipocarrera"];
                        $tmp->nombre = $row["nombre"];                        
                        if($return === 'object')
                            return $tmp;
                        $carreras[] = $tmp;
                    break;
        
                    
                    case 'id':
                        return $row["idcarrera"];
                    break;
                    
                    case 'boolean':
                        return true;
                    break;
                    
                    default:
                        $this->idcarrera = $row["idcarrera"];
                        $this->idtipocarrera = $row["idtipocarrera"];
                        $this->nombre = $row["nombre"];
                        return true;
                    break;
                    
                }
           }
           return $carreras;
        }else{
            #echo $this->db->last_query();
            return false;
        }
    }
    
    /* INSERTAR UN USUARIO A LA BD, RETORNA EL ID DEL USUARIO INSERTADO; EN CASO DE NO HABER INSERTADO ID = 0 */
    function insert($datos = array()){
        #SIN ID USUARIO.
        if(!empty($datos)){
            $data["idtipocarrera"] = (!empty($datos["idtipocarrera"])) ? $datos["idtipocarrera"] : NULL;
            $data["nombre"] = (!empty($datos["nombre"])) ? $datos["nombre"] : NULL;
        }else
            return false;
        $this->db->insert($this->tabla, $data); 
        return $this->db->insert_id();
    }
    
    function update($values = array()){
        if(!empty($values)){
            
            if(!empty($values["nombre"]))
                $this->db->set('nombre', $values["nombre"]);
            
            if(!empty($values["idtipocarrera"]))
                $this->db->set('idtipocarrera', $values["idtipocarrera"]);    
            
            if(!empty($this->idcarrera))
                $this->db->where('idcarrera', $this->idcarrera); 
            else
                $this->db->where('idcarrera', $values["idcarrera"]);
            
            return $this->db->update($this->tabla);
        }else
            return false;
        
    }

    function delete($idcarrera = NULL){
        if(empty($idcarrera))
            $idcarrera = $this->idcarrera;
            
        if(!empty($idcarrera)){           
            $this->db->where('idcarrera', $idcarrera);
            if($this->db->delete($this->tabla))
                return true;
            else
                return false;
        }else
            return false;
    }

}