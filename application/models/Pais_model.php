<?php

class Pais_model extends CI_Model {
	
	#PAIS
	private $idpais;
	private $nombre;
	private $nacionalidad;

	private $tabla = 'pais';
	
    function __construct($data = array()){
		parent::__construct(); 
    }
	
	function initialize($data = array()){
		if(!empty($data)){
			$this->idpais = (!empty($data["idpais"])) ? $data["idpais"] : NULL;
			$this->nombre = (!empty($data["nombre"])) ? $data["nombre"] : NULL;
			$this->nacionalidad = (!empty($data["nacionalidad"])) ? $data["nacionalidad"] : NULL;
		}
	}
	
	function getIdPais(){	return $this->idpais;	}
	function getNombre(){	return $this->nombre;	}
	function getNacionalidad(){	return $this->nacionalidad;	}
	
	function setIdPais($in){	$this->idpais = $in;	}
	function setNombre($in){	$this->nombre = $in;	}
	function setNacionalidad($in){	$this->nacionalidad = $in;	}
	
	
	function get($return = false, $data = array()){
		if(!empty($data))
			$this->initialize($data);
		
		if(!empty($this->idpais))
			$this->db->where('idpais', $this->idpais);
			
		if(!empty($this->nombre))
			$this->db->where('nombre', $this->nombre);
			
		if(!empty($this->nacionalidad))
			$this->db->where('nacionalidad', $this->nacionalidad);
	
		$this->db->order_by('nombre', 'asc');
		$query = $this->db->get($this->tabla);

		if ($query->num_rows() > 0){
			$paises = array();
		   	foreach ($query->result_array() as $row){
			   switch ($return){
					case 'all':
					case 'object':
						$tmp = new Pais_model();
						$tmp->idpais = $row["idpais"];
						$tmp->nombre = $row["nombre"];
						$tmp->nacionalidad = $row["nacionalidad"];
						if($return == 'object')
							return $tmp;
						$paises[] = $tmp;
		   			break;
					
					case 'array':
						return $row;
					break;
					
					case 'id':
						return $row["idpais"];
					break;
					
					case 'boolean':
						return true;
					break;
					
					default:
						$this->idpais = $row["idpais"];
						$this->nombre = $row["nombre"];
						$this->nacionalidad = $row["nacionalidad"];
						return true;
					break;
					
				}
			  	
			}
			return $paises;
		}
		return false;
	}
	
	
	
}