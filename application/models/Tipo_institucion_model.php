<?php
class Tipo_institucion_model extends CI_Model {

    private $idtipo_institucion;
    private $nombre;
        
    private $tabla = 'tipo_institucion';
    
    function __construct(){
        parent::__construct();
    }
    
    /* GETTER */
    function getIdTipoInstitucion(){    return $this->idtipo_institucion;    }
    function getNombre(){   return $this->nombre;   }

    /* SETTER */
    function setIdTipoInstitucion($in){ $this->idtipo_institucion = $in; }
    function setNombre($in){    $this->nombre = $in;    }
    
    /* inicializar la clase */
    function initialize($data = array()){
        if(!empty($data)){
            $this->idtipo_institucion = (!empty($data["idtipo_institucion"])) ? $data["idtipo_institucion"] : NULL;
            $this->nombre = (!empty($data["nombre"])) ? $data["nombre"] : NULL;
         }
    }
    
    /* OBTIENE USUARIOS SEGÚN LO DATOS DE LA CLASE */
    function get($return = false, $datos = NULL){
        
        if(!empty($datos) && is_array($datos))
            $this->initialize($datos);
            
        if(!empty($this->idtipo_institucion))
            $this->db->where('idtipo_institucion', $this->idtipo_institucion);
        
        if(!empty($this->nombre))
            $this->db->where('nombre', $this->nombre);
        
        $query = $this->db->get($this->tabla);
        if ($query->num_rows() > 0){
            $registros = array();
            foreach ($query->result_array() as $row){
                switch ($return){
                    case 'all':
                    case 'object':
                        $tmp = new Tipo_institucion_model();
                        $tmp->idtipo_institucion = $row["idtipo_institucion"];
                        $tmp->nombre = $row["nombre"];
                        if($return === 'object')
                            return $tmp;
                        $registros[] = $tmp;
                    break;
        
                    
                    case 'id':
                        return $row["idtipo_institucion"];
                    break;
                    
                    case 'boolean':
                        return true;
                    break;
                    
                    default:
                        $this->idtipo_institucion = $row["idtipo_institucion"];
                        $this->nombre = $row["nombre"];
                        return true;
                    break;
                    
                }
           }
           return $registros;
        }else{
            #echo $this->db->last_query();
            return false;
        }
    }
    
    /* INSERTAR UN USUARIO A LA BD, RETORNA EL ID DEL USUARIO INSERTADO; EN CASO DE NO HABER INSERTADO ID = 0 */
    function insert($datos = array()){
        #SIN ID USUARIO.
        if(!empty($datos)){
            $data["nombre"] = (!empty($datos["nombre"])) ? $datos["nombre"] : NULL;
        }else{
            return false;
        }
        $this->db->insert($this->tabla, $data); 
        return $this->db->insert_id();
    }
    
    function update($values = array()){
        if(!empty($values)){
            
            if(!empty($values["nombre"]))
                $this->db->set('nombre', $values["nombre"]);    
                
            if(!empty($this->idtipo_institucion))
                $this->db->where('idtipo_institucion', $this->idtipo_institucion); 
            else
                $this->db->where('idtipo_institucion', $values["idtipo_institucion"]);
            
            return $this->db->update($this->tabla);
        }else
            return false;
        
    }

    function delete($idtipo_institucion = NULL){
        if(empty($idtipo_institucion))
            $idtipo_institucion = $this->idtipo_institucion;
            
        if(!empty($idtipo_institucion)){           
        	$this->db->where('idtipo_institucion', $idtipo_institucion);
            if($this->db->delete($this->tabla))
                return true;
            else
                return false;
        }else
            return false;
    }

}