<?php
class Solicitud_mensaje_model extends CI_Model {

    private $idsolicitud_mensaje;
    private $idsolicitud;
    private $idusuario_ing;
    private $nombre;
    private $descripcion;
    private $fecha_ing;
    private $fecha_lectura_usuario;
    private $fecha_lectura_registro;
        
    private $tabla = 'solicitud_mensaje';
    
    function __construct(){
        parent::__construct();
    }
    
    /* GETTER */
    function getIdSolicitudMensaje(){    return $this->idsolicitud_mensaje;    }
    function getIdUsuarioIng(){    return $this->idusuario_ing;    }
    function getNombre(){   return $this->nombre;   }
    function getIdSolicitud(){ return $this->idsolicitud; }
    function getDescripcion(){ return $this->descripcion; }
    function getFechaIng(){   return $this->fecha_ing;   }
    function getFechaLecturaUsuario(){   return $this->fecha_lectura_usuario;   }
    function getFechaLecturaRegistro(){   return $this->fecha_lectura_registro;   }
    /* SETTER */
    function setIdSolicitudMensaje($in){ $this->idsolicitud_mensaje = $in; }
    function setIdUsuarioIng($in){ $this->idusuario_ing = $in; }
    function setNombre($in){    $this->nombre = $in;    }
    function setIdSolicitud($in){  $this->idsolicitud = $in;  }
    function setDescripcion($in){  $this->descripcion = $in;  }
    function setFechaIng($in){    $this->fecha_ing = $in;    }
    function setFechaLecturaUsuario($in){    $this->fecha_lectura_usuario = $in;    }
    function setFechaLecturaRegistro($in){    $this->fecha_lectura_registro = $in;    }
    
    /* inicializar la clase */
    function initialize($data = array()){
        if(!empty($data)){
            $this->idsolicitud_mensaje = (!empty($data["idsolicitud_mensaje"])) ? $data["idsolicitud_mensaje"] : NULL;
            $this->idusuario_ing = (!empty($data["idusuario_ing"])) ? $data["idusuario_ing"] : NULL;
            $this->idsolicitud = (!empty($data["idsolicitud"])) ? $data["idsolicitud"] : NULL;
            $this->descripcion = (!empty($data["descripcion"])) ? $data["descripcion"] : NULL;
            $this->fecha_ing = (!empty($data["fecha_ing"])) ? $data["fecha_ing"] : NULL;
            $this->fecha_lectura_usuario = (!empty($data["fecha_lectura_usuario"])) ? $data["fecha_lectura_usuario"] : NULL;
            $this->fecha_lectura_registro = (!empty($data["fecha_lectura_registro"])) ? $data["fecha_lectura_registro"] : NULL;
         }
    }
    
    /* OBTIENE USUARIOS SEGÚN LO DATOS DE LA CLASE */
    function get($return = false, $datos = NULL){
        
        if(!empty($datos) && is_array($datos))
            $this->initialize($datos);
            
        if(!is_null($this->idsolicitud_mensaje))
            $this->db->where('idsolicitud_mensaje', $this->idsolicitud_mensaje);

        if(!is_null($this->idusuario_ing))
            $this->db->where('idusuario_ing', $this->idusuario_ing);
      
        if(!is_null($this->idsolicitud))
            $this->db->where('idsolicitud', $this->idsolicitud);
            
        if(!empty($this->descripcion))
            $this->db->where('descripcion', $this->descripcion);
        
        $query = $this->db->get($this->tabla);
        if ($query->num_rows() > 0){
            $solicitudes = array();
            foreach ($query->result_array() as $row){
                switch ($return){
                    case 'all':
                    case 'object':
                        $tmp = new Solicitud_mensaje_model();
                        $tmp->idsolicitud_mensaje = $row["idsolicitud_mensaje"];
                        $tmp->idusuario_ing = $row["idusuario_ing"];
                        $tmp->idsolicitud = $row["idsolicitud"];
                        $tmp->descripcion = $row["descripcion"];
                        $tmp->fecha_lectura_usuario = $row["fecha_lectura_usuario"];
                        $tmp->fecha_ing = $row["fecha_ing"];
                        $tmp->fecha_lectura_registro = $row["fecha_lectura_registro"];
                        if($return === 'object')
                            return $tmp;
                        $solicitudes[] = $tmp;
                    break;
        
                    
                    case 'id':
                        return $row["idsolicitud_mensaje"];
                    break;
                    
                    case 'boolean':
                        return true;
                    break;
                    
                    default:
                        $this->idsolicitud_mensaje = $row["idsolicitud_mensaje"];
                        $this->idusuario_ing = $row["idusuario_ing"];
                        $this->nombre = $row["nombre"];
                        $this->idsolicitud = $row["idsolicitud"];
                        $this->descripcion = $row["descripcion"];
                        $this->fecha_ing = $row["fecha_ing"];
                        $this->fecha_lectura_usuario = $row["fecha_lectura_usuario"];
                        $this->fecha_lectura_registro = $row["fecha_lectura_registro"];
                        return true;
                    break;
                    
                }
           }
           return $solicitudes;
        }else{
            #echo $this->db->last_query();
            return false;
        }
    }
    
    /* INSERTAR UN USUARIO A LA BD, RETORNA EL ID DEL USUARIO INSERTADO; EN CASO DE NO HABER INSERTADO ID = 0 */
    function insert($datos = array()){
        #SIN ID USUARIO.
        if(!empty($datos)){
            $data["idusuario_ing"] = (!empty($datos["idusuario_ing"])) ? $datos["idusuario_ing"] : NULL;
            $data["nombre"] = (!empty($datos["nombre"])) ? $datos["nombre"] : NULL;
            $data["idsolicitud"] = (!empty($datos["idsolicitud"])) ? $datos["idsolicitud"] : NULL;
            $data["descripcion"] = (!empty($datos["descripcion"])) ? $datos["descripcion"] : NULL;
            $data["fecha_lectura_usuario"] = (!empty($datos["fecha_lectura_usuario"])) ? $datos["fecha_lectura_usuario"] : NULL;
            $data["fecha_ing"] = (!empty($datos["fecha_ing"])) ? $datos["fecha_ing"] : NULL;
            $data["fecha_lectura_registro"] = (!empty($datos["fecha_lectura_registro"])) ? $datos["fecha_lectura_registro"] : NULL;
        }else{
            return false;
        }
        $this->db->insert($this->tabla, $data); 
        return $this->db->insert_id();
    }
    
    function update($values = array()){
        if(!empty($values)){
            
            if(!empty($values["idusuario_ing"]))
                $this->db->set('idusuario_ing', $values["idusuario_ing"]);

            if(!empty($values["idsolicitud"]))
                $this->db->set('idsolicitud', $values["idsolicitud"]);
            
            if(!empty($values["nombre"]))
                $this->db->set('nombre', $values["nombre"]);    
                
            if(!empty($values["descripcion"]))
                $this->db->set('descripcion', $values["descripcion"]);
            
            if(!empty($values["fecha_ing"]))       
                $this->db->set('fecha_ing', $values["fecha_ing"]);

            if(!empty($values["fecha_lectura_usuario"]))       
                $this->db->set('fecha_lectura_usuario', $values["fecha_lectura_usuario"]);

            if(!empty($values["fecha_lectura_registro"]))       
                $this->db->set('fecha_lectura_registro', $values["fecha_lectura_registro"]);
            
            if(!empty($this->idsolicitud_mensaje))
                $this->db->where('idsolicitud_mensaje', $this->idsolicitud_mensaje); 
            else
                $this->db->where('idsolicitud_mensaje', $values["idsolicitud_mensaje"]);
            
            return $this->db->update($this->tabla);
        }else
            return false;
        
    }

    function delete($idsolicitud_mensaje = NULL){
        if(empty($idsolicitud_mensaje))
            $idsolicitud_mensaje = $this->idsolicitud_mensaje;
            
        if(!empty($idsolicitud_mensaje)){           
            $this->db->where('idsolicitud_mensaje', $idsolicitud_mensaje);
            if($this->db->delete($this->tabla))
                return true;
            else
                return false;
        }else
            return false;
    }

}