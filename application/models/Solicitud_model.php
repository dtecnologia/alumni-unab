<?php
class Solicitud_model extends CI_Model {

    private $idsolicitud;
    private $idregistro;
    private $idtipo_solicitud;
    private $email;
    private $nombre;
    private $descripcion;
    private $estado;
    private $fecha_ing;
    private $fecha_mod;
    private $idusuario_mod;
        
    private $tabla = 'solicitud';
    
    function __construct(){
        parent::__construct();
    }
    
    /* GETTER */
    function getIdSolicitud(){    return $this->idsolicitud;    }
    function getIdTipoSolicitud(){    return $this->idtipo_solicitud;    }
    function getEmail(){   return $this->email;   }
    function getNombre(){   return $this->nombre;   }
    function getIdRegistro(){ return $this->idregistro; }
    function getDescripcion(){ return $this->descripcion; }
    function getEstado($texto = true){  
        if(!$texto) 
            return $this->estado;
        switch($this->estado){
            case "INGRE":
                return "Ingresado";
            break;
            case "CURSO":
                return "En curso";
            break;
            case "APROB":
                return "Aprobado";
            break;
            case "FINAL":
                return "Finalizado";
            break;
            default:
                return "En espera";
            break;
        }   
    }
    function getFechaIng(){   return $this->fecha_ing;   }
    function getFechaMod(){   return $this->fecha_mod;   }
    function getIdUsuarioMod(){   return $this->idusuario_mod;   }
    /* SETTER */
    function setIdSolicitud($in){ $this->idsolicitud = $in; }
    function setIdTipoSolicitud($in){ $this->idtipo_solicitud = $in; }
    function setNombre($in){    $this->nombre = $in;    }
    function setIdRegistro($in){  $this->idregistro = $in;  }
    function setDescripcion($in){  $this->descripcion = $in;  }
    function setEstado($in){    $this->estado = $in;    }
    function setFechaIng($in){    $this->estado = $in;    }
    function setFechaMod($in){    $this->estado = $in;    }
    function setIdUsuarioMod($in){    $this->idusuario_mod = $in;    }
    
    /* inicializar la clase */
    function initialize($data = array()){
        if(!empty($data)){
            $this->idsolicitud = (!empty($data["idsolicitud"])) ? $data["idsolicitud"] : NULL;
            $this->idtipo_solicitud = (!empty($data["idtipo_solicitud"])) ? $data["idtipo_solicitud"] : NULL;
            $this->nombre = (!empty($data["nombre"])) ? $data["nombre"] : NULL;
            $this->email = (!empty($data["email"])) ? $data["email"] : NULL;
            $this->idregistro = (!empty($data["idregistro"])) ? $data["idregistro"] : NULL;
            $this->descripcion = (!empty($data["descripcion"])) ? $data["descripcion"] : NULL;
            $this->estado = (!empty($data["estado"])) ? $data["estado"] : NULL;
            $this->fecha_ing = (!empty($data["fecha_ing"])) ? $data["fecha_ing"] : NULL;
            $this->fecha_mod = (!empty($data["fecha_mod"])) ? $data["fecha_mod"] : NULL;
            $this->idusuario_mod = (!empty($data["idusuario_mod"])) ? $data["idusuario_mod"] : NULL;
         }
    }
    
    /* OBTIENE USUARIOS SEGÚN LO DATOS DE LA CLASE */
    function get($return = false, $datos = NULL, $custom = NULL){
        
        if(!empty($datos) && is_array($datos))
            $this->initialize($datos);
            
        if(!empty($this->idsolicitud))
            $this->db->where('idsolicitud', $this->idsolicitud);

        if(!empty($this->idtipo_solicitud))
            $this->db->where('idtipo_solicitud', $this->idtipo_solicitud);
        
        if(!empty($this->email))
            $this->db->where('email', $this->email);

        if(!empty($this->nombre))
            $this->db->where('nombre', $this->nombre);
        
        if(!empty($this->idregistro))
            $this->db->where('idregistro', $this->idregistro);
            
        if(!empty($this->descripcion))
            $this->db->where('descripcion', $this->descripcion);
            
        if(!empty($this->estado))
            $this->db->where('estado', $this->estado);
        
        if(!empty($custom) && is_array($custom))
        {
            if(!empty($custom["order"]))
                $this->db->order_by($custom["order"]); 
            if(is_numeric($custom["limit"]) && is_numeric($custom["pagina"]))
                $this->db->limit($custom["limit"], (($custom["limit"] * $custom["pagina"]) - $custom["limit"]));
            if(is_numeric($custom["limit"]))
                $this->db->limit($custom["limit"]);
        }
        
        $query = $this->db->get($this->tabla);
        if ($query->num_rows() > 0){
            if($return == 'count')
            {
                return $query->num_rows();
            }
            $solicitudes = array();
            foreach ($query->result_array() as $row){
                switch ($return){
                    case 'all':
                    case 'object':
                        $tmp = new Solicitud_model();
                        $tmp->idsolicitud = $row["idsolicitud"];
                        $tmp->idtipo_solicitud = $row["idtipo_solicitud"];
                        $tmp->nombre = $row["nombre"];
                        $tmp->email = $row["email"];
                        $tmp->idregistro = $row["idregistro"];
                        $tmp->descripcion = $row["descripcion"];
                        $tmp->estado = $row["estado"];
                        $tmp->fecha_mod = $row["fecha_mod"];
                        $tmp->fecha_ing = $row["fecha_ing"];
                        $tmp->idusuario_mod = $row["idusuario_mod"];
                        if($return === 'object')
                            return $tmp;
                        $solicitudes[] = $tmp;
                    break;
        
                    
                    case 'id':
                        return $row["idsolicitud"];
                    break;
                    
                    case 'boolean':
                        return true;
                    break;
                    
                    default:
                        $this->idsolicitud = $row["idsolicitud"];
                        $this->idtipo_solicitud = $row["idtipo_solicitud"];
                        $this->nombre = $row["nombre"];
                        $this->email = $row["email"];
                        $this->idregistro = $row["idregistro"];
                        $this->descripcion = $row["descripcion"];
                        $this->estado = $row["estado"];
                        $this->fecha_ing = $row["fecha_ing"];
                        $this->fecha_mod = $row["fecha_mod"];
                        $this->idusuario_mod = $row["idusuario_mod"];
                        return true;
                    break;
                    
                }
           }
           return $solicitudes;
        }else{
            #echo $this->db->last_query();
            return false;
        }
    }
    
    /* INSERTAR UN USUARIO A LA BD, RETORNA EL ID DEL USUARIO INSERTADO; EN CASO DE NO HABER INSERTADO ID = 0 */
    function insert($datos = array()){
        #SIN ID USUARIO.
        if(!empty($datos)){
            $data["idtipo_solicitud"] = (!empty($datos["idtipo_solicitud"])) ? $datos["idtipo_solicitud"] : NULL;
            $data["email"] = (!empty($datos["email"])) ? $datos["email"] : NULL;
            $data["nombre"] = (!empty($datos["nombre"])) ? $datos["nombre"] : NULL;
            $data["idregistro"] = (!empty($datos["idregistro"])) ? $datos["idregistro"] : NULL;
            $data["descripcion"] = (!empty($datos["descripcion"])) ? $datos["descripcion"] : NULL;
            $data["estado"] = (!empty($datos["estado"])) ? $datos["estado"] : 'INGRE';
            $data["fecha_mod"] = (!empty($datos["fecha_mod"])) ? $datos["fecha_mod"] : NULL;
            $data["fecha_ing"] = (!empty($datos["fecha_ing"])) ? $datos["fecha_ing"] : date('Y-m-d H:i:s');
            $data["idusuario_mod"] = (!empty($datos["idusuario_mod"])) ? $datos["idusuario_mod"] : NULL;
        }else{
            return false;
        }
        $this->db->insert($this->tabla, $data); 
        return $this->db->insert_id();
    }
    
    function update($values = array()){
        if(!empty($values)){
            
            if(!empty($values["idtipo_solicitud"]))
                $this->db->set('idtipo_solicitud', $values["idtipo_solicitud"]);

            if(!empty($values["idregistro"]))
                $this->db->set('idregistro', $values["idregistro"]);
            
            if(!empty($values["nombre"]))
                $this->db->set('nombre', $values["nombre"]);    

             if(!empty($values["email"]))
                $this->db->set('email', $values["email"]);    
                
            if(!empty($values["descripcion"]))
                $this->db->set('descripcion', $values["descripcion"]);
                
            if(!empty($values["estado"]))       
                $this->db->set('estado', $values["estado"]);

            if(!empty($values["fecha_ing"]))       
                $this->db->set('fecha_ing', $values["fecha_ing"]);

            if(!empty($values["idusuario_mod"]))       
                $this->db->set('idusuario_mod', $values["idusuario_mod"]);

            $this->db->set('fecha_mod', date('Y-m-d H:i:s'));
            
            if(!empty($this->idsolicitud))
                $this->db->where('idsolicitud', $this->idsolicitud); 
            else
                $this->db->where('idsolicitud', $values["idsolicitud"]);
            
            return $this->db->update($this->tabla);
        }else
            return false;
        
    }

    function delete($idsolicitud = NULL){
        if(empty($idsolicitud))
            $idsolicitud = $this->idsolicitud;
            
        if(!empty($idsolicitud)){           
            $this->db->where('idsolicitud', $idsolicitud);
            if($this->db->delete($this->tabla))
                return true;
            else
                return false;
        }else
            return false;
    }

}