<?php
class Usuario_model extends CI_Model {

    private $idusuario;
    private $nombre;
    private $user;
    private $pass;
    private $activo;
        
    private $tabla = 'usuario';
    
    function __construct(){
        parent::__construct();
    }
    
    /* GETTER */
    function getIdUsuario(){    return $this->idusuario;    }
    function getNombre(){   return $this->nombre;   }
    function getUser(){ return $this->user; }
    function getPass(){ return $this->pass; }
    function getActivo(){   return $this->activo;   }
    /* SETTER */
    function setIdUsuario($in){ $this->idusuario = $in; }
    function setNombre($in){    $this->nombre = $in;    }
    function setUser($in){  $this->user = $in;  }
    function setPass($in){  $this->pass = $in;  }
    function setActivo($in){    $this->activo = $in;    }
    
    /* inicializar la clase */
    function initialize($data = array()){
        if(!empty($data)){
            $this->idusuario = (!empty($data["idusuario"])) ? $data["idusuario"] : NULL;
            $this->nombre = (!empty($data["nombre"])) ? $data["nombre"] : NULL;
            $this->user = (!empty($data["user"])) ? $data["user"] : NULL;
            $this->pass = (!empty($data["pass"])) ? $data["pass"] : NULL;
            $this->activo = (isset($data["activo"])) ? $data["activo"] : NULL;
         }
    }
    
    /* OBTIENE USUARIOS SEGÚN LO DATOS DE LA CLASE */
    function get($return = false, $datos = NULL){
        
        if(!empty($datos) && is_array($datos))
            $this->initialize($datos);
            
        if(!is_null($this->idusuario))
            $this->db->where('idusuario', $this->idusuario);
        
        if(!empty($this->nombre))
            $this->db->where('nombre', $this->nombre);
        
        if(!empty($this->user))
            $this->db->where('user', $this->user);
            
        if(!empty($this->pass))
            $this->db->where('pass', md5($this->pass));
            
        if(is_numeric($this->activo))
            $this->db->where('activo', $this->activo);
        
        
        $query = $this->db->get($this->tabla);
        if ($query->num_rows() > 0){
            $usuarios = array();
            foreach ($query->result_array() as $row){
                switch ($return){
                    case 'all':
                    case 'object':
                        $tmp = new Usuario_model();
                        $tmp->idusuario = $row["idusuario"];
                        $tmp->nombre = $row["nombre"];
                        $tmp->user = $row["user"];
                        $tmp->pass = $row["pass"];
                        $tmp->activo = $row["activo"];
                        if($return === 'object')
                            return $tmp;
                        $usuarios[] = $tmp;
                    break;
        
                    
                    case 'id':
                        return $row["idusuario"];
                    break;
                    
                    case 'boolean':
                        return true;
                    break;
                    
                    default:
                        $this->idusuario = $row["idusuario"];
                        $this->nombre = $row["nombre"];
                        $this->user = $row["user"];
                        $this->pass = $row["pass"];
                        $this->activo = $row["activo"];
                        return true;
                    break;
                    
                }
           }
           return $usuarios;
        }else{
            #echo $this->db->last_query();
            return false;
        }
    }
    
    /* INSERTAR UN USUARIO A LA BD, RETORNA EL ID DEL USUARIO INSERTADO; EN CASO DE NO HABER INSERTADO ID = 0 */
    function insert($datos = array()){
        #SIN ID USUARIO.
        if(!empty($datos)){
            $data["nombre"] = (!empty($datos["nombre"])) ? $datos["nombre"] : NULL;
            $data["user"] = (!empty($datos["user"])) ? $datos["user"] : NULL;
            $data["pass"] = (!empty($datos["pass"])) ? md5($datos["pass"]) : NULL;
            $data["activo"] = (!is_null($datos["activo"])) ? $datos["activo"] : NULL;
        }else{
            return false;
        }
        $this->db->insert($this->tabla, $data); 
        return $this->db->insert_id();
    }
    
    function update($values = array()){
        if(!empty($values)){
            
            if(!empty($values["user"]))
                $this->db->set('user', $values["user"]);
            
            if(!empty($values["nombre"]))
                $this->db->set('nombre', $values["nombre"]);    
                
            if(!empty($values["pass"]))
                $this->db->set('pass', md5($values["pass"]));
                
            if(!empty($values["activo"]))       
                $this->db->set('activo', $values["activo"]);
            
            if(!empty($this->idusuario))
                $this->db->where('idusuario', $this->idusuario); 
            else
                $this->db->where('idusuario', $values["idusuario"]);
            
            return $this->db->update($this->tabla);
        }else
            return false;
        
    }

}