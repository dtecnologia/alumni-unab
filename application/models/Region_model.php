<?php

class Region_model extends CI_Model {
	
	#PAIS
	private $idregion;
	private $nombre;
	private $idpais;

	private $tabla = 'region';
	
    function __construct($data = array()){
		parent::__construct(); 
    }
	
	function initialize($data = array()){
		if(!empty($data)){
			$this->idregion = (!empty($data["idregion"])) ? $data["idregion"] : NULL;
			$this->nombre = (!empty($data["nombre"])) ? $data["nombre"] : NULL;
			$this->idpais = (!empty($data["idpais"])) ? $data["idpais"] : NULL;
		}
	}
	
	function getIdRegion(){	return $this->idregion;	}
	function getNombre(){	return $this->nombre;	}
	function getIdPais(){	return $this->idpais;	}
	
	function setIdRegion($in){	$this->idregion = $in;	}
	function setNombre($in){	$this->nombre = $in;	}
	function setIdPais($in){	$this->idpais = $in;	}
	
	function get($return = false, $data = array()){
		if(!empty($data))
			$this->initialize($data);
		
		if(!empty($this->idregion))
			$this->db->where('idregion', $this->idregion);
			
		if(!empty($this->nombre))
			$this->db->where('nombre', $this->nombre);
			
		if(!empty($this->idpais))
			$this->db->where('idpais', $this->idpais);
		
		$this->db->order_by('nombre', 'asc');
		$query = $this->db->get($this->tabla);
		if ($query->num_rows() > 0){
			$regiones = array();
		   	foreach ($query->result_array() as $row){
			   switch ($return){
					case 'all':
					case 'object':
						$tmp = new Region_model();
						$tmp->idpais = $row["idpais"];
						$tmp->idregion = $row["idregion"];
						$tmp->nombre = $row["nombre"];
						if($return == 'object')
							return $tmp;
						$regiones[] = $tmp;
		   			break;
					
					case 'array':
						return $row;
					break;
					
					case 'id':
						return $row["idregion"];
					break;
					
					case 'boolean':
						return true;
					break;
					
					default:
						$this->idpais = $row["idpais"];
						$this->idregion = $row["idregion"];
						$this->nombre = $row["nombre"];
						return true;
					break;
					
				}  	
			}
			return $regiones;
		}
		return false;
	}
	
	
	
}