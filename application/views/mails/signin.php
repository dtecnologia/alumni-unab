<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Mailing Nuevo Registro</title>
</head>

<body>
<div style="margin:0 auto; width: 100%; height:100%">
  <div style="background-color: #C21A01; width:610px; height:100px;margin:0 auto; display: block"> <img src="<?php echo base_url(); ?>assets/img/logo.png" height="80" style="padding: 10px;"> </div>
  
  <div 
	 style="margin: 0 auto;
	background-color: #EFEFEF;
	box-shadow: 0px 2px 2px #CCC;
	border-radius: 5px;
	width:550px;
	padding:30px"> 
    
    <p style="
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	line-height: 20px;
	color: #333;
	margin: 0 0 10px;
	font-weight: lighter;
	">Estimado(a) <?php echo $registro->getNombreCompleto(); ?></p>
      <br>
       <p style="
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	line-height: 20px;
	color: #333;
	margin: 0 0 10px;
	font-weight: lighter;
	">Usted ha realizado una solicitud de registro el cual será verificado por equipo alumni para una posterior aprobación o rechazo, mientras se gestiona su solicitud de igual forma puede ingresar a la plataforma con los siguientes datos.</p>
    <p style="
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	line-height: 20px;
	color: #333;
	margin: 0 0 10px;
	font-weight: lighter;
	"><br>
      <b>Usuario / Rut: </b> <?php echo isRut($registro->getRut()); ?><br>
      <b>Contraseña: </b> <?php echo $registro->getPassword(); ?><br>
      <br>
      <br>
    </p>
    <span style="
	  background-color: #C21A01;

	display: inline-block;
	padding: 8px 12px;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11.844px;
	font-weight: normal;
	line-height: 14px;
	color: #FFF;
	text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
	white-space: nowrap;
	vertical-align: baseline;
	  "> <a style="color:inherit; text-decoration:none; color:#FFF" href="<?php echo base_url(); ?>" target="_blank">Ingresar a Alumni</a> para verificar su estado de solicitud. </span></div>
  <div 
 style="margin: 0 auto; background-color: #c2c2c2; box-shadow: 0px 2px 2px #CCC; width: 550px; padding: 10px 30px">
 <img src="<?php echo base_url(); ?>assets/img/logo.png" height="43"><br />
  <a style="
font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
line-height: 20px;
color: #333;
margin: 0 30px 10px 0;
font-weight: lighter;
color:#FFF;
" href="<?php echo base_url(); ?>" target="_blank"><?php echo base_url(); ?></a> <a style="
font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
line-height: 20px;
color: #333;
margin: 0 0 10px;
font-weight: lighter;
color:#FFF;
" href="mailto:unab@alumni.cl" target="_blank">unab@alumni.cl</a>
    <p style=" font-family:Arial, Helvetica, sans-serif; font-size:9px; color:#FFF">El contenido del presente mensaje y/o sus archivos adjuntos, son privados, estrictamente confidenciales y exclusivo para sus destinatarios, pudiendo contener información protegida por normas legales y de secreto profesional. Por ninguna circunstancia, su contenido puede ser transmitido a terceros ni divulgado en forma alguna. Si este correo lo ha recibido por error, solicitamos eliminarlo de su sistema</p>
  </div>
</div>
</body>
</html>