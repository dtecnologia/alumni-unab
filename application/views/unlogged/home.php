<div class="col-md-9 col-sm-12 col-xs-12 pull-right" style="background-color: #EFEFEF; padding: 20px;">
    <div class="row">
        <div class="col-md-8  col-sm-7 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Ingresa a AlumniUnab</h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" name="form_login" id="form_login" role="form" action="/login" method="post">
                        <div class="form-group">
                            <label for="user" class="col-sm-3 control-label">Usuario</label>
                            <div class="col-sm-9">
                                <input class="form-control" name="user" value="<?php echo set_value('user'); ?>" placeholder="E-mail, RUT o Nickname" type="text">
                                <?php echo form_error('user', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pass" class="col-sm-3 control-label">Contraseña</label>
                            <div class="col-sm-9">
                                <input class="form-control" name="pass" placeholder="Contraseña" type="password">
                                <?php echo form_error('pass', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <!-- <button type="submit" class="btn btn-default">Ingresar</button> -->
                                <button type="submit" class="btn btn-default">Ingresar</button>
                                <a href="<?php echo base_url(); ?>login/recovery" class="btn btn-link">Olvidé mi contraseña</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="pull-left">
                <a href="<?php echo base_url(); ?>help" class="btn btn-primary">Soy Alumni-Unab, necesito ayuda</a> &nbsp; 
                <a href="<?php echo base_url(); ?>signin" class="btn btn-default">Soy nuevo en Alumni-Unab</a>
            </div>
        </div>
        <div class="col-md-4 col-sm-5 col-xs-12">
            <img style="margin:0 auto;" src="<?php echo base_url(); ?>assets/img/concepto.png" class="img-responsive">
        </div>
    </div>
</div>