<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1">
        <title>AlumniUnab - Gestión de exalumnos</title>
        <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datepicker.js"></script>
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" type="text/css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.gray-blue.css" type="text/css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>assets/css/datepicker.css" type="text/css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>assets/css/html.css" type="text/css" rel="stylesheet"/>
    </head>
    <body style="background-image: url(<?php echo base_url(); ?>assets/img/imagen-fondo-alumni.jpg);">
        <div class="container">
            <div class="row" style="height: 130px; padding: 15px 60px; background-color: #C21A01; margin-bottom:30px">
                <a href="<?php echo base_url(); ?>"><img class="img-responsive" src="<?php echo base_url(); ?>assets/img/logo.png" style="max-height:100px" /></a>
            </div>
            <?php echo $body; ?>
        </div>
    </body>

</html>