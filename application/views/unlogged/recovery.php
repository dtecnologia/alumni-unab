<div class="col-lg-6 col-md-8 col-sm-10" style="background-color: #EFEFEF; padding: 20px; margin:0 auto; float:none">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Restablecer contraseña</h3>
        </div>
        <div class="panel-body">
            <p>Ingresa tu e-mail de registro, luego haz clic en el botón Restablecer. Te enviaremos una nueva contraseña al e-mail ingresado. Si deseas recuperar la contraseña de un usuario administrativo, contáctate con el administrador general.<br><br></p>
            <form class="form-horizontal" role="form" action="" method="post">
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">E-mail</label>
                    <div class="col-sm-9">
                        <input class="form-control" id="email" name="email" placeholder="E-mail" type="text" value="<?php echo set_value('email'); ?>">
                        <?php echo form_error('email', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>              
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-default">Restablecer</button>
                        <a href="<?php echo base_url(); ?>" type="button" class="btn btn-link"> &larr; Volver al inicio</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php if(!empty($message)){ ?>
    <div class="alert alert-<?php echo $message["status"]; ?> alert-dismissible fade in" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>Acción realizada: </strong> <?php echo $message["message"]; ?>.
    </div>
    <?php } ?>
</div>