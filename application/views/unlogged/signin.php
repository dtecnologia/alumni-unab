<div class="col-lg-6 col-md-8" style="background-color: #EFEFEF; padding: 20px; margin:0 auto; float:none">
    <div class="panel panel-default" style="margin: 0;">
        <div class="panel-heading">
            <h3 class="panel-title">Registrarme en AlumniUnab</h3>
        </div>
        <div class="panel-body">
            <p>Completa el siguiente formulario, luego haz clic en el botón Registrarme. En caso que estes completamente habilitado para acceder a AlumniUnab, te enviaremos las contraseñas a tu correo electrónico. 
            En caso que no estés habilitado, el equipo de AlumniUnab se comunicará contigo para realizar la gestión.<br><br></p>
            <form class="form-horizontal" role="form" action="" method="post">
                <div class="form-group">
                    <label for="rut" class="col-sm-4 control-label">R.U.T.</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="rut" name="rut" value="<?php echo set_value('rut'); ?>" type="text">
                        <?php echo form_error('rut', '<small class="text-danger">','</small>'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nombre" class="col-sm-4 control-label">Nombre Completo</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="nombre" name="nombre" type="text" value="<?php echo set_value('nombre'); ?>">
                        <?php echo form_error('nombre', '<small class="text-danger">','</small>'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-4 control-label">E-mail</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="email" name="email" type="text" value="<?php echo set_value('email'); ?>">
                        <?php echo form_error('email', '<small class="text-danger">','</small>'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="pass" class="col-sm-4 control-label">Contraseña</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="pass" name="pass" type="password">
                        <?php echo form_error('pass', '<small class="text-danger">','</small>'); ?>
                    </div>
                </div>  
                <div class="form-group">
                    <label for="repass" class="col-sm-4 control-label">Repetir contraseña</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="repass" name="repass" type="password">
                        <?php echo form_error('repass', '<small class="text-danger">','</small>'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-7">
                        <button type="submit" class="btn btn-default">Registrarme</button>
                        <a href="<?php echo base_url(); ?>" type="button" class="btn btn-link"> &larr; Volver al inicio</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php if(!empty($message)){ ?>
    <div class="alert alert-<?php echo $message["status"]; ?> alert-dismissible fade in" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>Acción realizada: </strong> <?php echo $message["message"]; ?>.
    </div>
    <?php } ?>
</div>