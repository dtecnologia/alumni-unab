<div class="col-lg-6" style="background-color: #EFEFEF; padding: 20px; margin:0 auto 20px auto; float:none">
    <div class="panel panel-default" style="margin: 0;">
        <div class="panel-heading">
            <h3 class="panel-title">Te podemos ayudar</h3>
        </div>
        <div class="panel-body">
            <p>Completa el siguiente formulario, seleccionando la opción que más se acerque a tu problema, pronto nos comunicaremos contigo.<br><br></p>
            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="asunto" class="col-sm-4 control-label">Asunto</label>
                    <div class="col-sm-7">
                    
                        <select class="form-control" id="asunto" name="tipo_solicitud">
                            <option value="">Selecciona una opción</option>
                        <?php foreach($tipo_solicitudes as $tipo_solicitud){ ?>
                            <option value="<?php echo $tipo_solicitud->getIdTipoSolicitud(); ?>" <?php if(set_value('tipo_solicitud') == $tipo_solicitud->getIdTipoSolicitud()) echo 'selected'; ?>><?php echo $tipo_solicitud->getNombre(); ?></option>
                        <?php } ?>    
                          
                        </select>
                        <?php echo form_error('tipo_solicitud', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="rut" class="col-sm-4 control-label">R.U.T.</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="rut" name="rut" value="<?php echo set_value('rut'); ?>" placeholder="Ej. 17886390-K" type="text">
                        <?php echo form_error('rut', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nombre" class="col-sm-4 control-label">Nombre Completo</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="nombre" name="nombre" value="<?php echo set_value('nombre'); ?>" type="text">
                        <?php echo form_error('nombre', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-4 control-label">E-mail</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="email" name="email" type="email" value="<?php echo set_value('email'); ?>">
                        <?php echo form_error('email', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="descripcion" class="col-sm-4 control-label">Descripción</label>
                    <div class="col-sm-7">
                        <textarea id="descripcion" name="descripcion" class="form-control" rows="6"><?php echo set_value('descripcion'); ?></textarea>
                        <?php echo form_error('descripcion', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>  
                <!-- <div class="form-group">
                    <label for="adjunto" class="col-sm-4 control-label">Adjunto (.jpg, .png)</label>
                    <div class="col-sm-7">
                        <input type="file" id="adjunto" name="userfile" class="form-control">
                    </div>
                </div>   
                -->
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-7">
                        <button type="submit" class="btn btn-default">Enviar</button>
                        <a href="<?php echo base_url(); ?>" type="button" class="btn btn-link"> &larr; Volver al inicio</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php if(!empty($message)){ ?>
    <div class="alert alert-<?php echo $message["status"]; ?> fade in" id="request-alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <span id="request-message"><strong>Acción realizada: </strong><?php echo $message["message"]; ?></span>
    </div>
    <?php } ?>
</div>