
<tr onclick="checked('laboral', '<?php echo $row->getIdRegistroLaboral(); ?>');">
	<td><input type="checkbox" data-name="idregistro_laboral" name="idregistro_laboral[]" id="laboral-<?php echo $row->getIdRegistroLaboral(); ?>" value="<?php echo $row->getIdRegistroLaboral(); ?>"></td>
	<td><?php echo $row->getEmpresa(); ?></td>
	<td><?php if($row->getFechaInicio() == '') 
            echo "Sin fechas"; 
        else{ 
            echo date('m-Y', strtotime($row->getFechaInicio()));
            if($row->getFechaTermino() == '')
                echo ' a la fecha';
            else
                echo " al ".date('m-Y', strtotime($row->getFechaTermino())); 
        } ?></td>
</tr>