<tr onclick="checked('academico', '<?php echo $registro_academico->getIdRegistroAcademico(); ?>');">
	<td><input type="checkbox" data-name="idregistro_academico" name="idregistro_academico[]" id="academico-<?php echo $registro_academico->getIdRegistroAcademico(); ?>" value="<?php echo $registro_academico->getIdRegistroAcademico(); ?>"></td>
	<td><?php echo $institucion->getNombre(); ?></td>
	<td><?php if($registro_academico->getFechaInicio() == '') 
            echo "Sin fechas"; 
        else{ 
            echo date('m-Y', strtotime($registro_academico->getFechaInicio()));
            if($registro_academico->getFechaTermino() == '')
                echo ' a la fecha';
            else
                echo " al ".date('m-Y', strtotime($registro_academico->getFechaTermino())); 
        } ?></td>
</tr>