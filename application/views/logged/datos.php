<!-- Header start -->
<header>
    <div class="row">
        <div class="col-sm-12">
            <h3 class="pull-left" style="color:#C21A01;"><i class="glyphicon glyphicon-edit"></i> Mi Cuenta</h3>
        </div>
    </div>
</header>
<!---- Header end -->

<!-- Content start -->
<div id="content">
    <section id="navigation">
        <div class="container-fluid" style="padding-top:30px;">
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Datos Personales</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <!-- RUT -->
                                <div class="form-group">
                                    <label for="rut" class="col-sm-3 control-label">Rut</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="rut" id="rut" placeholder="Ej. 17072799-1" type="text">
                                    </div>
                                </div>
                                <!-- Nombre Completo-->
                                <div class="form-group">
                                    <label for="nombre" class="col-sm-3 control-label">Nombre Completo</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="nombre" id="nombre" placeholder="Ej. Danilo Armando Aranda Fernández" type="text">
                                    </div>
                                </div>
                                <!-- Apellidos -->
                                <!-- <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Rut</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" id="inputEmail3" placeholder="Email" type="email">
                                    </div>
                                </div> -->
                                <!-- Email -->
                                <div class="form-group">
                                    <label for="email" class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="email" id="email" placeholder="Ej. dann.aranda@gmail.com" type="email">
                                    </div>
                                </div>
                                <!-- Fecha Nacimiento-->
                                <div class="form-group">
                                    <label for="fechanacimiento" class="col-sm-3 control-label">Fecha Nacimiento</label>
                                    <div class="col-sm-5">
                                        <div class="input-group date datepicker">
                                            <input class="form-control" id="fechanacimiento" name="fechanacimiento" placeholder="dd/mm/yyyy" data-format="dd/mm/yyyy" type="text">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <!-- Sexo-->
                                <div class="form-group">
                                    <label for="sexo" class="col-sm-3 control-label">Género</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="sexo" name="sexo">
                                            <option value="">-- Selecciona Género --</option>
                                            <option value="M">Femenino</option>
                                            <option value="H">Masculino</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <!-- Nacionalidad-->
                                <div class="form-group">
                                    <label for="pais" class="col-sm-3 control-label">Pais natal</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="pais" name="pais">
                                            <option value="">-- Selecciona pais --</option>
                                            <option value="56">Chile</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Datos residenciales</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                
                                <div class="form-group">
                                    <label for="region" class="col-sm-3 control-label">Región</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="region" name="region">
                                            <option value="">-- Selecciona región --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ciudad" class="col-sm-3 control-label">Ciudad</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="ciudad" name="ciudad">
                                            <option value="">-- Selecciona ciudad --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="comuna" class="col-sm-3 control-label">Comuna</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="comuna" name="comuna">
                                            <option value="">-- Selecciona comuna --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="direccion" class="col-sm-3 control-label">Dirección</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" id="direccion" name="direccion" placeholder="Ej. General jofré 388 of. A" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="telefono" class="col-sm-3 control-label">Teléfono fijo</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" id="telefono" name="telefono" placeholder="Ej. 27763511" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="celular" class="col-sm-3 control-label">Teléfono móvil</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" id="celular" name="celular" placeholder="Ej. 83716389" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Antecedentes Laborales</strong></h3>
                        </div>
                        <div class="panel-body">
                            BODY
                        </div>
                    </div>
                </div>    
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Antecedentes Laborales</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label for="area" class="col-sm-3 control-label">Área de Desempeño</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="area" name="area">
                                            <option value="">-- Selecciona área --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="empresa" class="col-sm-3 control-label">Empresa</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" id="empresa" name="empresa" type="text" placeholder="Ej. Universidad Andrés Bello">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tipocargo" class="col-sm-3 control-label">Tipo de Cargo</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="tipocargo" name="tipocargo">
                                            <option value="">-- Selecciona cargo --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="cargo" class="col-sm-3 control-label">Cargo</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" id="cargo" name="cargo" type="text" placeholder="Ej. Asistente administrativo">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="emaillaboral" class="col-sm-3 control-label">Email empresa</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" id="emaillaboral" name="emaillaboral" type="text" placeholder="Ej. a.bello@alumniunab.cl">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="telefonolaboral" class="col-sm-3 control-label">Teléfono empresa</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" id="telefonolaboral" name="telefonolaboral" type="text" placeholder="Ej. 25660000">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><strong>Antecedentes Académicos</strong></h3>
                            </div>
                            <div class="panel-body">
                            BODY
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Antecedentes Académicos</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label for="tipoinstitucion" class="col-sm-3 control-label">Tipo de institución</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="tipoinstitucion" name="tipoinstitucion">
                                            <option value="">-- Selecciona tipo institucion --</option>
                                            <option value="">Enseñanza básica y/o media</option>
                                            <option value="">Instituto profesional o C.F.T.</option>
                                            <option value="">Universidad</option>
                                            <option value="">Centros de perfeccionamiento</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="institucion" class="col-sm-3 control-label">Tipo de institución</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="institucion" name="institucion">
                                            <option value="">-- Selecciona Institución --</option>
                                            <option value="">Universidad Andres Bello</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" id="divtipocarrera">
                                    <label for="tipocarrera" class="col-sm-3 control-label">Tipo de carrera</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="tipocarrera" name="tipocarrera">
                                            <option value="">-- Selecciona tipo carrera --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" id="divcarrera">
                                    <label for="carrera" class="col-sm-3 control-label">Carrera</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="carrera" name="carrera">
                                            <option value="">-- Selecciona carrera --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" id="divcampus">
                                    <label for="campus" class="col-sm-3 control-label">Campus</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="campus" name="campus">
                                            <option value="">-- Selecciona campus --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ingreso" class="col-sm-3 control-label">Periodo de estudios</label>
                                    <div class="col-sm-3">
                                        <input class="form-control" id="ingreso" name="ingreso" placeholder="Ej. 2009" type="number">
                                    </div>
                                    <div class="col-sm-3">
                                        <input class="form-control" id="egreso" name="egreso" placeholder="Ej. 2014" type="number">
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Content end -->




<script>
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        startView: 2,
        language: "es",
        autoclose: true
    })
</script>