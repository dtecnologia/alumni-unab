<header>
    <div class="row">
        <div class="col-sm-12">
            <h3 class="pull-left" style="color:#C21A01;"> Portal Alumni Universidad Andres Bellos</h3>
        </div>
    </div>
</header>

<div id="content">
    <section id="navigation">
        <div class="container-fluid" style="padding-top:30px;">
            <div class="row">
            <?php if($config["contadores"] === true){ ?>
            	<div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Resumen de Ex-alumnos</h3>
                        </div>
                        <div class="panel-body">
                        	<p><strong>Ultimo Registrado:</strong> <?php echo date('d-m-Y H:i:s', strtotime($ultimo_registro->getFechaRegistro())); ?></p>
                        	<p><strong>Alumni Registrados:</strong> <?php echo $count["registrado"]; ?></p>
                        	<p><strong>Alumni Solicitados:</strong> <?php echo $count["solicitado"]; ?></p>
                        	<p><strong>Alumni Precargados:</strong> <?php echo $count["precargado"]; ?></p>
                        	<p><strong>Alumni Totales:</strong> <?php echo $count["total"]; ?></p>

                        </div>
                        <div class="panel-footer" style="text-align: right">
                        	<a href="#" class="btn btn-link"> Realizar una búsqueda de ex-alumnos &rarr;</a>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <?php if($config["solicitudes"] === true){ ?>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Últimas solicitudes</h3>
                        </div>
                        <div class="panel-body">
                        	<table class="table table-hover">
		                        <thead>
			                        <tr>
			                            <th>Fecha</th>
			                            <th>Descripción</th>
			                        </tr>
		                        </thead>

		                        <tbody data-link="row" class="rowlink">
		                        <?php foreach($solicitudes as $row){ ?>
			                        <tr onclick="location.href='<?php echo base_url(); ?>logged/request/detail/<?php echo $row["solicitud"]->getIdSolicitud(); ?>'" >
			                            <td><?php echo date('d-m-Y H:i:s', strtotime($row["solicitud"]->getFechaIng())); ?></td>
			                            <td><?php echo $row["solicitud"]->getDescripcion(); ?></td>
			                        </tr>
			                    <?php } ?>
		                        </tbody>
		                    </table>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                        	<a href="<?php echo base_url(); ?>logged/request/" class="btn btn-link"> Ver todas las solicitudes &rarr;</a>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if($config["rubros"] === true){ ?>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Registros por Rubro empresarial</h3>
                        </div>
                        <div class="panel-body">
                            <?php foreach($registro_rubro as $row){ ?>
                                <p><strong><?php echo $row["cantidad"]; ?></strong> &mdash; <?php echo $row["nombre"]; ?></p>
                            <?php } ?>
                            <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <a href="#" class="btn btn-link"> Ver todos los Rubros Empresariales &rarr;</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </section>
</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script>
    $(function () {
        
        
        $('#container').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Rubro con mayor cantidad de Alumni'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: 'Rubros',
                colorByPoint: true,
                data: [
                    <?php foreach($registro_rubro as $row): ?>
                        {
                            name: '<?php print $row["nombre"];?>',
                            y: <?php print $row["cantidad"];?>
                        },
                        <?php endforeach;?>
                ]
            }]
        });
    });
                
                
</script>