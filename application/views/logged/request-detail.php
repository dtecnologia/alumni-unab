<!-- Header start -->
<header>
    <div class="row">
        <div class="col-sm-12">
            <h3 class="pull-left"><i class="glyphicon glyphicon-edit"></i> Solicitudes</h3>
        </div>
    </div>
</header>
<!---- Header end -->


<div id="content">
    <section id="navigation">
        <div class="container-fluid" style="padding-top:25px;">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-9">
                            <h3>Módulo de solicitudes <small>Administra tus solicitudes</small></h3>
                        </div>
                        <div class="col-sm-3 text-right">
                            <h3><a class="btn btn-link" href="<?php echo base_url(); ?>logged/request">&larr; Volver al listado de solicitudes</a></h3>
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Detalle de Solicitante </h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover">
                                <tbody data-link="row" class="rowlink" onclick="location.href='#'">
                                    <tr>
                                        <td style="border: none;"><strong>RUT:</strong> <?php echo $registro["rut"]; ?></td>
                                        <td style="border: none;"><strong>Nombre:</strong> <?php echo $registro["nombre"]; ?></td>
                                        <td class="hidden-xs" style="border: none;"><strong>E-mail:</strong> <?php echo $registro["email"]; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Detalle de solicitud </h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading panel-gray">
                                            <h4 class="panel-title">Datos básicos</h4>
                                        </div>
                                        <div class="panel-body">
                                            <form class="form-horizontal" role="form">
                                                
                                                <div class="form-group form-group-sm">
                                                    <label class="col-sm-3 control-label">Asunto:</label>
                                                    <div class="col-sm-9">
                                                        <input class="form-control" disabled value="<?php echo $solicitud->getNombre(); ?>" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group  form-group-sm">
                                                    <label class="col-sm-3 control-label">Tipo de solicitud:</label>
                                                    <div class="col-sm-9">
                                                        <select class="form-control" disabled>
                                                        <?php foreach($tipo_solicitudes as $row){ ?>
                                                            <option value="<?php echo $row->getIdTipoSolicitud(); ?>" <?php if($row->getIdTipoSolicitud() == $solicitud->getIdTipoSolicitud()) echo 'selected'; ?>><?php echo $row->getNombre();?></option>
                                                        <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-sm">
                                                    <label class="col-sm-3 control-label">Descripción:</label>
                                                    <div class="col-sm-9">
                                                        <textarea disabled class="form-control" style="height:110px"><?php echo $solicitud->getDescripcion(); ?></textarea>
                                                        
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading panel-gray">
                                            <h4 class="panel-title">Datos de gestión</h4>
                                        </div>
                                        <div class="panel-body">
                                            <form class="form-horizontal" role="form">
                                                <div class="form-group form-group-sm">
                                                    <label for="inputPassword" class="col-sm-4 control-label">Estado:</label>
                                                    <div class="col-sm-8">
                                                        <p class="form-control-static"><span class="label label-default"><?php echo $solicitud->getEstado(); ?></span></p>
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-sm">
                                                    <label class="col-sm-4 control-label">Código solicitud:</label>
                                                    <div class="col-sm-8">
                                                        <p class="form-control-static"><?php echo $solicitud->getIdSolicitud(); ?></p>
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-sm">
                                                    <label class="col-sm-4 control-label">Fecha de creación:</label>
                                                    <div class="col-sm-8">
                                                        <p class="form-control-static"><?php echo date('d-m-Y H:i:s', strtotime($solicitud->getFechaIng())); ?></p>
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-sm">
                                                    <label class="col-sm-4 control-label">Última modificación:</label>
                                                    <div class="col-sm-8">
                                                        <p class="form-control-static"><?php if($solicitud->getFechaMod() != '') echo date('d-m-Y H:i:s', strtotime($solicitud->getFechaMod())); else echo 'No modificado'; ?></p>
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-sm">
                                                    <label for="inputPassword" class="col-sm-4 control-label">Última atención:</label>
                                                    <div class="col-sm-8">
                                                        <p class="form-control-static"><?php if($solicitud->getIdUsuarioMod() == '') echo "No atendido"; else echo $solicitud->getIdUsuarioMod(); ?></p>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-sm-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading panel-gray">
                                            <h4 class="panel-title">Archivos adjuntos</h4>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-condensed">
                                                <tbody>
                                                    <tr>
                                                        <td><a href="">Curriculum vitae.doc</a></td>
                                                        <td style="text-align:right;"><a class="btn btn-xs btn-primary" href=""><i class="glyphicon glyphicon-remove"></i> Eliminar</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="">Captura de pantalla 12-12-2014 (1).jpg</a></td>
                                                        <td style="text-align:right;"><a class="btn btn-xs btn-primary" href=""><i class="glyphicon glyphicon-remove"></i> Eliminar</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="col-sm-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading panel-gray">
                                            <h4 class="panel-title">Mensajería</h4>
                                        </div>
                                        <div class="panel-body" style="min-height: 150px; max-height: 300px; overflow-y: auto;" id="request-message-body">
                                        <?php foreach($mensajes as $row){ 
                                            if(!empty($row["usuario"])){ ?>
                                                <blockquote class="blockquote<?php if($this->session->userdata('perfil') == 'usuario') echo '-reverse'; ?>">
                                                    <small><?php echo $row["usuario"]->getNombre(); ?></small>
                                                    <p><?php echo $row["mensaje"]->getDescripcion(); ?></p>
                                                </blockquote>
                                            <?php } else { ?>
                                                <blockquote class="blockquote<?php if($this->session->userdata('perfil') == 'registro') echo '-reverse'; ?>">
                                                    <small><?php echo $row["registro"]->getNombre(); ?></small>
                                                    <p><?php echo $row["mensaje"]->getDescripcion(); ?></p>
                                                </blockquote>
                                        <?php  }
                                        }  ?>
                                        </div>
                                        <script>$("#request-message-body").scrollTop(99999999999);</script>
                                        <div class="panel-footer">
                                            <form class="form-inline" role="form" action="javascript: requestMessage('insert');" id="request-message-form">
                                                <div class="form-group" style="width: 80%; margin-right: 15px;">
                                                    <label class="sr-only" for="mensaje">Mensaje</label>
                                                    <textarea class="form-control" id="descripcion" name="descripcion" placeholder="Escriba un mensaje" style="width: 100%"></textarea>
                                                </div>
                                                <button type="submit" class="btn btn-primary">Enviar</button>
                                                <input type="hidden" value="insert" name="action">
                                                <input type="hidden" value="<?php echo $solicitud->getIdSolicitud(); ?>" name="idsolicitud" id="idsolicitud">
                                            </form>
                                            <p id="request-message-message" style="display: none"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>