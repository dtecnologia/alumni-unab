<!-- Header start -->
<header>
    <div class="row">
        <div class="col-sm-12">
            <h3 class="pull-left" style="color:#C21A01;"><i class="glyphicon glyphicon-edit"></i> Mis Datos</h3>
        </div>
    </div>
</header>
<!---- Header end -->

<!-- Content start -->
<div id="content">
    <section id="navigation">
        <div class="container-fluid" style="padding-top:25px;">
            <div class="row">
                <?php if(!empty($message)){ ?>
                <div class="alert alert-<?php echo $message["status"]; ?> alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                  <strong>Acción realizada: </strong> <?php echo $message["message"]; ?>.
                </div>
                <?php } ?>


                <form name="form_perfil" id="form_perfil" role="form" action="<?php echo base_url(); ?>logged/profile/basico" method="post">
                    <div class="col-lg-12">
                        <h3>Antescedentes Personales <small>Datos básicos y residenciales</small></h3>
                        <hr>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">Datos Básicos</h4>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">
                                                    <!-- RUT -->
                                                    <div class="form-group">
                                                        <label for="rut" class="col-sm-4 control-label">Rut</label>
                                                        <div class="col-sm-7">
                                                            <input class="form-control" name="rut" id="rut" placeholder="Ej. 17.072.799-1" type="text" disabled="" value="<?php echo isRut($registro->getRut()); ?>">
                                                            <?php echo form_error('rut', '<small class="text-danger">','</small>'); ?>
                                                        </div>
                                                    </div>
                                                    <!-- Nombre Completo-->
                                                    <div class="form-group">
                                                        <label for="nombre" class="col-sm-4 control-label">Nombres</label>
                                                        <div class="col-sm-7">
                                                            <input class="form-control" name="nombre" id="nombre" type="text" value="<?php echo $registro->getNombreCompleto(); ?>">
                                                            <?php echo form_error('nombre', '<small class="text-danger">','</small>'); ?>
                                                        </div>
                                                    </div>
                                                    <!-- Apellido Paterno -->
                                                    <!-- <div class="form-group">
                                                        <label for="apaterno" class="col-sm-4 control-label">Apellido Paterno</label>
                                                        <div class="col-sm-7">
                                                            <input class="form-control" id="apaterno" name="apaterno" type="text" value="<?php echo $registro->getApaterno(); ?>">
                                                            <?php echo form_error('apaterno', '<small class="text-danger">','</small>'); ?>
                                                        </div> 
                                                    </div> -->
                                                    <!-- Apellido Materno -->
                                                    <!-- <div class="form-group">
                                                        <label for="amaterno" class="col-sm-4 control-label">Apellido Materno</label>
                                                        <div class="col-sm-7">
                                                            <input class="form-control" id="amaterno" name="amaterno" type="text" value="<?php echo $registro->getAmaterno(); ?>">
                                                            <?php echo form_error('amaterno', '<small class="text-danger">','</small>'); ?>
                                                        </div> 
                                                    </div> -->
                                                    <!-- Email -->
                                                    <div class="form-group">
                                                        <label for="email" class="col-sm-4 control-label">Email personal</label>
                                                        <div class="col-sm-7">
                                                            <input class="form-control" name="email" id="email" type="email" value="<?php echo $registro->getEmail(); ?>">
                                                            <?php echo form_error('email', '<small class="text-danger">','</small>'); ?>
                                                        </div>
                                                    </div>
                                                    <!-- Email Alumni -->
                                                    <div class="form-group">
                                                        <label for="email" class="col-sm-4 control-label">Email Alumni</label>
                                                        <div class="col-sm-7">
                                                            <input class="form-control" name="email_alumni" id="email_alumni" type="email" value="<?php echo $registro->getEmailAlumni(); ?>">
                                                            <?php echo form_error('email_alumni', '<small class="text-danger">','</small>'); ?>
                                                        </div>
                                                    </div>
                                                    <!-- Fecha Nacimiento-->
                                                    <div class="form-group">
                                                        <label for="fechanacimiento" class="col-sm-4 control-label">Fecha Nacimiento</label>
                                                        <div class="col-sm-5">
                                                            <div class="input-group date datepicker">
                                                                <input class="form-control" id="fechanacimiento" name="fechanacimiento" placeholder="dd-mm-yyyy" data-format="dd-mm-yyyy" type="text" value="<?php if($registro->getFechaNacimiento() != '') echo date('d-m-Y', strtotime($registro->getFechaNacimiento())); ?>">
                                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                            </div>
                                                            <?php echo form_error('fechanacimiento', '<small class="text-danger">','</small>'); ?>
                                                        </div>
                                                    </div>
                                                    <!-- Sexo-->
                                                    <div class="form-group">
                                                        <label for="sexo" class="col-sm-4 control-label">Género</label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control" id="sexo" name="sexo">
                                                                <option value="">-- Selecciona Género --</option>
                                                                <option value="F" <?php if($registro->getSexo() == "F") echo 'selected'; ?>>Femenino</option>
                                                                <option value="M" <?php if($registro->getSexo() == "M") echo 'selected'; ?>>Masculino</option>
                                                            </select>
                                                            <?php echo form_error('sexo', '<small class="text-danger">','</small>'); ?>
                                                        </div>
                                                    </div>
                                                    
                                                    <!-- Nacionalidad-->
                                                    <div class="form-group">
                                                        <label for="nacionalidad" class="col-sm-4 control-label">Pais natal</label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control" id="nacionalidad" name="nacionalidad">
                                                                <option value="">-- Selecciona País --</option>
                                                            <?php foreach($paises as $row){ ?>
                                                                <option value="<?php echo $row->getIdPais(); ?>" <?php if($registro->getNacionalidad() == $row->getIdPais()) echo 'selected'; ?>><?php echo $row->getNombre(); ?></option>
                                                            <?php } ?>
                                                            </select>
                                                            <?php echo form_error('nacionalidad', '<small class="text-danger">','</small>'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">Datos Residenciales</h4>
                                                <input id="pais" name="pais" value="56" type="hidden">
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">
                                                    
                                                    <div class="form-group">
                                                        <label for="region" class="col-sm-3 control-label">Región</label>
                                                        <div class="col-sm-8">
                                                            <select class="form-control" id="region" name="region" onchange="combobox('region', 'ciudad');">
                                                                <option value="">-- Selecciona Región --</option>
                                                            <?php foreach($regiones as $row){ ?>
                                                                <option value="<?php echo $row->getIdRegion(); ?>" <?php if(is_object($region) && $region->getIdRegion() == $row->getIdRegion()) echo 'selected data="'.$region->getIdRegion().' '.$row->getIdRegion().'"'; ?>><?php echo $row->getNombre(); ?></option>
                                                            <?php } ?>
                                                            </select>
                                                            <?php echo form_error('region', '<small class="text-danger">','</small>'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ciudad" class="col-sm-3 control-label">Ciudad</label>
                                                        <div class="col-sm-8">
                                                            <select class="form-control" id="ciudad" name="ciudad" onchange="combobox('ciudad', 'comuna');">
                                                                <option value="">-- Selecciona Ciudad --</option>
                                                            <?php foreach($ciudades as $row){ ?>
                                                                <option value="<?php echo $row->getIdCiudad(); ?>" <?php if(is_object($ciudad) && $ciudad->getIdCiudad() == $row->getIdCiudad()) echo 'selected'; ?>><?php echo $row->getNombre(); ?></option>
                                                            <?php } ?>
                                                            </select>
                                                            <?php echo form_error('ciudad', '<small class="text-danger">','</small>'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="comuna" class="col-sm-3 control-label">Comuna</label>
                                                        <div class="col-sm-8">
                                                            <select class="form-control" id="comuna" name="comuna">
                                                                <option value="">-- Selecciona Comuna --</option>
                                                            <?php foreach($comunas as $row){ ?>
                                                                <option value="<?php echo $row->getIdComuna(); ?>" <?php if(is_object($comuna) && $comuna->getIdComuna() == $row->getIdComuna()) echo 'selected'; ?>><?php echo $row->getNombre(); ?></option>
                                                            <?php } ?>
                                                            </select>
                                                            <?php echo form_error('comuna', '<small class="text-danger">','</small>'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="direccion" class="col-sm-3 control-label">Dirección</label>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" id="direccion" name="direccion" type="text" value="<?php echo $registro->getDireccion(); ?>">
                                                            <?php echo form_error('direccion', '<small class="text-danger">','</small>'); ?>
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <label for="telefono" class="col-sm-3 control-label">Teléfono fijo</label>
                                                        <div class="col-sm-4">
                                                            <input class="form-control" id="telefono" name="telefono" type="text" value="<?php echo $registro->getTelefono(); ?>">
                                                            <?php echo form_error('telefono', '<small class="text-danger">','</small>'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="celular" class="col-sm-3 control-label">Teléfono móvil</label>
                                                        <div class="col-sm-4">
                                                            <input class="form-control" id="celular" name="celular" type="text" value="<?php echo $registro->getCelular(); ?>">
                                                            <?php echo form_error('celular', '<small class="text-danger">','</small>'); ?>
                                                        </div>
                                                    </div>
                                                    <!-- postal -->
                                                    <div class="form-group">
                                                        <label for="postal" class="col-sm-3 control-label">Código Postal</label>
                                                        <div class="col-sm-4">
                                                            <input class="form-control" id="postal" name="postal" type="text" value="<?php echo $registro->getPostal(); ?>">
                                                            <?php echo form_error('postal', '<small class="text-danger">','</small>'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer" style="text-align: right;">
                                <input type="hidden" name="bloque" value="update">
                                <button type="submit" class="btn btn-primary">Guardar Antescedentes Personales</button>
                            </div>
                        </div>
                    </div>
                </form>


                <form name="form-academico" id="form-academico" role="form" action="javascript:actionPerfil('academico', '<?php echo base_url(); ?>');" method="post">
                    <div class="col-lg-12">
                        <h3>Antescedentes Académicos <small>Datos ingresados</small></h3>
                        <hr>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">Datos académicos <small id="subtitle-academico"></small></h4>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <label for="tipoinstitucion" class="col-sm-4 control-label">Tipo de institución</label>
                                                        <div class="col-sm-8">
                                                            <select class="form-control" id="tipoinstitucion" name="tipoinstitucion" onchange="combobox('tipoinstitucion', 'institucion'); mostrar_carreras();">
                                                                <option value="">-- Selecciona tipo institución --</option>
                                                                <?php foreach($tipo_institucion as $row){ ?>
                                                                <option value="<?php echo $row->getIdTipoInstitucion(); ?>"><?php echo $row->getNombre(); ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="institucion" class="col-sm-4 control-label">Institución</label>
                                                        <div class="col-sm-8">
                                                            <select class="form-control" id="institucion" name="institucion" onchange="mostrar_campus();">
                                                                <option value="">-- Selecciona Institución --</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" id="divtipocarrera" style="display: none">
                                                        <label for="tipocarrera" class="col-sm-4 control-label">Tipo de carrera</label>
                                                        <div class="col-sm-8">
                                                            <select class="form-control" id="tipocarrera" name="tipocarrera" onchange="combobox('tipocarrera', 'carrera');">
                                                                <option value="">-- Selecciona tipo de carrera --</option>
                                                            <?php foreach($tipo_carrera as $row){ ?>
                                                                <option value="<?php echo $row->getIdTipoCarrera(); ?>"><?php echo $row->getNombre(); ?></option>
                                                            <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" id="divcarrera" style="display: none">
                                                        <label for="carrera" class="col-sm-4 control-label">Carrera</label>
                                                        <div class="col-sm-8">
                                                            <select class="form-control" id="carrera" name="carrera">
                                                                <option value="">-- Selecciona carrera --</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" id="divcampus" style="display: none">
                                                        <label for="campus" class="col-sm-4 control-label">Campus</label>
                                                        <div class="col-sm-8">
                                                            <select class="form-control" id="campus" name="campus">
                                                            <option value="">-- Selecciona campus --</option>
                                                            <?php foreach($campus as $row){ ?>
                                                                <option value="<?php echo $row->getIdCampus(); ?>"><?php echo $row->getNombre(); ?></option>
                                                            <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="fecha_academico_inicio" class="col-sm-4 control-label">Periodo de estudio</label>
                                                        <div class="col-sm-4">
                                                            <div class="input-group date" id="datepicker_academico_inicio">
                                                                <input class="form-control" id="fecha_academico_inicio" name="fecha_inicio" data-format="dd-mm-yyyy" type="text">
                                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="input-group date" id="datepicker_academico_termino">
                                                                <input class="form-control" id="fecha_academico_termino" name="fecha_termino" data-format="dd-mm-yyyy" type="text">
                                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="descripcion-academica" class="col-sm-4 control-label">Descripción Breve</label>
                                                        <div class="col-sm-8">
                                                            <textarea class="form-control" id="descripcion_academico" name="descripcion"></textarea>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div id="alert-academico" class="alert alert-dismissible fade in" style="display: none;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><small id="message-academico">Registro actualizado con éxito</small></div>
                                            <div class="panel-footer" style="text-align: right;">
                                                <input type="hidden" name="accion" id="accion-academico" value="insert">
                                                <input type="hidden" name="idedit" id="idedit-academico" value="">
                                                <input type="hidden" name="idregistro" id="idregistro-academico" value="">
                                                <button type="submit" id="btn-academico-submit" class="btn btn-primary">Guardar datos</button>
                                                <button type="reset" id="btn-academico-reset" class="btn btn-default">Limpiar</button>
                                                <button type="button" onclick="cancelEdit('academico');" id="btn-academico-cancelar" class="btn btn-default" style="display:none;">Cancelar Edición</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">Antescedentes ingresados</h4>
                                            </div>
                                            <div class="panel-body">
                                                <table class="table table-hover" id="table-content-academico" <?php if(empty($registro_academico)) echo 'style="display:none;"'; ?>>
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Estudio</th>
                                                            <th>Años</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody data-link="row" class="rowlink" id="content-academico">
                                                    <?php if(!empty($registro_academico)){
                                                        echo $registro_academico;
                                                    } ?>
                                                    </tbody>
                                                </table>
                                                <?php if(empty($registro_academico)){ ?>
                                                <p id="no-content-academico"><i class="glyphicon glyphicon-info-sign"></i> Sin antescedentes laborales ingresados</p>
                                                <?php } ?>
                                            </div>
                                            <div class="panel-footer" id="footer-academico" style="text-align: right; display: none;">
                                                <button type="submit" id="btn-academico-edit" class="btn btn-default btn-sm" onclick="$('#accion-academico').val('edit');"><i class="glyphicon glyphicon-pencil"></i> Editar</button>
                                                <button type="submit" class="btn btn-default btn-sm" id="btn-academico-delete" onclick="$('#accion-academico').val('delete');"><i class="glyphicon glyphicon-remove"></i> Eliminar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <hr>

                <form name="form-laboral" id="form-laboral" role="form" action="javascript:actionPerfil('laboral', '<?php echo base_url(); ?>');" method="post">
                    <div class="col-lg-12">                    
                        <h3>Antescedentes Laborales <small>Datos ingresados</small></h3>
                        <hr>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">Datos Laborales <small id="subtitle-laboral"></small></h4>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <label for="rubro" class="col-sm-4 control-label">Rubro de Desempeño</label>
                                                        <div class="col-sm-8">
                                                            <select class="form-control" id="rubro" name="rubro">
                                                                <option value="">-- Selecciona Rubro --</option>
                                                            <?php foreach($rubro as $row){ ?>
                                                                <option value="<?php echo $row->getIdRubroEmpresarial(); ?>"><?php echo $row->getNombre(); ?></option>
                                                            <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="empresa" class="col-sm-4 control-label">Empresa</label>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" id="empresa" name="empresa" type="text" placeholder="Ej. Universidad Andrés Bello">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tipocargo" class="col-sm-4 control-label">Tipo de Cargo</label>
                                                        <div class="col-sm-8">
                                                            <select class="form-control" id="tipocargo" name="tipocargo">
                                                                <option value="">-- Selecciona cargo --</option>
                                                            <?php foreach($tipo_cargo as $row){ ?>
                                                                <option value="<?php echo $row->getIdTipoCargo(); ?>"><?php echo $row->getNombre(); ?></option>
                                                            <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="cargo" class="col-sm-4 control-label">Cargo</label>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" id="cargo" name="cargo" type="text" placeholder="Ej. Asistente administrativo">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email_laboral" class="col-sm-4 control-label">Email empresa</label>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" id="email_laboral" name="email_laboral" type="text" placeholder="Ej. a.bello@alumniunab.cl">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="telefono_laboral" class="col-sm-4 control-label">Teléfono empresa</label>
                                                        <div class="col-sm-4">
                                                            <input class="form-control" id="telefono_laboral" name="telefono_laboral" type="text" placeholder="Ej. 25660000">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ingreso" class="col-sm-4 control-label">Periodo de trabajo</label>
                                                        <div class="col-sm-4">
                                                            <div class="input-group date" id="datepicker_laboral_inicio">
                                                                <input class="form-control" id="fecha_laboral_inicio" name="fecha_inicio" data-format="dd-mm-yyyy" type="text">
                                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="input-group date" id="datepicker_laboral_termino">
                                                                <input class="form-control" id="fecha_laboral_termino" name="fecha_termino" data-format="dd-mm-yyyy" type="text">
                                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="descripcion_laboral" class="col-sm-4 control-label">Descripción Breve</label>
                                                        <div class="col-sm-8">
                                                            <textarea class="form-control" id="descripcion_laboral" name="descripcion"></textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div id="alert-laboral" class="alert alert-dismissible fade in" style="display: none;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><small id="message-laboral">Registro actualizado con éxito</small></div>
                                            <div class="panel-footer" style="text-align: right;">
                                                <input type="hidden" name="accion" id="accion-laboral" value="insert">
                                                <input type="hidden" name="idedit" id="idedit-laboral" value="">
                                                <input type="hidden" name="idregistro" id="idregistro-laboral" value="">
                                                <button type="submit" id="btn-laboral-submit" class="btn btn-primary">Guardar datos</button>
                                                <button type="reset" id="btn-laboral-reset" class="btn btn-default">Limpiar</button>
                                                <button type="button" onclick="cancelEdit('laboral');" id="btn-laboral-cancelar" class="btn btn-default" style="display:none;">Cancelar Edición</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">Antescedentes ingresados</h4>
                                            </div>
                                            <div class="panel-body">
                                                <table class="table table-hover" id="table-content-laboral" <?php if(empty($registro_laboral)) echo 'style="display:none;"'; ?>>
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Empresa</th>
                                                            <th>Años</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody data-link="row" class="rowlink" id="content-laboral">
                                                    <?php if(!empty($registro_laboral)){
                                                        echo $registro_laboral;
                                                    } ?>
                                                    </tbody>
                                                </table>
                                                <?php if(empty($registro_laboral)){ ?>
                                                <p id="no-content-laboral"><i class="glyphicon glyphicon-info-sign"></i> Sin antescedentes laborales ingresados</p>
                                                <?php } ?>
                                            </div>
                                            <div class="panel-footer" id="footer-laboral" style="text-align: right; display: none;">
                                                <button type="submit" id="btn-laboral-edit" class="btn btn-default btn-sm" onclick="$('#accion-laboral').val('edit');"><i class="glyphicon glyphicon-pencil"></i> Editar</button>
                                                <button type="submit" class="btn btn-default btn-sm" id="btn-laboral-delete" onclick="$('#accion-laboral').val('delete');"><i class="glyphicon glyphicon-remove"></i> Eliminar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>


                <form name="form_imagen" id="form_imagen" role="form" action="<?php echo base_url(); ?>logged/profile" method="post">
                    <div class="col-lg-12">                    
                        <h3>Otros Datos <small>Anexos a tu perfil</small></h3>
                        <hr>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">Modificar imagen de perfil</h4>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">
                                                    <!-- Nueva imagen -->
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Imagen actual</label>
                                                        <div class="col-sm-8">
                                                            <img style="width: 200px" src="http://static1.creativosonline.org/blog/wp-content/uploads/2010/05/creativosonline_crear_avatar_profesional.jpg">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="file" class="col-sm-4 control-label">Nueva imagen</label>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" type="file" name="file" id="file">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="text-align: right">
                                                <input type="hidden" name="bloque" value="imagen">
                                                <input type="submit" class="btn btn-primary" value="Actualizar imagen">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>


            </div>           
        </div>
    </section>
</div>
<!-- Content end -->




<script>
    $('.datepicker').datepicker({
        format: "dd-mm-yyyy",
        startView: 2,
        language: "es",
        autoclose: true
    });

    /* RANGO DE FECHAS LABORALES 
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        startView: 2,
        language: "es",
        autoclose: true
    })

    $('#datetimepicker6').datetimepicker();
    $('#datetimepicker7').datetimepicker({
            useCurrent: false //Important! See issue #1075
    });
    $("#datetimepicker6").on("dp.change", function (e) {
        $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    });
    $("#datetimepicker7").on("dp.change", function (e) {
        $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    });
    FIN DE RANGO LABORALES */



    /* RANGO DE FECHAS LABORALES */
    var date = new Date();
    $('#fecha_laboral_inicio').datepicker({
        format: "dd-mm-yyyy",
        startView: 1,
        language: "es",
        autoclose: true,
        endDate: date
    });
    $('#fecha_laboral_termino').datepicker({
        format: "dd-mm-yyyy",
        startView: 1,
        language: "es",
        autoclose: true,
        useCurrent: false,
        endDate: date //Important! See issue #1075
    });
    $("#fecha_laboral_inicio").on("change", function (e) {
        $('#fecha_laboral_termino').datepicker('setStartDate', $(this).val());
    });
    $("#fecha_laboral_termino").on("change", function (e) {
        $('#fecha_laboral_inicio').datepicker('setEndDate', $(this).val());
    });
    /* FIN DE RANDO LABORALES */

    /* RANGO DE FECHAS ACADEMICAS */
    var date = new Date();
    $('#fecha_academico_inicio').datepicker({
        format: "dd-mm-yyyy",
        startView: 1,
        language: "es",
        autoclose: true,
        endDate: date
    });
    $('#fecha_academico_termino').datepicker({
        format: "dd-mm-yyyy",
        startView: 1,
        language: "es",
        autoclose: true,
        useCurrent: false,
        endDate: date //Important! See issue #1075
    });
    $("#fecha_academico_inicio").on("change", function (e) {
        $('#fecha_academico_termino').datepicker('setStartDate', $(this).val());
    });
    $("#fecha_academico_termino").on("change", function (e) {
        $('#fecha_academico_inicio').datepicker('setEndDate', $(this).val());
    });
    /* FIN DE RANDO ACADEMICAS */
</script>