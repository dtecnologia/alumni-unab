<header>
    <div class="row">
        <div class="col-sm-12">
            <h3 class="pull-left" style="color:#C21A01;"><i class="glyphicon glyphicon-cog"></i> Mi Cuenta</h3>
        </div>
    </div>
</header>


<div id="content">
    <section id="navigation">
    	<div class="container-fluid" style="padding-top:30px;">
    		<div class="row">
    			<div class="col-lg-6">
					<div class="panel panel-default">
		                <div class="panel-heading">
		                    <h3 class="panel-title">Modificar foto de perfil</h3>
		                </div>
		                <div class="panel-body">
		                    <div class="form-horizontal" role="form">
                                <!-- Nueva imagen -->
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Imagen actual</label>
                                    <div class="col-sm-8">
                                        <img style="width: 200px" src="https://scontent-mia1-1.xx.fbcdn.net/hphotos-xpf1/v/t1.0-9/378423_2341077481019_648064401_n.jpg?oh=1134aed1b31ae72ebe0d1ead7a4b050b&amp;oe=56126BAD">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="file" class="col-sm-4 control-label">Nueva imagen</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="file" name="file" id="file">
                                    </div>
                                </div>
                            </div>
		                </div>
		                <div class="panel-footer" style="text-align: right">
                            <input type="submit" class="btn btn-primary btn-sm" value="Actualizar imagen">
		                </div>
		            </div>
		        </div>
    			<div class="col-lg-6">
					<div class="panel panel-default">
		                <div class="panel-heading">
		                    <h3 class="panel-title">Modificar contraseña</h3>
		                </div>
		                <div class="panel-body">
		                    <div class="form-horizontal" role="form">
                                <!-- Nueva imagen -->
                                <div class="form-group">
                                    <label for="pass" class="col-sm-4 control-label">Contraseña actual</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text" name="pass" id="pass">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="newpass" class="col-sm-4 control-label">Nueva contraseña</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text" name="newpass" id="newpass">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="renewpass" class="col-sm-4 control-label">Repita Nueva contraseña</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text" name="renewpass" id="renewpass">
                                    </div>
                                </div>
                            </div>
		                </div>
		                <div class="panel-footer" style="text-align: right;">
                            <input type="submit" class="btn btn-primary btn-sm" value="Actualizar contraseña">
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</section>
</div>