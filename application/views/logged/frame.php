<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1">
        <title>AlumniUnab - Gestión de exalumnos</title>
        <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datepicker.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/funcs.js"></script>
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" type="text/css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.gray-blue.css" type="text/css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>assets/css/datepicker.css" type="text/css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>assets/css/html.css" type="text/css" rel="stylesheet"/>
        <?php if($this->session->userdata('perfil') == 'usuario'){ ?>
        <style>    
            #sidenav-left {
              background-color: #1B1D24;
            }
        </style>
        <?php } ?>
    </head>
    <body>
        <div id="main-wrapper">
            <!-- Aside start -->
            <aside class="hidden-xs">
                <nav id="sidenav-left">
                    <div class="container-fluid scrollable">
                        
                        <div style="padding: 5px 0 15px 0"><a href="<?php echo base_url(); ?>"><img class="img-responsive" src="<?php echo base_url(); ?>assets/img/logo.png" style="max-width:170px" /></a></div>
                        
                        <?php if($this->session->userdata('perfil') == 'usuario'){ ?>

                        <ul class="list-unstyled" id="accordion">
                            <li><hr style="border-color: #FFF;"></li>
                            <li>
                                <a href="<?php echo base_url(); ?>logged/">
                                    <span class="hidden-xs"><i class="glyphicon glyphicon-blackboard"></i> Resumen</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>logged/message">
                                    <span class="hidden-xs"><i class="glyphicon glyphicon-comment"></i> Avisos</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>logged/request">
                                    <span class="hidden-xs"><i class="glyphicon glyphicon-th-list"></i> Solicitudes</span>
                                </a>
                            </li>                            
                            <li>
                                <a href="<?php echo base_url(); ?>logged/search">
                                    <span class="hidden-xs"><i class="glyphicon glyphicon-search"></i> Buscar Ex-Alumnos</span>
                                </a>
                            </li>
                            <!--
                            <li> 
                                <a href="<?php echo base_url(); ?>logged/profile"> 
                                    <span class="hidden-xs"><i class="glyphicon glyphicon-edit"></i> Mi Cuenta </span>
                                </a> 
                            </li>
                            -->
                            <li>
                                <a href="<?php echo base_url(); ?>logged/logout">
                                    <span class="hidden-xs"><i class="glyphicon glyphicon-off"></i> Salir</span>
                                </a>
                            </li>
                        </ul>

                        <?php }else{ ?>

                        <div style="margin:0px 30px; border: 3px solid #FFF; width: 110px; height: 110px; background-image: url(http://static1.creativosonline.org/blog/wp-content/uploads/2010/05/creativosonline_crear_avatar_profesional.jpg); background-size:cover;" class="img-circle"></div>
                        
                        <ul class="list-unstyled" id="accordion">
                            <li class="hidden-xs"><small>Daniel Araneda Hernández</small></li>
                            <li><hr style="border-color: #FFF;"></li>
                            <li>
                                <a href="<?php echo base_url(); ?>logged/">
                                    <span class="hidden-xs"><i class="glyphicon glyphicon-blackboard"></i> Resumen</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>logged/message">
                                    <span class="hidden-xs"><i class="glyphicon glyphicon-comment"></i> Mis Avisos</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>logged/request">
                                    <span class="hidden-xs"><i class="glyphicon glyphicon-th-list"></i> Mis Solicitudes</span>
                                </a>
                            </li>
                            <li> 
                                <a href="<?php echo base_url(); ?>logged/profile"> 
                                    <span class="hidden-xs"><i class="glyphicon glyphicon-edit"></i> Mis Datos </span>
                                </a> 
                            </li>
                            <!--
                            <li>
                                <a href="<?php echo base_url(); ?>logged/document">
                                    <span class="hidden-xs"><i class="glyphicon glyphicon-file"></i> Mi Curriculum</span>
                                </a>
                            </li>
                            -->
                            <li>
                                <a href="<?php echo base_url(); ?>logged/search">
                                    <span class="hidden-xs"><i class="glyphicon glyphicon-search"></i> Buscar Ex-Alumnos</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>logged/logout">
                                    <span class="hidden-xs"><i class="glyphicon glyphicon-off"></i> Salir</span>
                                </a>
                            </li>
                        </ul>
                        <?php } ?>
                    </div>
                </nav>
            </aside>
            <!-- Aside end -->

            <!-- Main start -->
            <section id="main-container">
                <div class="visible-xs" style="text-align: center; background-color: #C21A01" id="logo-movil">
                    <div class="col-sm-12" style="padding: 8px 15px"><a href="<?php echo base_url(); ?>">
                        <img class="img-responsive" src="<?php echo base_url(); ?>assets/img/logo.png" style="max-width:100px;" /></a>
                        <button class="pull-right btn btn-gunmetal" style="margin-top: -38px" type="button" data-toggle="collapse" data-target="#bs-navbar" aria-controls="bs-navbar" aria-expanded="false"><i class="glyphicon glyphicon-menu-hamburger"></i></button>
                    </div>
                    <nav aria-expanded="false" id="bs-navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="<?php echo base_url(); ?>logged/message"><i class="glyphicon glyphicon-comment"></i> Mis Avisos</a>
                            </li> 
                            <li>
                                <a href="<?php echo base_url(); ?>logged/request">
                                    <i class="glyphicon glyphicon-th-list"></i> Mis Solicitudes
                                </a>
                              <br>
                            </li>
                            <li> <a href="<?php echo base_url(); ?>logged/profile"> <i class="glyphicon glyphicon-edit"></i> Mi Cuenta </a> </li>
                            <li> <a href="<?php echo base_url(); ?>logged/document"> <i class="glyphicon glyphicon-file"></i> Mi Curriculum </a> </li>
                            <li>
                                <a href="<?php echo base_url(); ?>logged/search">
                                    <i class="glyphicon glyphicon-search"></i> Buscar Ex-Alumnos
                            </a></li>
                            <li>
                                <a href="<?php echo base_url(); ?>logged/logout"><i class="glyphicon glyphicon-off"></i> Salir</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                
                <?php echo $body; ?>
                <!-- Footer start -->
                <!-- <footer>
                    <p> &mdash; Desarrollado por Daniel Araneda Hernández (daniel.araneda.h@gmail.com) para Mitsoon, Periodo Julio - Agosto 2015</p>
                </footer> -->
                <!-- Footer end -->
            </section>

            <!-- Main end -->
        </div>
    

        <script> 
        if($("#logo-movil").height() > 0){
            $(window).scroll(function(){ 
                if( $(this).scrollTop() < $("#logo-movil").height()){
                    $("header").css("top", ($("#logo-movil").height() - $(this).scrollTop()) + "px");
                }else{
                    $("header").css("top", "0px").css("position", "fixed");
                }
            });
        }
        </script>
    </body>
</html>