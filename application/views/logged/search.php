<header>
    <div class="row">
        <div class="col-sm-12">
            <h3 class="pull-left" style="color:#C21A01;"><i class="glyphicon glyphicon-search"></i> Búsqueda de ex-alumnos</h3>
        </div>
    </div>
</header>

<div id="content">
    <section id="navigation">
        <div class="container-fluid" style="padding-top:25px;">
            <div class="row">

                <div class="alert alert-<?php echo $this->session->message["status"]; ?> alert-dismissible fade in" role="alert" style="display: none">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                  <strong>Acción realizada: </strong>Sin resultados
                </div>

                <div class="col-lg-12">
                    <h3>Búsqueda ex-alumnos <small>Ingresa filtros para mejores resultados</small></h3>
                    <hr>
                	<div class="panel panel-default">
                		<form name="form-search" id="form-search" method="post" action="<?php echo base_url(); ?>logged/search">
	                        <div class="panel-heading">
	                            <h4 class="panel-title">Filtros de búsqueda</h4>
	                        </div>
	                        <div class="panel-body">
	                            <div class="form-inline" role="form">
                                        <?php if ($this->session->userdata('perfil') == 'usuario'): ?>
	                                <div class="form-group col-lg-3">
	                                    <label for="rut">RUT de Registro</label>
                                            <br/>
                                            <input class="form-control" id="rut" name="rut" placeholder="Ej. 16786123-0" type="text" value="<?php if(!empty($filtros["rut"])){echo $filtros["rut"];}; ?>">
	                                </div>
                                        <?php endif;?>
	                                <div class="form-group col-lg-3">
	                                    <label for="nombre">Nombre / Apellido</label>
                                            <br/>
	                                    <input class="form-control" id="nombre" name="nombre" placeholder="Ej. Araneda" type="text" value="<?php if(!empty($filtros["nombre_completo"])){echo $filtros["nombre_completo"];} ?>">
	                                </div>
	                                <div class="form-group col-lg-3">
	                                    <label for="email">Email</label>
                                            <br/>
	                                    <input class="form-control" id="email" name="email" placeholder="Ej. alumni@unab.cl" type="text" value="<?php if(!empty($filtros["email"])){echo $filtros["email"];}; ?>">
	                                </div>
                                        <?php if ($this->session->userdata('perfil') == 'usuario'): ?>
	                                <div class="form-group col-lg-3">
	                                    <label for="estado">Estado</label>
                                            <br/>
	                                    <select class="form-control" id="estado" name="estado">
	                                    	<option value="" <?php if(!empty($filtros["estado"]) && $filtros["estado"] == ""){echo 'selected';} ?>>-- Selecciona estado --</option>
	                                    	<option value="1" <?php if(!empty($filtros["estado"]) && $filtros["estado"] == "1"){echo 'selected';}?>>Registrado</option>
	                                    	<option value="2" <?php if(!empty($filtros["estado"]) && $filtros["estado"] == "2"){echo 'selected';}?>>Solicitado</option>
	                                    	<option value="0" <?php if(!empty($filtros["estado"]) && $filtros["estado"] == "3"){echo 'selected';}?>>Precargado</option>
	                                    </select>
	                                </div>
                                        <?php endif;?>
	                            </div>
                                    <br/>
                                    <br/>
                                    <div class="collapse <?php echo $filtros["_bavanzada"] == TRUE ?  "in" :  ""; ?>" id="collapseExample"   style="margin-top: 20px;">
                                        <div class="well">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">Filtros Academicos</h4>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-inline" role="form">
                                                        <div class="form-group col-lg-3">
                                                            <label for="tipoCarrera">Tipo Carrera</label>
                                                            <br/>
                                                            <select class="form-control" style="width: 100%;" id="idtipocarrera" name="idtipocarrera" onchange="combobox('idtipocarrera', 'carrera');">
                                                                <option value="" <?php if(!empty($filtros["idtipocarrera"])){echo 'selected';} ?>>-- Selecciona tipo carrera --</option>
                                                                <?php foreach ($tipo_carreras as $tipo_carrera): ?>
                                                                    <option value="<?php print $tipo_carrera->getIdTipoCarrera(); ?>" <?php if(!empty($filtros["idtipocarrera"]) && $filtros["idtipocarrera"] == $tipo_carrera->getIdTipoCarrera()){echo 'selected';}?>><?php print $tipo_carrera->getNombre(); ?></option>
                                                                <?php endforeach; ?>

                                                            </select>
                                                        </div> 
                                                        <div class="form-group col-lg-8">
                                                            <label for="carrera">Carrera</label>
                                                            <br/>
                                                            <select class="form-control" style="width: 100%;" id="carrera" name="carrera" onchange="combobox('carrera', 'idcampus');">
                                                                <option value="" <?php if(!empty($filtros["idcarrera"])){echo 'selected';} ?>>-- Selecciona carrera --</option>
                                                                <?php foreach ($carreras as $carrera): ?>
                                                                    <option value="<?php print $carrera->getIdCarrera(); ?>" <?php if(!empty($filtros["idcarrera"]) && $filtros["idcarrera"] == $carrera->getIdCarrera()){echo 'selected';}?>><?php print $carrera->getNombre(); ?></option>
                                                                <?php endforeach; ?>

                                                            </select>
                                                        </div>
                                                        <br/>
                                                        <div class="form-group col-lg-3">
                                                            <label for="campus">Campus</label>
                                                            <br/>
                                                            <select class="form-control" style="width: 100%;" id="idcampus" name="idcampus">
                                                                <option value="" <?php if(!empty($filtros["idcampus"])){echo 'selected';} ?>>-- Selecciona Campus --</option>
                                                                <?php foreach ($campus as $campu): ?>
                                                                    <option value="<?php print $campu->getIdCampus(); ?>" <?php if(!empty($filtros["idcampus"]) && $filtros["idcampus"] == $campu->getIdCampus()){echo 'selected';}?>><?php print $campu->getNombre(); ?></option>
                                                                <?php endforeach; ?>

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">Filtros de Geográficos</h4>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-inline" role="form">
                                                        <div class="form-group col-lg-3">
                                                            <label for="pais">Pa&#237;s</label>
                                                            <br/>
                                                            <select class="form-control" id="pais" name="pais" onchange="combobox('pais', 'region');">
                                                                <option value="" <?php if(!empty($filtros["idpais"])){echo 'selected';} ?>>-- Selecciona Pa&#237;s --</option>
                                                                <?php foreach ($paises as $pais): ?>
                                                                    <option value="<?php print $pais->getIdPais(); ?>" <?php if(!empty($filtros["idpais"]) && $filtros["idpais"] == $pais->getIdPais()){echo 'selected';}?>><?php print $pais->getNombre(); ?></option>
                                                                <?php endforeach; ?>

                                                            </select>
                                                        </div> 
                                                        <div class="form-group col-lg-5">
                                                            <label for="region">Regi&#243;n</label>
                                                            <br/>
                                                            <select class="form-control" id="region" name="region" onchange="combobox('region', 'ciudad');">
                                                                <option value="" <?php if(!empty($filtros["idregion"])){echo 'selected';} ?>>-- Selecciona Regi&#243;n --</option>
                                                                <?php foreach ($regiones as $region): ?>
                                                                    <option value="<?php print $region->getIdRegion(); ?>" <?php if(!empty($filtros["idregion"]) && $filtros["idregion"] == $region->getIdRegion()){echo 'selected';}?>><?php print $region->getNombre(); ?></option>
                                                                <?php endforeach; ?>

                                                            </select>
                                                        </div>
                                                        <div class="form-group col-lg-3">
                                                            <label for="ciudad">Ciudad</label>
                                                            <br/>
                                                           <select class="form-control" id="ciudad" name="ciudad" onchange="combobox('ciudad', 'comuna');">
                                                                <option value="" <?php if(!empty($filtros["idciudad"])){echo 'selected';} ?>>-- Selecciona Ciudad --</option>
                                                                <?php foreach ($ciudades as $ciudad): ?>
                                                                    <option value="<?php print $ciudad->getIdCiudad(); ?>" <?php if(!empty($filtros["idciudad"]) && $filtros["idciudad"] == $ciudad->getIdCiudad()){echo 'selected';}?>><?php print $ciudad->getNombre(); ?></option>
                                                                <?php endforeach; ?>

                                                            </select>
                                                        </div>
                                                        <div class="form-group col-lg-3">
                                                            <label for="comuna">Comuna</label>
                                                            <br/>
                                                            <select class="form-control" id="comuna" name="comuna">
                                                                <option value="" <?php if(!empty($filtros["idcomuna"])){echo 'selected';} ?>>-- Selecciona Comuna --</option>
                                                                <?php foreach ($comunas as $comuna): ?>
                                                                    <option value="<?php print $comuna->getIdComuna(); ?>" <?php if(!empty($filtros["idcomuna"]) && $filtros["idcomuna"] == $comuna->getIdComuna()){echo 'selected';}?>><?php print $comuna->getNombre(); ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if ($this->session->userdata('perfil') == 'usuario'): ?>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">Filtros Laborales</h4>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-inline" role="form">
                                                        <div class="form-group col-lg-3">
                                                            <label for="cargoLaboral">Cargo Laboral</label>
                                                            <br/>
                                                            <select class="form-control" id="tipo_cargo" name="tipo_cargo">
                                                                <option value="" <?php if(!empty($filtros["tipo_cargo"])){echo 'selected';} ?>>-- Selecciona Cargo Laboral --</option>
                                                                <?php foreach ($tipos_cargos as $tipo_cargo): ?>
                                                                    <option value="<?php print $tipo_cargo->getIdTipoCargo(); ?>" <?php if(!empty($filtros["tipo_cargo"]) && $filtros["tipo_cargo"] == $tipo_cargo->getIdTipoCargo()){echo 'selected';}?>><?php print $tipo_cargo->getNombre(); ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div> 
                                                        <div class="form-group col-lg-3">
                                                            <label for="rubroEmpresarial">Rubro Empresarial</label>
                                                            <br/>
                                                            <select class="form-control" id="rubro" name="rubro">
                                                                <option value="" <?php if(!empty($filtros["rubro"])){echo 'selected';} ?>>-- Selecciona Rubro Empresarial --</option>
                                                                <?php foreach ($rubros as $rubro): ?>
                                                                    <option value="<?php print $rubro->getIdRubroEmpresarial(); ?>" <?php if(!empty($filtros["rubro"]) && $filtros["rubro"] == $rubro->getIdRubroEmpresarial()){echo 'selected';}?>><?php print $rubro->getNombre(); ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endif; ?>
                                            <?php if ($this->session->userdata('perfil') == 'usuario'): ?>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">Fechas de  Registro</h4>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-inline" role="form">
                                                        <div class="form-group col-lg-3">
                                                            <label for="desdeRegistro">Desde</label>
                                                            
                                                                <div class="input-group date datepicker">
                                                                    <input class="form-control" id="desdeRegistro" name="desdeRegistro" placeholder="dd/mm/yyyy" data-format="dd/mm/yyyy" type="text" value="<?php if(!empty($desdeRegistro)){echo $desdeRegistro;}; ?>">
                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                                </div>
                                                            
                                                        </div> 
                                                        <div class="form-group col-lg-3 date datepicker">
                                                            <label for="hastaRegistro">Hasta</label>
                                                            
                                                                <div class="input-group date datepicker">
                                                                    <input class="form-control" id="hastaRegistro" name="hastaRegistro" placeholder="dd/mm/yyyy" data-format="dd/mm/yyyy" type="text" value="<?php if(!empty($hastaRegistro)){echo $hastaRegistro;}; ?>">
                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                                </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
	                        </div>
	                        <div class="panel-footer" style="text-align: right;">
	                            <input type="hidden" name="bloque" value="update">
	                            <button type="submit" class="btn btn-primary btn-sm">Realizar Búsqueda</button>
	                            <button type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="<?php echo $filtros["_bavanzada"] == TRUE ?  "true" :  "false"; ?>" aria-controls="collapseExample" class="btn btn-default btn-sm">Búsqueda Avanzada</button>
                                </div>
	                    </form>
                    </div>

                    <div class="panel panel-default">
		                <div class="panel-heading">
		                    <h4 class="panel-title">Registros ingresados</h4>
		                </div>
		                <div class="panel-body">
	                	<?php if(count($registros) == 0 ){ ?>
	                		<p><i class="glyphicon glyphicon-info-sign"></i> Sin registros ingresados</p>
	                	<?php } else { ?>
		                    <table class="table table-striped">
		                        <thead>
			                        <tr>
			                            <th>ID</th>
			                            <th>Nombre</th>
			                            <th>Email</th>
			                            <?php if($this->session->userdata('perfil') == 'usuario'){ ?>
			                            <th style="text-align: right;">Acciones</th>
			                            <?php } ?>
			                        </tr>
		                        </thead>
		                        <tbody>
                                        <?php if( !empty($registros) ) { ?>
		                        <?php foreach($registros as $row) { ?>
			                        <tr>
			                            <td><?php echo $row->getIdRegistro(); ?></td>
			                            <td><?php echo $row->getNombreCompleto(); ?></td>
			                            <td><?php echo $row->getEmail(); ?></td>
			                            <?php if($this->session->userdata('perfil') == 'usuario'){ ?>
			                            <td style="text-align: right;">
			                            	<a class="btn btn-xs btn-concrete" href="javascript:void(0)"><i class="glyphicon glyphicon-ok"></i> Activar</a>
			                            	<a class="btn btn-xs btn-concrete" href="javascript:void(0)"><i class="glyphicon glyphicon-pencil"></i> Editar</a>
			                            	<a class="btn btn-xs btn-concrete" href="javascript:void(0)"><i class="glyphicon glyphicon-remove"></i> Eliminar</a>
			                            </td>
			                            <?php } ?>
			                        </tr>
			                    <?php } ?> 
                                        <?php }?>
		                        </tbody>
		                    </table>
		                <?php } ?>
		                </div>
		                <div class="panel-footer">
                    		<div class="pull-left">
                    			<div aria-relevant="all" aria-live="polite" role="alert" id="basic2_info" class="dataTables_info"><?php echo $total_registros; ?> registros ingresados, en 22 páginas</div>
                    		</div>
	                    	<div class="pull-right">	
                                    <?php echo $this->pagination->create_links(); ?>
	                    	</div>
	                    	<div class="clearfix">
	                    	</div>
		                </div>
		            </div>

	            </div>    
            </div>
        </div>
    </section>
</div>    
<script>
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true
    })
</script>