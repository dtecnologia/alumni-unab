<!-- Header start -->
<header>
    <div class="row">
        <div class="col-sm-12">
            <h3 class="pull-left" style="color:#C21A01;"><i class="glyphicon glyphicon-comment"></i> Avisos</h3>
        </div>
    </div>
</header>
<!---- Header end -->

<!-- Content start -->
<div id="content">
    <section id="navigation">
        <div class="container-fluid" style="padding-top:25px;">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-10">
                            <h3>Módulo de avisos <small>Crear nuevo aviso</small></h3>
                        </div>
                        <div class="col-sm-2 text-right">
                            <h3><a type="button" class="btn btn-link" href="<?php echo base_url(); ?>logged/message"> &larr; Volver al listado</a></h3>
                        </div>
                    </div>
                    <hr>

                    <?php if(!empty($message)){ ?>
                        <div class="alert alert-<?php echo $message["status"]; ?> alert-dismissible fade in" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                          <strong><i class="glyphicon glyphicon-info-sign"></i> Observación: </strong><?php echo $message["message"]; ?>
                        </div>
                    <?php } ?>
                </div>
                
                <div class="col-lg-7" style="margin:0 auto 20px auto;">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Crear nuevo aviso</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" action="<?php echo base_url();?>logged/message/submit" method="post" role="form" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="titulo" class="col-sm-4 control-label">Asunto</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" id="titulo" name="titulo" value="<?php echo set_value('titulo'); ?>" type="text">
                                        <?php echo form_error('titulo', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="bajada" class="col-sm-4 control-label">Descripción Breve <br><small>(250 caractéres)</small></label>
                                    <div class="col-sm-8">
                                        <textarea id="bajada" name="bajada" class="form-control" rows="3"><?php echo set_value('bajada'); ?></textarea>
                                        <?php echo form_error('bajada', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label for="cuerpo" class="col-sm-4 control-label">Descripción Completa</label>
                                    <div class="col-sm-8">
                                        <textarea id="cuerpo" name="cuerpo" class="form-control" rows="10"><?php echo set_value('cuerpo'); ?></textarea>
                                        <?php echo form_error('cuerpo', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                </div>  
                                <!-- <div class="form-group">
                                    <label for="adjunto" class="col-sm-4 control-label">Adjunto (.jpg, .png)</label>
                                    <div class="col-sm-7">
                                        <input type="file" id="adjunto" name="userfile" class="form-control">
                                    </div>
                                </div>   
                                -->
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-7">
                                        <input type="hidden" name="accion" value="<?php echo $accion; ?>">
                                        <button type="submit" class="btn btn-default">Enviar formulario</button>
                                        <a href="<?php echo base_url(); ?>" type="button" class="btn btn-link"> &larr; Cancelar</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <?php if(!empty($message)){ ?>
                    <div class="alert alert-<?php echo $message["status"]; ?> fade in" id="request-alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <span id="request-message"><strong>Acción realizada: </strong><?php echo $message["message"]; ?></span>
                    </div>
                    <?php } ?>
                </div>
                <div class="col-lg-5">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Datos de Emisor</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="post">
                                <div class="form-group">
                                    <label for="titulo" class="col-sm-4 control-label">Tipo de Emisor</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" value="<?php echo $emisor["tipo"]; ?>" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="titulo" class="col-sm-4 control-label">Nombre</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" value="<?php echo $emisor["nombre"]; ?>" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="titulo" class="col-sm-4 control-label">Email</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" value="<?php echo $emisor["email"]; ?>" readonly="">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Datos de Receptor</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="tipo_receptor" class="col-sm-4 control-label">Tipo de Receptor</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="tipo_receptor" id="tipo_receptor">
                                            <option value="usuario">Equipo Alumni</option>
                                            <option value="registro-individual">Registro Alumni individual</option>
                                            <option value="registro">Registro Alumni grupal</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="receptor_nombre" class="col-sm-4 control-label">Nombre</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="receptor_nombre" id="receptor_nombre">
                                    </div>
                                </div>
                                
                                 <div class="form-group">
                                    <label for="receptor_rut" class="col-sm-4 control-label">Rut</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="receptor_rut" id="receptor_rut">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="receptor_pais" class="col-sm-4 control-label">Pais</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="receptor_pais" name="receptor_pais">
                                            <option value=""> -- Selecciona un País -- </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="receptor_region" class="col-sm-4 control-label">Región</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="receptor_region" name="receptor_region">
                                            <option value=""> -- Selecciona una Región -- </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="receptor_ciudad" class="col-sm-4 control-label">Ciudad</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="receptor_ciudad" name="receptor_ciudad">
                                            <option value=""> -- Selecciona una Ciudad -- </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="receptor_comuna" class="col-sm-4 control-label">Comuna</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="receptor_comuna" name="receptor_comuna">
                                            <option value=""> -- Selecciona una Comuna -- </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="receptor_tipo_carrera" class="col-sm-4 control-label">Tipo de Carrera</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="receptor_tipo_carrera" name="receptor_tipo_carrera">
                                            <option value=""> -- Selecciona un tipo de carrera -- </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="receptor_carrera" class="col-sm-4 control-label">Carrera</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="receptor_carrera" name="receptor_carrera">
                                             <option value=""> -- Selecciona una Carrera -- </option>
                                        </select>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label for="receptor_campus" class="col-sm-4 control-label">Campus</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="receptor_campus" name="receptor_campus">
                                            <option value=""> -- Selecciona un Campus -- </option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

