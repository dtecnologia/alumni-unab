<!-- Header start -->
<header>
    <div class="row">
        <div class="col-sm-12">
            <h3 class="pull-left"><i class="glyphicon glyphicon-edit"></i> Solicitudes</h3>
        </div>
    </div>
</header>
<!---- Header end -->


<div id="content">
    <section id="navigation">
        <div class="container-fluid" style="padding-top:25px;">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-10">
                            <h3>Módulo de solicitudes <small>Administra tus solicitudes</small></h3>
                        </div>
                        <div class="col-sm-2 text-right">
                            <h3><a type="button" class="btn btn-primary" href="<?php echo base_url(); ?>logged/request/new"><i class="glyphicon glyphicon-plus-sign"></i> Nueva Solicitud</a></h3>
                        </div>
                    </div>
                    <hr>

                    <?php if(!empty($message)){ ?>
                        <div class="alert alert-<?php echo $message["status"]; ?> alert-dismissible fade in" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                          <strong><i class="glyphicon glyphicon-info-sign"></i> Observación: </strong><?php echo $message["message"]; ?>
                        </div>
                    <?php } ?>



                    <div class="panel panel-default">
                        <form name="form-search" id="form-search" method="post" action="<?php echo base_url(); ?>logged/search">
                            <div class="panel-heading">
                                <h4 class="panel-title">Filtros de búsqueda</h4>
                            </div>
                            <div class="panel-body">
                                <div class="form-inline" role="form">
                                    <div class="form-group col-lg-3">
                                        <label for="rut">RUT de Registro</label>
                                        <input class="form-control" id="rut" name="rut" placeholder="Ej. 16786123-0" type="text">
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="nombre">Nombre / Apellido</label>
                                        <input class="form-control" id="nombre" name="nombre" placeholder="Ej. Araneda" type="text">
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="email">Email</label>
                                        <input class="form-control" id="email" name="email" placeholder="Ej. alumni@unab.cl" type="text">
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="estado">Estado</label>
                                        <select class="form-control" id="estado">
                                            <option value="">-- Selecciona estado --</option>
                                            <option value="1">Registrado</option>
                                            <option value="2">Solicitado</option>
                                            <option value="0">Precargado</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer" style="text-align: right;">
                                <input type="hidden" name="bloque" value="update">
                                <button type="submit" class="btn btn-primary btn-sm">Realizar Búsqueda</button>
                                <button type="submit" class="btn btn-default btn-sm">Búsqueda Avanzada</button>
                            </div>
                        </form>
                    </div>





                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Solicitudes ingresadas</h4>
                        </div>
                        <div class="panel-body">
                        <?php if(count($solicitudes) == 0 ){ ?>
                            <p><i class="glyphicon glyphicon-info-sign"></i> Sin solicitudes ingresadas, para crear una nueva haz click en el botón superior.</p>
                        <?php } else { ?>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th style="width: 2%">ID</th>
                                        <th style="width: 17%">Tipo de solicitud</th>
                                        <th style="width: 29%">Asunto</th>
                                        <th style="width: 10%">Atendido por</th>
                                        <th style="width: 12%">Fecha</th>
                                        <th style="width: 5%">Estado</th>
                                        <th style="width: 15%; text-align: right;">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach($solicitudes as $row){ ?>
                                    <tr>
                                        <td><?php echo $row["solicitud"]->getIdSolicitud(); ?></td>
                                        <td><?php echo $row["tipo_solicitud"]->getNombre(); ?></td>
                                        <td><?php echo $row["solicitud"]->getNombre(); ?></td>
                                        <td><?php echo (is_object($row["usuario"])) ? $row["usuario"]->getNombre() : $row["usuario"]; ?></td>
                                        <td><?php echo date('d-m-Y H:i', strtotime($row["solicitud"]->getFechaIng())); ?></td>
                                        <td><?php echo $row["solicitud"]->getEstado(); ?></td>
                                        
                                        <td style="text-align: right;">
                                            <!-- <a class="btn btn-xs btn-concrete" href="javascript:void(0)"><i class="glyphicon glyphicon-ok"></i> Activar</a> -->
                                            <a class="btn btn-xs btn-concrete" href="<?php echo base_url()."logged/request/detail/".$row["solicitud"]->getIdSolicitud(); ?>"><i class="glyphicon glyphicon-pencil"></i> Editar</a>
                                            <a class="btn btn-xs btn-concrete" href="javascript:void(0)"><i class="glyphicon glyphicon-remove"></i> Archivar</a>
                                        </td>
                                    </tr>
                                <?php } ?>    
                                </tbody>
                            </table>
                        <?php } ?>
                        </div>
                        <?php if($total_registros > 1){ ?>
                        <div class="panel-footer">
                            <div class="pull-left">
                                <div aria-relevant="all" aria-live="polite" role="alert" id="basic2_info" class="dataTables_info"><?php echo $total_registros; ?> registros ingresados, en <?php echo $total_paginas; ?> página(s)</div>
                            </div>
                            <?php if($total_paginas > 1){ ?>
                            <div class="pull-right">
                                <div id="basic2_paginate" class="dataTables_paginate paging_bs_normal">
                                    <ul class="pagination">
                                        <li class="prev disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span>&nbsp;</a></li>
                                        <li class="active"><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">5</a></li>
                                        <li class="next"><a href="#">&nbsp;<span class="glyphicon glyphicon-chevron-right"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="clearfix">
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>