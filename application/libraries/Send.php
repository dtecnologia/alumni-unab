<?php if (!defined('BASEPATH')) exit('No permitir el acceso directo al script'); 

class Send {
	
	private $CI;
	
	private $bcc;
	private $from;
	
	function __construct()
	{
		$this->CI =& get_instance(); #Contiene las propiedades del codeigniter

		#llamar librerias y herlpers
		$this->CI->load->library('email');
		$this->CI->load->helper(array('url', 'rut'));
		
		#Declarar receptores por defecto.
		$this->bcc = array("daniel.araneda.h@gmail.com");
		$this->from = "unab@alumni.cl";

		#Declarar estructura html
		$config['protocol'] = 'mail';
		$config['wordwrap'] = FALSE;
		$config['mailtype'] = 'html';
		
		$this->CI->email->initialize($config);
		
	}
	
	function recovery($registro)
	{
		if(!empty($registro))
		{
			$this->CI->email->from($this->from, 'Alumni UNAB');
			$this->CI->email->to($registro->getEmail());
			$this->CI->email->bcc($this->bcc);
			$this->CI->email->subject('Alumni UNAB - Recuperar contraseña');
			$this->CI->email->message($this->CI->load->view('mails/recovery', array('registro' => $registro) ,true));
			return $this->CI->email->send();
		}else{
			return false;
		}
	}

	function signin($registro)
	{
		if(!empty($registro))
		{
			$this->CI->email->from($this->from, 'Alumni UNAB');
			$this->CI->email->to($registro->getEmail());
			$this->CI->email->bcc($this->bcc);
			$this->CI->email->subject('Alumni UNAB - Solicitud de registro');
			$this->CI->email->message($this->CI->load->view('mails/signin', array('registro' => $registro) ,true));
			return $this->CI->email->send();
		}else{
			return false;
		}
	}
}