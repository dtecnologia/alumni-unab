<?php if (!defined('BASEPATH')) exit('No permitir el acceso directo al script'); 

class Folder {
	
	private $CI;
	
	function __construct(){
		$this->CI =& get_instance(); 
	}
	
    function userFolder($idregistro = NULL, $tipo){
    	
    	if(empty($idregistro))
    		$idregistro = 'anonimo';
    	
		$base = $this->CI->input->server('DOCUMENT_ROOT');
		$ruta = $base."/files/".$idregistro;
		
		if(!is_dir($ruta)){
			if(!mkdir($ruta, 0755))
				echo "Error creando la ruta:: ".$ruta;
		}			
		
		$ruta = $ruta."/".$tipo;
		
		if(!is_dir($ruta)){
			if(!mkdir($ruta, 0755))
				echo "Error creando la ruta:: ".$ruta;
		}	
		
		if(is_dir($ruta))
			return $ruta;
		else
			return false;
    }
	
}

/* End of file Someclass.php */