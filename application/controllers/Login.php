<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index() {

        #Validar envío de formulario para login
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user', 'Usuario', 'trim|required');
        $this->form_validation->set_rules('pass', 'Contraseña', 'trim|required|callback__check_login');
        if ($this->form_validation->run() === TRUE) {
            #redireccionar a l home logged
            redirect(base_url() . 'logged/home');
        }

        #Mostrar layout de login
        $data_frame["body"] = $this->load->view('unlogged/home', '', true);
        $this->load->view('unlogged/frame', $data_frame);
    }

    /* METODO PARA REESTABLECER CONTRASEÑA */

    public function recovery() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|callback__check_email');
        if ($this->form_validation->run() === TRUE) {
            $message["status"] = "success";
            $message["message"] = "Se ha enviado a la dirección ingresada tus credenciales de acceso";
            $data_recovery['message'] = $message;
        }

        $data_frame["body"] = $this->load->view('unlogged/recovery', $data_recovery, true);
        $this->load->view('unlogged/frame', $data_frame);
    }

    /* FUNCIONES "PRIVADAS" */

    /*
     * 	Verifica usuario y contraseña si son válidos, guarda en session, retorna boolean
     */

    public function _check_login($pass) {

        $this->load->helper(array('email', 'rut'));
        $usuario = $this->input->post("user");
        $registro = array();
        #Si es email

        if (valid_email($usuario)) {
            #set registro by email
            $registro = array('email' => $usuario, 'password' => $pass);
        }

        if ($rut = isRut($usuario)) {
            #set registro by rut
            $registro = array('rut' => $rut, 'password' => $pass);
        }

        if (!empty($registro)) {
            #get registro by rut
            $this->load->model("Registro_model");
            if ($this->Registro_model->get(false, $registro)) {
                #si está activo
                if ($this->Registro_model->getActivo() == 1) {
                    #si está solicitado
                    if ($this->Registro_model->getSolicitado() == 1) {
                        $this->session->set_userdata('idregistro', $this->Registro_model->getIdRegistro());
                        $this->session->set_userdata('email', $this->Registro_model->getEmail());
                        $this->session->set_userdata('nombre', $this->Registro_model->getNombreCompleto());
                        $this->session->set_userdata('perfil', 'registro');
                        $this->session->set_userdata('estado', 'solicitado');
                        $this->session->set_userdata('logged', true);
                        return true;
                    }
                    #si está registrado
                    if ($this->Registro_model->getRegistrado() == 1) {
                        $this->session->set_userdata('idregistro', $this->Registro_model->getIdRegistro());
                        $this->session->set_userdata('email', $this->Registro_model->getEmail());
                        $this->session->set_userdata('nombre', $this->Registro_model->getNombreCompleto());
                        $this->session->set_userdata('perfil', 'registro');
                        $this->session->set_userdata('logged', true);
                        return true;
                    }

                    #ningun otro
                    $this->form_validation->set_message('_check_login', 'Usuario precargado, envíe una solicitud de registro para activar su cuenta.');
                    return false;
                } else { #si está inactivo o eliminado
                    $this->form_validation->set_message('_check_login', 'Usuario inhabilitado para iniciar sesión, consulte con administrador.');
                    return false;
                }
            } else {
                $this->form_validation->set_message('_check_login', 'Usuario y contraseña no coinciden con nuestros registros.');
                return false;
            }
        } else {
            #get user by username
            $this->load->model("Usuario_model");
            if ($this->Usuario_model->get(false, array('user' => $usuario, 'pass' => $pass))) {
                if ($this->Usuario_model->getActivo() == 1) {
                    #guardar en session y retornar
                    $this->session->set_userdata('idusuario', $this->Usuario_model->getIdUsuario());
                    $this->session->set_userdata('user', $this->Usuario_model->getUser());
                    $this->session->set_userdata('nombre', $this->Usuario_model->getNombre());
                    $this->session->set_userdata('perfil', 'usuario');
                    $this->session->set_userdata('logged', true);
                    return true;
                } else {
                    #guardar mensaje y retornar false
                    $this->form_validation->set_message('_check_login', 'Usuario inhabilitado para iniciar sesión, consulte con administrador.');
                    return false;
                }
            } else {
                $this->form_validation->set_message('_check_login', 'Usuario y contraseña no coinciden con nuestros registros.');
                return false;
            }
        }
    }

    public function _check_email($email) {
        $this->load->model("Registro_model");
        if ($this->Registro_model->get(false, array('email' => $email))) {
            #enviar correo con contraseña
            $this->load->library('send');
            if ($this->send->recovery($this->Registro_model)) {
                return true;
            } else {
                $this->form_validation->set_message('_check_email', 'Ha ocurrido un error al enviar la notificacion a su cuenta de email.');
                return false;
            }
        } else {
            $this->form_validation->set_message('_check_email', 'E-mail ingresado no coincide con nuestros registros.');
            return false;
        }
    }

}
