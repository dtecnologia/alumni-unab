<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() 
	{
		$this->load->model('Registro_model');
		#Validar envío de formulario para registro
		$this->load->library('form_validation');
		$this->load->helper('rut');

        $this->form_validation->set_rules('rut', 'Rut', 'trim|required|isRut|callback__check_rut', array('isRut' => 'El {field} ingresado es inválido'));
        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|callback__check_email');
        $this->form_validation->set_rules('pass', 'Contraseña', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('repass', 'Repetir Contraseña', 'required|matches[pass]');
		if ($this->form_validation->run() === TRUE) 
		{
			#insertar nuevo registro en estado de solicitado
			$datos = array('rut' => isRut($this->input->post('rut')),
						   'nombre_completo' => $this->input->post('nombre'),
						   'email' => $this->input->post('email'),
						   'password' => $this->input->post('pass'),
						   'activo' => 1,
						   'solicitado' => 1,
						   'fechaingreso' => date('Y-m-d H:i:s'),
						   'fechasolicitud' => date('Y-m-d H:i:s'));
			if($idregistro = $this->Registro_model->insert($datos))
			{
				#insertar solicitud de nuevo registro
				$this->load->model('Solicitud_model');
				$datos_solicitud = array('idregistro' => $idregistro,
										 'idtipo_solicitud' => 2,
										 'nombre' => 'Solicitud de registro',
										 'descripcion' => 'Registro desde la plataforma alumni de '.$this->input->post('nombre').' rut '.isRut($this->input->post('rut')),
										 'estado' => 'INGRE',
										 'fecha_ing' => date('Y-m-d H:i:s'));
				if($idsolicitud = $this->Solicitud_model->insert($datos_solicitud))
				{
					#insertar adjuntos
					#-- SIN ADJUNTOS

					#insertar historial
					$this->load->model('Solicitud_historial_model');
					$datos_historial = array('idsolicitud' => $idsolicitud,
											 'descripcion' => 'Solicitud creada desde portal alumni',
											 'estado' => 'INGRE',
											 'fecha' => date('Y-m-d H:i:s'),
											 'idregistro' => $idregistro);
					if($this->Solicitud_historial_model->insert($datos_historial))
					{
						$message['status'] = 'success';
						$message['message'] = 'Solicitud de registro realizada exitosamente, accede a la plataforma para revisar el estado de tu solicitud.';
						$data_signin["message"] = $message;
					}
					else
					{
						$message['status'] = 'warning';
						$message['message'] = 'Solicitud de registro realizada exitosamente, el historial de solicitud no se encuentra disponible';
						$data_signin["message"] = $message;
					}
				}
				else
				{
					$message['status'] = 'warning';
					$message['message'] = 'Registro realizado exitosamente, la solicitud no se encuentra disponible';
					$data_signin["message"] = $message;
				}
				#enviar correo	
			}
			else
			{
				$message['status'] = 'danger';
				$message['message'] = 'Ha ocurrido un error al insertar el registro, inténtelo más tarde';
				$data_signin["message"] = $message;
			}
			
		}
		$data_frame["body"] = $this->load->view('unlogged/signin', $data_signin, true);
		$this->load->view('unlogged/frame', $data_frame); 
	}





	public function _check_rut($rut)
	{
		#consultar si existe este rut
		if($this->Registro_model->get('boolean', array('rut' => $rut))){
			$this->form_validation->set_message('_check_rut', 'Rut ingresado ya existe en nuestros registros.');
			return false;
		}
		return true;
	}


	public function _check_email($email)
	{
		if(!$this->Registro_model->get('boolean', array('email' => $email)))
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('_check_email', 'E-mail ingresado ya existe en nuestros registros.');
			return false;
		}
	}

}
