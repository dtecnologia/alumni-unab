<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
    {
        parent::__construct();

        #Validar logged
        if(!$this->session->userdata('logged'))
			redirect(base_url(), 'header');
    }

	public function index() 
	{
		
		$this->load->model('Registro_model');
		$this->load->model('Solicitud_model');
		$this->load->model('Tipo_solicitud_model');
		$this->load->model('Registro_laboral_model');

		$this->load->helper('rut');


		/*
		 * CONFIGURAR SOLICITUDES INGRESADAS
		 */
		$config_solicitud = array();

		if($this->session->userdata('perfil') == 'usuario')
		{
			/*
			 * ULTIMOS REGISTROS Y CONTADORES
			 */
			$data_home["config"]["contadores"] = true;
			$data_home["ultimo_registro"] = $this->Registro_model->get('object', array('registrado' => 1,'activo' => 1), array('order' => 'fecharegistro desc', 'limit' => 1));
			$data_home["count"]["registrado"] = $this->Registro_model->get('count', array('registrado' => 1,'activo' => 1));
			$data_home["count"]["solicitado"] = $this->Registro_model->get('count', array('solicitado' => 1,'activo' => 1));
			$data_home["count"]["precargado"] = $this->Registro_model->get('count', array('solicitado' => 0,'registrado' => 0, 'activo' => 0));
			$data_home["count"]["total"] = $this->Registro_model->get('count', array(NULL));
			

		}	
		else
		{
			/*
			 * CONFIGURAR SOLICITUDES INGRESAS
			 */
			if($this->session->userdata('idregistro') != '')
				$config_solicitud = array('idregistro' => $this->session->userdata('idregistro'));
		}


		/*
		 * SOLICITUDES INGRESADAS (según perfil)
		 */
		$data_home["config"]["solicitudes"] = true;
		if($solicitudes = $this->Solicitud_model->get('all', $config_solicitud, array('order' => 'fecha_ing desc', 'limit' => 10)))
		{
			foreach($solicitudes as $row)
			{
				$tmp["solicitud"] = $row;
				$tmp["tipo_solicitud"] = $this->Tipo_solicitud_model->get('object', array('idtipo_solicitud' => $row->getIdTipoSolicitud()));
				$data_home["solicitudes"][] = $tmp;
			}
		}else
			$data_home["solicitudes"] = array();

                
                /*
                * REGISTROS POR RUBROS
                */
                $data_home["config"]["rubros"] = true;
                $data_home["registro_rubro"] = $this->Registro_laboral_model->getByRubro(NULL, 10);
                
                

		#LLamar a las vistas
		$data_frame["body"] = $this->load->view('logged/home', $data_home, true);
		$this->load->view('logged/frame', $data_frame); 
	}

}