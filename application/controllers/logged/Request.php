<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends CI_Controller {

	public function __construct()
    {
            parent::__construct();

            #Validar logged
            if(!$this->session->userdata('logged'))
				redirect(base_url(), 'header');
    }

	public function index() 
	{
		#incorporar clases, librerias y helpers
		$this->load->model('Registro_model');
		$this->load->model('Solicitud_model');
		$this->load->model('Tipo_solicitud_model');
		$this->load->model('Usuario_model');

		$custom = array('limit' => 20, 'pagina' => 1);

		#consultar solicitudes del registro
		if($this->session->userdata('perfil') == 'registro')
		{
			#codigo para registros
			$idregistro = $this->session->userdata('idregistro');
			if($registro = $this->Registro_model->get('object', array('idregistro' => $idregistro, 'activo' => 1))){
				$this->Solicitud_model->setIdRegistro($idregistro);
			}
			else
			{
				$data["message"]["message"] = 'El perfil a consultar no existe, o bien, la sesión ha finalizado. Reingresa al sistema.';
				$data["message"]["status"] = 'danger';
			}
		}

		$data["total_solicitudes"] = $this->Registro_model->get('count', $filtros);
		$data["total_paginas"] = ceil($data["total_solicitudes"] / $custom["limit"]);
		$data["solicitudes"] = array();
		if($solicitudes = $this->Solicitud_model->get('all', $filtros, $custom)){
			foreach($solicitudes as $solicitud)
			{
				$solicitudes["solicitud"] = $solicitud;
				$solicitudes["tipo_solicitud"] = $this->Tipo_solicitud_model->get('object', array('idtipo_solicitud' => $solicitud->getIdTipoSolicitud()));
				$solicitudes["usuario"] = ($solicitud->getIdUsuarioMod()) ? $this->Usuario_model->get('object', array('idusuario' => $solicitud->getIdUsuarioMod())) : "No atendido";
				$solicitudes["registro"] = $registro;
				$data["solicitudes"][] = $solicitudes;	
			}
		}		

		$data_frame["body"] = $this->load->view('logged/request', $data, true);
		$this->load->view('logged/frame', $data_frame); 
	}

	public function detail($id) 
	{
		#validar id
		if(is_numeric($id) && !empty($id))
		{
			$this->load->model('Solicitud_model');
			$this->load->model('Tipo_solicitud_model');
			$this->load->model('Usuario_model');
			$this->load->model('Registro_model');
                        $this->load->helper('rut');
			#consultar solicitud
			$datos_solicitud["idsolicitud"] = $idsolicitud;
			if($this->session->userdata('perfil') == 'registro')
			{
				$datos_solicitud["idregistro"] = $this->session->userdata('idregistro');
			}
			if($solicitud = $this->Solicitud_model->get('object', $datos_solicitud))
			{
				#$this->load->model('Solicitud_adjunto_model');
				$this->load->model('Solicitud_mensaje_model');
				$this->load->model('Solicitud_historial_model');
				#tipos de solicitudes
				$data_request["tipo_solicitudes"] = $this->Tipo_solicitud_model->get('all');
				#guardar datos basicos de solicitud
				$data_request["solicitud"] = $solicitud;
                                #registro
                                if($solicitud->getIdRegistro() != ''){
                                    if($this->Registro_model->get(false, array('idregistro'=>$solicitud->getIdRegistro()))){
                                        $data_request["registro"]["nombre"] = $this->Registro_model->getNombre();
                                        $data_request["registro"]["rut"] = isRut($this->Registro_model->getRut());
                                        $data_request["registro"]["email"] = $this->Registro_model->getEmail();
                                    }else{
                                        $data_request["registro"]["nombre"] = $solicitud->getNombre();
                                        $data_request["registro"]["rut"] = $solicitud->getRut();
                                        $data_request["registro"]["email"] = $solicitud->getEmail();
                                    }
                                }else{
                                    $data_request["registro"]["nombre"] = $solicitud->getNombre();
                                    $data_request["registro"]["rut"] = $solicitud->getRut();
                                    $data_request["registro"]["email"] = $solicitud->getEmail();
                                }
				#guardar adjuntos                                
				$data_request["adjuntos"] = NULL;
				#$this->Solicitud_adjunto_model->get('object', array('idregistro' => $idregistro));
				#guardar mensajes
				$data_request["mensajes"] = array();
				if($mensajes = $this->Solicitud_mensaje_model->get('all', array('idsolicitud' => $solicitud->getIdSolicitud()))){
					foreach($mensajes as $row)
					{
						$tmp["mensaje"] = $row;
						$tmp["usuario"] = ($row->getIdUsuarioIng() != '') ? $this->Usuario_model->get('object', array('idusuario' => $row->getIdUsuarioIng())) : NULL;
						if(empty($tmp["usuario"]))
							$tmp["registro"] = $this->Registro_model->get('object', array('idregistro' => $solicitud->getIdRegistro()));
						$data_request["mensajes"][] = $tmp;
					}
				}

				#guardar historial
				$data_request["historial"] = $this->Solicitud_historial_model->get('all', array('idsolicitud' => $solicitud->getIdSolicitud()));



				#llamar vista
				$data_frame["body"] = $this->load->view('logged/request-detail', $data_request, true);
				$this->load->view('logged/frame', $data_frame); 	
			}
			else{
				#mensaje de error
				$message["status"] = "danger";
				$message["message"] = "La solicitud seleccionada no se encuentra disponible";
				$this->session->flashdata('message', $message);
				$this->index();
			}
		}
		else
		{
			$message["status"] = "danger";
			$message["message"] = "El código de solicitud ingresada es inválido";
			$this->session->flashdata('message', $message);
			$this->index();
		}
		
	}

	public function message($accion = NULL)
	{
				
		switch($accion)
		{

			case "insert":
				#validar si viene de ajax o no.
				if($this->input->is_ajax_request()) 
				{
					$this->load->library('form_validation');
					$this->form_validation->set_rules('descripcion', 'Mensaje', 'trim|required');
					$this->form_validation->set_rules('idsolicitud', 'Solicitud', 'required|is_natural_no_zero|callback__check_solicitud');

					if ($this->form_validation->run() === TRUE) 
					{
						#insertar mensaje
						$this->load->model('Solicitud_mensaje_model');
						$this->load->model('Solicitud_historial_model');

						if($this->Solicitud_model->get(false, array('idsolicitud' => $this->input->post('idsolicitud')))){
							$data_mensaje["descripcion"] = $this->input->post('descripcion');
							$data_mensaje["idsolicitud"] = $this->input->post('idsolicitud');
							$data_mensaje["nombre"] = "Mensajería";
							$data_mensaje["fecha_ing"] = date('Y-m-d H:i:s');
							$data_mensaje["idusuario_ing"] = ($this->session->userdata('perfil') == 'usuario') ? $this->session->userdata('idusuario') : NULL;

							if($this->Solicitud_mensaje_model->insert($data_mensaje))
							{
								$response["status"] = "success";
								$response["message"] = "Exito";		
								$response["html"] = $this->load->view('ajax/request_message', $data_mensaje, true);

								#si es usuario, actualizar última atención
								$this->_atendido();
								#insertar en historial
								$this->load->model('Solicitud_historial_model');
								$data_historial = array('idsolicitud' => $this->Solicitud_model->getIdSolicitud(),
														'estado' => $this->Solicitud_model->getEstado(false),
														'descripcion' => $this->session->userdata('nombre'). ' ha ingresado un mensaje en la solicitud',
														'fecha' => date('Y-m-d H:i:s'));
								if($this->session->userdata('perfil') == 'usuario')
									$data_historial["idusuario"] = $this->session->userdata('idusuario');
								else
									$data_historial["idregistro"] = $this->session->userdata('idregistro');
								$this->Solicitud_historial_model->insert($data_historial);
							}
							else
							{
								$response["status"] = "danger";
								$response["message"] = "Ha ocurrido un error al insertar el mensaje, favor reintentar más tarde";		
							}
						}
						else
						{
							$response["status"] = "danger";
							$response["message"] = "La solicitud seleccionada no se encuentra disponible, o no tienes permisos para modificarla";		
						}
					}
					else
					{
						$response["status"] = "danger";
						$response["message"] = validation_errors();
					}
					echo json_encode($response);
				}
			break;

		}
	}

	public function _check_solicitud($idsolicitud)
	{
		$this->load->model('Solicitud_model');
		if($solicitud = $this->Solicitud_model->get('object', array('idsolicitud' => $idsolicitud)))
		{
			if($this->session->userdata('perfil') == 'registro')
			{
				if($this->session->userdata('idregistro') == $solicitud->getIdRegistro())
					return true;
				else{
					$this->form_validation->set_message('_check_solicitud', 'No tiene permisos para modificar la solicitud.');
					return false;
				}
			}
			else
				return true;	
		}
		else{
			$this->form_validation->set_message('_check_solicitud', 'La solicitud que desea modificar no existe.');
			return false;
		}
	}

	public function _atendido($estado = NULL)
	{	
		if($this->session->userdata('perfil') == 'usuario')
		{
			if(!empty($estado))
				$data["estado"] = $estado;
			$data["fecha_mod"] = date('Y-m-d H:i:s');
			$data["idusuario_mod"] = $this->session->userdata('idusuario');
			if($this->Solicitud_model->update($data))
				return true;
			else
				return false;
		}
		else
			return false;
	}

}

