<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

        public function __construct() {
            parent::__construct();
            #Validar logged
            if(!$this->session->userdata('logged'))
				redirect(base_url(), 'header');
        }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {
            #Libreria
            $this->load->library('session');
            $this->load->library('pagination');
            #Helper
            $this->load->helper('datecustom');
            #CONSULTAR REGISTROS.
            $this->load->model("Registro_model");
            #Academicos
            $this->load->model("Tipo_carrera_model");
            $this->load->model("Carrera_model");
            $this->load->model("Campus_model");
            #Geograficos
            $this->load->model("Pais_model");
            $this->load->model("Region_model");
            $this->load->model("Ciudad_model");
            $this->load->model("Comuna_model");
            #Laborales
            $this->load->model("Tipo_cargo_model");
            $this->load->model("Rubro_model");
            #PAGINADOR
            if ($this->uri->segment(3) === FALSE) {
                $pagina = 1;
            }else {
                $pagina = ($this->uri->segment(3));
            }
            $custom = array('limit' => 20, 'pagina' => $pagina);
            
            #FILTROS
            $filtros = array();
            $this->validateFormSearch($filtros);
            
            #SEARCH
            if((!empty($filtros) && is_array($filtros))) {
                
                $data["filtros"] = $filtros;
                $this->session->set_flashdata('filtros', $filtros);
                /**
                 * TODO falta filtro.
                 */
                $data["tipo_carreras"] = $this->Tipo_carrera_model->get('all');
                $data["carreras"] = $this->Carrera_model->get('all', array('idtipocarrera' => $filtros["idtipocarrera"]));
                $data["campus"] = $this->Campus_model->get('all', $filtros);
                $data["paises"] = $this->Pais_model->get('all');
                $data["regiones"] = $this->Region_model->get('all', $filtros);
                $data["ciudades"] = $this->Ciudad_model->get('all', $filtros);
                $data["comunas"] = $this->Comuna_model->get('all', $filtros);
                $data["tipos_cargos"] = $this->Tipo_cargo_model->get('all');
                $data["rubros"] = $this->Rubro_model->get('all', $filtros);
                $data["registros"] = $this->Registro_model->get('all',$filtros, $custom);
                $data["total_registros"] = $this->Registro_model->get('count', $filtros);
                $data["pagination"] = $this->paginator($data["total_registros"]);
                
            } else {
                $data["tipo_carreras"] = $this->Tipo_carrera_model->get('all');
                $data["carreras"] = $this->Carrera_model->get('all');
                $data["campus"] = $this->Campus_model->get('all');
                $data["paises"] = $this->Pais_model->get('all');
                $data["regiones"] = $this->Region_model->get('all');
                $data["ciudades"] = $this->Ciudad_model->get('all');
                $data["comunas"] = $this->Comuna_model->get('all');
                $data["tipos_cargos"] = $this->Tipo_cargo_model->get('all');
                $data["rubros"] = $this->Rubro_model->get('all');
                $data["total_registros"] = $this->Registro_model->get('count', $filtros);
                $data["pagination"] = $this->paginator($data["total_registros"]);
                $data["registros"] = $this->Registro_model->get('all', $filtros, $custom);
            }
            if(!empty($filtros["desdeRegistro"])){
                
                $data["desdeRegistro"] = change_format_date($filtros["desdeRegistro"], 'Y-m-d', 'd/m/Y');//nice_date($filtros["desdeRegistro"], 'd/m/Y');
            }
            if(!empty($filtros["hastaRegistro"])){
                $data["hastaRegistro"] = change_format_date($filtros["hastaRegistro"], 'Y-m-d', 'd/m/Y');//nice_date($filtros["hastaRegistro"], 'd/m/Y');
                
            }


            $data_frame["body"] = $this->load->view('logged/search', $data, true);
            $this->load->view('logged/frame', $data_frame); 
	}
        
        private function paginator($cantidad){
            
            $config['base_url'] = base_url().'/logged/search/';
            $config['per_page'] = 20;
            $config['total_rows'] = $cantidad;
            $config['use_page_numbers'] = TRUE;
            
            //Custom
            $config['uri_segment'] = 3;
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
            $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] = "<li>";
            $config['next_tagl_close'] = "</li>";
            $config['prev_tag_open'] = "<li>";
            $config['prev_tagl_close'] = "</li>";
            $config['first_tag_open'] = "<li>";
            $config['first_tagl_close'] = "</li>";
            $config['last_tag_open'] = "<li>";
            $config['last_tagl_close'] = "</li>";
            $config['last_link'] = 'Último';
            $config['first_link'] = 'Primero';
            
            
            $this->pagination->initialize($config);
            return $this->pagination;
        }
        
        private function validateFormSearch(&$filtros){
            
           if($this->session->flashdata('filtros') != ""){
                $filtros = $this->session->flashdata('filtros');
            }
           #Busqueda normal
            if(!is_null($this->input->post("rut"))){
                if ($this->input->post("rut") != $filtros["rut"])
                    $filtros["rut"] = $this->input->post("rut");
            }

            if(!is_null($this->input->post("nombre"))){
                if ($this->input->post("nombre") != $filtros["nombre_completo"])
                    $filtros["nombre_completo"] = $this->input->post("nombre");
            }
            if(!is_null($this->input->post("email"))){
                if ($this->input->post("email") != $filtros["email"])
                    $filtros["email"] = $this->input->post("email");
            }

            if(!is_null($this->input->post("estado"))){
                if ($this->input->post("estado") != $filtros["estado"])
                    $filtros["estado"] = $this->input->post("estado");
            }	
            #FILTROS AVANZADA
            #variable para ver ventana de busqueda avanzada
            $_bavanzada = false;
            #Filtros academicos
            if(!is_null($this->input->post("idtipocarrera"))){
                $_bavanzada = true;
                if ($this->input->post("idtipocarrera") != $filtros["idtipocarrera"])
                    $filtros["idtipocarrera"] = $this->input->post("idtipocarrera");
            }
            if(!is_null($this->input->post("carrera"))){
                $_bavanzada = true;
                if ($this->input->post("carrera") != $filtros["idcarrera"])
                    $filtros["idcarrera"] = $this->input->post("carrera");
            }
            if(!is_null($this->input->post("idcampus"))){
                $_bavanzada = true;
                if ($this->input->post("idcampus") != $filtros["idcampus"])
                    $filtros["idcampus"] = $this->input->post("idcampus");
            }
            #Filtros geograficos
            if(!is_null($this->input->post("pais"))){
                if ($this->input->post("pais") != $filtros["idpais"])
                    $filtros["idpais"] = $this->input->post("pais");
            }
            if(!is_null($this->input->post("region"))){
                $_bavanzada = true;
                if ($this->input->post("region") != $filtros["idregion"])
                    $filtros["idregion"] = $this->input->post("region");
            }
            if(!is_null($this->input->post("ciudad"))){
                $_bavanzada = true;
                if ($this->input->post("ciudad") != $filtros["idciudad"])
                    $filtros["idciudad"] = $this->input->post("ciudad");
            }
            if(!is_null($this->input->post("comuna"))){
                $_bavanzada = true;
                if ($this->input->post("comuna") != $filtros["idcomuna"])
                    $filtros["idcomuna"] = $this->input->post("comuna");
            }
            #Filtros laborales
            if(!is_null($this->input->post("tipo_cargo"))){
                $_bavanzada = true;
                if ($this->input->post("tipo_cargo") != $filtros["tipo_cargo"])
                    $filtros["tipo_cargo"] = $this->input->post("tipo_cargo");
            }
            if(!is_null($this->input->post("rubro"))){
                $_bavanzada = true;
                if ($this->input->post("rubro") != $filtros["rubro"])
                    $filtros["rubro"] = $this->input->post("rubro");
            }
            #Filtros fechas
            if(!is_null($this->input->post("desdeRegistro"))){
                $_desdeDateFU = change_format_date($this->input->post("desdeRegistro"), 'd/m/Y', 'Y-m-d');//nice_date($this->input->post("desdeRegistro"), 'Y-m-d');
                $_bavanzada = true;
                if ($_desdeDateFU != $filtros["desdeRegistro"])
                    $filtros["desdeRegistro"] = $_desdeDateFU;
            }
            if(!is_null($this->input->post("hastaRegistro"))){
                $_hastaDateFU = change_format_date($this->input->post("hastaRegistro"), 'd/m/Y', 'Y-m-d');
                $_bavanzada = true;
                if ($_hastaDateFU != $filtros["hastaRegistro"])
                    $filtros["hastaRegistro"] = $_hastaDateFU;
                
                
            } 
            $filtros["_bavanzada"] = $_bavanzada;
        }

}
