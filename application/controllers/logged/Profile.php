<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	private $message;

	public function __construct()
    {
            parent::__construct();

            #Validar logged
            if(!$this->session->userdata('logged'))
                redirect(base_url(), 'header');
    }

	public function index() 
	{
		#Si el perfil es Usuario, debe redireccionar a list.
		if($this->session->userdata('perfil') == 'usuario')
			$this->all();

		# -- MODELS
		$this->load->model('Registro_model');
		$this->load->model('Comuna_model');
		$this->load->model('Ciudad_model');
		$this->load->model('Region_model');
		$this->load->model('Pais_model');

		# -- HELPERS
		$this->load->helper(array('rut','form'));


		/* 
		 * Obtener combos académicos
		 */
		$this->load->model('Tipo_institucion_model');
		$this->load->model('Institucion_model');
		$this->load->model('Tipo_carrera_model');
		$this->load->model('Campus_model');
		$data_profile["tipo_institucion"] = $this->Tipo_institucion_model->get('all');
		$data_profile["tipo_carrera"] = $this->Tipo_carrera_model->get('all');
		$data_profile["campus"] = $this->Campus_model->get('all');
		
		/*
		 * Obtener combos laborales
		 */
		$this->load->model('Rubro_model');
		$this->load->model('Tipo_cargo_model');
		$data_profile["rubro"] = $this->Rubro_model->get('all', array('activo' => 1));
		$data_profile["tipo_cargo"] = $this->Tipo_cargo_model->get('all');


		#obtener datos de registro
		$registro = array('idregistro' => $this->session->userdata('idregistro'));
		if($registro = $this->Registro_model->get('object', $registro))
		{
			$data_profile["registro"] = $registro;
			#OBTENER LOCALIDADES
			if($data_profile["comuna"] = $this->Comuna_model->get('object', array('idcomuna' => $registro->getIdComuna())))
			{
				#obtener ciudad
				if($data_profile["ciudad"] = $this->Ciudad_model->get('object', array('idciudad' => $data_profile["comuna"]->getIdCiudad())))
				{
					#obtener region
					if($data_profile["region"] = $this->Region_model->get('object', array('idregion' => $data_profile["ciudad"]->getIdRegion())))
					{
						#obtener inputs localidades
						$data_profile["ciudades"] = $this->Ciudad_model->get('all', array('idregion' => $data_profile["region"]->getIdRegion()));
						$data_profile["comunas"] = $this->Comuna_model->get('all', array('idciudad' => $data_profile["ciudad"]->getIdCiudad()));
					}
				}
			}
			$data_profile["regiones"] = $this->Region_model->get('all', array('idpais' => 56));
			$data_profile["paises"] = $this->Pais_model->get('all');

			#OBTENER DATOS ACADEMICOS
			$this->load->model('Registro_academico_model');
			
			if($registro_academico = $this->Registro_academico_model->get('all', array('idregistro' => $registro->getIdRegistro()))){
				foreach($registro_academico as $row)
				{
					$data["registro_academico"] = $row;
					$data["institucion"] = $this->Institucion_model->get('object', array('idinstitucion' => $row->getIdInstitucion()));
					$data_profile["registro_academico"] .= $this->load->view('ajax/profile_academico', $data, true);
				}
			}

			#OBTENER DATOS LABORALES
			$this->load->model('Registro_laboral_model');
			
			if($registro_laboral = $this->Registro_laboral_model->get('all', array('idregistro' => $registro->getIdRegistro()))){
				foreach($registro_laboral as $row)
				{
					$data_profile["registro_laboral"].= $this->load->view("ajax/profile_laboral", array('row' => $row), true);
				}
			}

		}
		else
		{
			$message = "ERROR";
		}

		#mostrar vista
		if(!empty($this->message)){
			$data_profile["message"] = $this->message;
			$this->message = "";
		}
		$data_frame["body"] = $this->load->view('logged/profile', $data_profile, true);
		$this->load->view('logged/frame', $data_frame); 
	}


	public function basico()
	{
		#-- MODELS
		$this->load->model('Registro_model');
		#-- LIBRERY
		$this->load->library('form_validation');
		#-- HELPER
		$this->load->helper('date');

		$bloque = $this->input->post('bloque');
		switch ($bloque) {
			case 'update':
				
				$this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
				$this->form_validation->set_rules('email', 'email personal', 'trim|required|valid_email');
				$this->form_validation->set_rules('email_alumni', 'email alumni', 'trim|valid_email');
				$this->form_validation->set_rules('fechanacimiento', 'fecha de nacimiento', 'trim|required');
				$this->form_validation->set_rules('sexo', 'sexo', 'trim|required');
				$this->form_validation->set_rules('nacionalidad', 'pais natal', 'trim|required|is_natural');
				$this->form_validation->set_rules('comuna', 'comuna', 'trim|required|is_natural_no_zero');
				$this->form_validation->set_rules('direccion', 'dirección', 'trim|required');
				$this->form_validation->set_rules('telefono', 'teléfono fijo ', 'trim|required|numeric');
				$this->form_validation->set_rules('celular', 'teléfono móvil', 'trim|required|numeric');
				$this->form_validation->set_rules('postal', 'código postal', 'trim|numeric');
				if ($this->form_validation->run() === TRUE) {
					#redireccionar a l home logged

					$datos["idregistro"] = $this->session->userdata('idregistro');
					$datos["nombre"] = $this->input->post("nombre");
					$datos["apaterno"] = $this->input->post("apaterno");
					$datos["amaterno"] = $this->input->post("amaterno");
					$datos["email"] = $this->input->post("email");
					$datos["fechanacimiento"] = date('Y-m-d', strtotime($this->input->post("fechanacimiento")));
					$datos["sexo"] = $this->input->post("sexo");
					$datos["nacionalidad"] = $this->input->post("nacionalidad");
					$datos["idcomuna"] = $this->input->post("comuna");
					$datos["direccion"] = $this->input->post("direccion");
					$datos["telefono"] = $this->input->post("telefono");
					$datos["celular"] = $this->input->post("celular");
					$datos["postal"] = $this->input->post("postal");
					$datos["email_alumni"] = $this->input->post("email_alumni");

					if($this->Registro_model->update($datos))
					{
						$message["status"] = 'success';
						$message["message"] = 'Los datos ingresados han sido actualizados existosamente';
						$this->session->message = $message;
					}
					else
					{
						$message["status"] = 'danger';
						$message["message"] = 'Ha ocurrido un eror al actualizar los datos ingresados';
						$this->session->message = $message;
					}
				}
				else
				{
					$message["status"] = 'danger';
					$message["message"] = 'Uno o más campos son inválidos, revisa el formulario e inténtalo nuevamente';
				}
			break;

			default:
				redirect(base_url()."logged/profile");
			break;
		}
		$this->message = $message;
		$this->index();

	}

	public function laboral()
	{
		if($this->input->is_ajax_request()) 
		{
			#-- MODELS
			$this->load->model('Registro_model');
			$this->load->model("Registro_laboral_model");
			#-- LIBRERY
			$this->load->library('form_validation');

			#-- HELPER
			$this->load->helper('date');

			$accion = $this->input->post('accion');
			if($this->session->userdata('perfil') == 'usuario')
				$datos["idregistro"] = $this->input->post('idregistro');
			else
				$datos["idregistro"] = $this->session->userdata('idregistro'); 


			switch ($accion) {
				case 'update':
				case 'insert':
					
					$this->form_validation->set_rules('rubro', 'Rubro de Desempeño', 'trim|is_natural_no_zero|required');
					$this->form_validation->set_rules('empresa', 'Empresa', 'trim|required');
					$this->form_validation->set_rules('tipocargo', 'Tipo de Cargo', 'trim|is_natural_no_zero|required');
					$this->form_validation->set_rules('cargo', 'Cargo', 'trim|required');
					$this->form_validation->set_rules('email_laboral', 'Email Empresa', 'trim|valid_email');				
					if ($this->form_validation->run() === TRUE) {
						#redireccionar a l home logged
						#Puede que no necesariamente sea el logeado
						$datos["descripcion"] = $this->input->post("descripcion");
						$datos["idrubro"] = $this->input->post("rubro");
						$datos["idtipocargo"] = $this->input->post("tipocargo");					
						$datos["empresa"] = $this->input->post("empresa");
						$datos["cargo"] = $this->input->post("cargo");
						#$datos["direccion"] = $this->input->post("direccion");
						#$datos["idcomuna"] = $this->input->post("idcomuna");
						$datos["email"] = $this->input->post("email_laboral");
						#$datos["postal"] = $this->input->post("postal");					
						#$datos["celular"] = $this->input->post("celular");
						$datos["telefono"] = $this->input->post("telefono_laboral");
						$datos["fecha_inicio"] = date('Y-m-d', strtotime($this->input->post("fecha_inicio")));
						$datos["fecha_termino"] = date('Y-m-d', strtotime($this->input->post("fecha_termino")));
						$datos["descripcion"] = $this->input->post("descripcion");
						if($accion == 'insert')
						{
							if($idregistro_laboral = $this->Registro_laboral_model->insert($datos))
							{
								#ir a buscar todos nuevamente
								$return["status"] = 'success';
								$return["message"] = 'Los datos ingresados han sido actualizados existosamente';
								foreach($this->Registro_laboral_model->get('all', array('idregistro' => $datos["idregistro"])) as $row){
									$return["html"] .= $this->load->view('ajax/profile_laboral', array('row' => $row), true);
								}
							}
							else
							{
								$return["status"] = 'danger';
								$return["message"] = 'Ha ocurrido un eror al actualizar los datos ingresados';
								
							}
						}
						else
						{
							#validar si existe el registro y pertenece al usuario
							$idedit = $this->input->post('idedit');
							if(is_numeric($idedit) && !empty($idedit)){
								$datos["idregistro_laboral"] = $idedit;
								if($this->Registro_laboral_model->get(false, array('idregistro_laboral' => $datos["idregistro_laboral"], 'idregistro' => $datos["idregistro"])))
								{
									#ir a buscar todos nuevamente
									if($this->Registro_laboral_model->update($datos)){
										$return["status"] = 'success';
										$return["message"] = 'Los datos ingresados han sido actualizados existosamente';
										foreach($this->Registro_laboral_model->get('all', array('idregistro' => $datos["idregistro"])) as $row){
											$return["html"] .= $this->load->view('ajax/profile_laboral', array('row' => $row), true);
										}
									}
									else
									{
										$return["status"] = 'danger';
										$return["message"] = 'Ha ocurrido un error al actualizar los datos ingresados';	
									}
								}
								else
								{
									$return["status"] = 'danger';
									$return["message"] = 'El registro a modificar no existe o no tienes permisos';
								}
							}else{
								$return["status"] = 'danger';
								$return["message"] = 'No es posible identificar el registro a actualizar';
							}
						}
					}else{
						$return["status"] = 'danger';
						$return["message"] = validation_errors();
					}

				break;

				case "edit":
					$this->form_validation->set_rules('idregistro_laboral[]', 'para realizar la edición', 'trim|required');
					if ($this->form_validation->run() === TRUE) {
						$idregistro_laboral = $this->input->post("idregistro_laboral[]");
						$datos["idregistro_laboral"] = $idregistro_laboral[0];
						if($this->Registro_laboral_model->get(false, $datos))
						{
							#ir a buscar la vista
							$return["data"]["idregistro_laboral"] = $this->Registro_laboral_model->getIdRegistroLaboral();
							$return["data"]["idregistro"] = $this->Registro_laboral_model->getIdRegistro();
							$return["data"]["idrubro"] = $this->Registro_laboral_model->getIdRubro();
							$return["data"]["empresa"] = $this->Registro_laboral_model->getEmpresa();
							$return["data"]["idtipocargo"] = $this->Registro_laboral_model->getIdTipoCargo();
							$return["data"]["cargo"] = $this->Registro_laboral_model->getCargo();
							$return["data"]["email_laboral"] = $this->Registro_laboral_model->getEmail();
							$return["data"]["telefono_laboral"] = $this->Registro_laboral_model->getTelefono();

							if($this->Registro_laboral_model->getFechaInicio() != '')
								$return["data"]["fecha_inicio"] = date('d-m-Y', strtotime($this->Registro_laboral_model->getFechaInicio()));
							if($this->Registro_laboral_model->getFechaTermino())
								$return["data"]["fecha_termino"] = date('d-m-Y', strtotime($this->Registro_laboral_model->getFechaTermino()));
							$return["data"]["descripcion"] = $this->Registro_laboral_model->getDescripcion();
							$return["data"]["accion"] = "update";
							$return["data"]["grupo"] = "laboral";

							$return["status"] = 'success';
							$return["message"] = 'Datos obtenidos correctamente';
						}
						else
						{
							$return["status"] = 'danger';
							$return["message"] = 'El registro que desea modificar no existe, no pertenece a este perfil.';
							
						}
					}else{
						$return["status"] = 'danger';
						$return["message"] = validation_errors();
					}

				break;

				case "delete":
					$this->form_validation->set_rules('idregistro_laboral[]', 'para realizar la edición', 'trim|required');
					if ($this->form_validation->run() === TRUE) {
						#Eliminar
						$cont = 0;
						foreach($this->input->post('idregistro_laboral[]') as $id){
							$datos["idregistro_laboral"] = $id;
							if($this->Registro_laboral_model->get('boolean', $datos))
								if($this->Registro_laboral_model->delete($id))
									$cont++;
						}
						if($cont > 0){
							$return["status"] = 'success';
							$return["message"] = 'El registro seleccionado ha sido eliminado exitosamente';
							foreach($this->Registro_laboral_model->get('all', array('idregistro' => $datos["idregistro"])) as $row){
								$return["html"] .= $this->load->view('ajax/profile_laboral', array('row' => $row), true);
							}
						}
						else
						{
							$return["status"] = 'danger';
							$return["message"] = 'No ha sido posible eliminar el o los registros seleccionados';
						}
					}else{
						$return["status"] = 'danger';
						$return["message"] = validation_errors();
					}
				break;

				default:
					$return["status"] = 'danger';
					$return["message"] = 'No ha sido posible realizar la acción indicada';	
				break;
			}

			echo json_encode($return);
		}
		else
			$this->index();
	}


	public function academico()
	{
		if($this->input->is_ajax_request()) 
		{
			#-- MODELS
			$this->load->model('Registro_model');
			$this->load->model("Registro_academico_model");
			$this->load->model('Institucion_model');
			$this->load->model('Carrera_model');
			#-- LIBRERY
			$this->load->library('form_validation');

			#-- HELPER
			$this->load->helper('date');

			$accion = $this->input->post('accion');
			if($this->session->userdata('perfil') == 'usuario')
				$datos["idregistro"] = $this->input->post('idregistro');
			else
				$datos["idregistro"] = $this->session->userdata('idregistro'); 


			switch ($accion) {
				case 'update':
				case 'insert':
					
					$this->form_validation->set_rules('tipoinstitucion', 'Tipo de institución', 'trim|required|is_natural_no_zero');
					$this->form_validation->set_rules('institucion', 'Institución', 'trim|required|is_natural_no_zero');
					$this->form_validation->set_rules('fecha_inicio', 'Periodo de estudios', 'trim|required');
					$this->form_validation->set_rules('fecha_termino', 'Periodo de estudios', 'trim|required');
								
					if ($this->form_validation->run() === TRUE) {
						#Puede que no necesariamente sea el logeado
						#$datos["tipoinstitucion"] = $this->input->post("tipoinstitucion");
						$datos["idinstitucion"] = $this->input->post("institucion");
						#$datos["tipocarrera"] = $this->input->post("tipocarrera");					
						$datos["idcarrera"] = $this->input->post("carrera");
						$datos["idcampus"] = $this->input->post("campus");
						$datos["fecha_inicio"] = date('Y-m-d', strtotime($this->input->post("fecha_inicio")));
						$datos["fecha_termino"] = date('Y-m-d', strtotime($this->input->post("fecha_termino")));
						$datos["descripcion"] = $this->input->post("descripcion");
						if($accion == 'insert')
						{
							if($idregistro_academico = $this->Registro_academico_model->insert($datos))
							{
								#ir a buscar todos nuevamente
								$return["status"] = 'success';
								$return["message"] = 'Los datos ingresados han sido actualizados existosamente';
								foreach($this->Registro_academico_model->get('all', array('idregistro' => $datos["idregistro"])) as $row){
									$data["registro_academico"] = $row;
									$data["institucion"] = $this->Institucion_model->get('object', array('idinstitucion' => $row->getIdInstitucion()));
									$return["html"] .= $this->load->view('ajax/profile_academico', $data, true);
								}
							}
							else
							{
								$return["status"] = 'danger';
								$return["message"] = 'Ha ocurrido un eror al actualizar los datos ingresados';
								
							}
						}
						else
						{
							#validar si existe el registro y pertenece al usuario
							$idedit = $this->input->post('idedit');
							if(is_numeric($idedit) && !empty($idedit)){
								$datos["idregistro_academico"] = $idedit;
								if($this->Registro_academico_model->get(false, array('idregistro_academico' => $datos["idregistro_academico"], 'idregistro' => $datos["idregistro"])))
								{
									#ir a buscar todos nuevamente
									if($this->Registro_academico_model->update($datos)){
										$return["status"] = 'success';
										$return["message"] = 'Los datos ingresados han sido actualizados existosamente';
										foreach($this->Registro_academico_model->get('all', array('idregistro' => $datos["idregistro"])) as $row){
											$data["registro_academico"] = $row;
											$data["institucion"] = $this->Institucion_model->get('object', array('idinstitucion' => $row->getIdInstitucion()));
											$return["html"] .= $this->load->view('ajax/profile_academico', $data, true);
										}
									}
									else
									{
										$return["status"] = 'danger';
										$return["message"] = 'Ha ocurrido un error al actualizar los datos ingresados';	
									}
								}
								else
								{
									$return["status"] = 'danger';
									$return["message"] = 'El registro a modificar no existe o no tienes permisos';
								}
							}else{
								$return["status"] = 'danger';
								$return["message"] = 'No es posible identificar el registro a actualizar';
							}
						}
					}else{
						$return["status"] = 'danger';
						$return["message"] = validation_errors();
					}

				break;

				case "edit":
					$this->form_validation->set_rules('idregistro_academico[]', 'para realizar la edición', 'trim|required');
					if ($this->form_validation->run() === TRUE) {

						$idregistro_academico = $this->input->post("idregistro_academico[]");
						$datos["idregistro_academico"] = $idregistro_academico[0];
						if($this->Registro_academico_model->get(false, $datos))
						{
							#ir a buscar la vista
							$return["data"]["idregistro_academico"] = $this->Registro_academico_model->getIdRegistroAcademico();
							$return["data"]["idregistro"] = $this->Registro_academico_model->getIdRegistro();
							$return["data"]["idcarrera"] = $this->Registro_academico_model->getIdCarrera();
							$return["data"]["idinstitucion"] = $this->Registro_academico_model->getIdInstitucion();
							$return["data"]["idcampus"] = $this->Registro_academico_model->getIdCampus();

							#tipoinstitucion, tipocarrera
							if($this->Carrera_model->get(false, array('idcarrera' => $this->Registro_academico_model->getIdCarrera()))){
								$return["data"]["idtipocarrera"] = $this->Carrera_model->getIdTipoCarrera();
							}
							if($this->Institucion_model->get(false, array('idinstitucion' => $this->Registro_academico_model->getIdInstitucion()))){
								$return["data"]["idtipoinstitucion"] = $this->Institucion_model->getIdTipoInstitucion();
								$return["data"]["institucion"] = $this->Institucion_model->getNombre();
							}


							if($this->Registro_academico_model->getFechaInicio() != '')
								$return["data"]["fecha_inicio"] = date('d-m-Y', strtotime($this->Registro_academico_model->getFechaInicio()));
							if($this->Registro_academico_model->getFechaTermino())
								$return["data"]["fecha_termino"] = date('d-m-Y', strtotime($this->Registro_academico_model->getFechaTermino()));
							$return["data"]["descripcion"] = $this->Registro_academico_model->getDescripcion();
							$return["data"]["accion"] = "update";
							$return["data"]["grupo"] = "academico";

							$return["status"] = 'success';
							$return["message"] = 'Datos obtenidos correctamente';
						}
						else
						{
							$return["status"] = 'danger';
							$return["message"] = 'El registro que desea modificar no existe, no pertenece a este perfil.';
							
						}
					}else{
						$return["status"] = 'danger';
						$return["message"] = validation_errors();
					}

				break;

				case "delete":
					$this->form_validation->set_rules('idregistro_academico[]', 'para realizar la edición', 'trim|required');
					if ($this->form_validation->run() === TRUE) {
						#Eliminar
						$cont = 0;
						foreach($this->input->post('idregistro_academico[]') as $id){
							$datos["idregistro_academico"] = $id;
							if($this->Registro_academico_model->get('boolean', $datos))
								if($this->Registro_academico_model->delete($id))
									$cont++;
						}
						if($cont > 0){
							$return["status"] = 'success';
							$return["message"] = 'El registro seleccionado ha sido eliminado exitosamente';
							foreach($this->Registro_academico_model->get('all', array('idregistro' => $datos["idregistro"])) as $row){
								$data["registro_academico"] = $row;
								$data["institucion"] = $this->Institucion_model->get('object', array('idinstitucion' => $row->getIdInstitucion()));
								$return["html"] .= $this->load->view('ajax/profile_academico', $data, true);
							}
						}
						else
						{
							$return["status"] = 'danger';
							$return["message"] = 'No ha sido posible eliminar el o los registros seleccionados';
						}
					}else{
						$return["status"] = 'danger';
						$return["message"] = validation_errors();
					}
				break;

				default:
					$return["status"] = 'danger';
					$return["message"] = 'No ha sido posible realizar la acción indicada';	
				break;
			}

			echo json_encode($return);
		}
		else
			$this->index();

	}


}