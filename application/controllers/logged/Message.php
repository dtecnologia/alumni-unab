<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends CI_Controller {

        private $data = array();
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
        public function __construct(){
            parent::__construct();
            #Validar logged
            if(!$this->session->userdata('logged'))
                redirect(base_url(), 'header');
        }
        
	public function index() 
	{
            #Declarar recursos
            $this->load->model('Contenido_model');
            $this->load->model('Usuario_model');
            $this->load->model('Registro_model');
            $custom = array('limit' => 20, 'pagina' => 1);
            
            #Obtener filtros de búsqueda
            $filtros = NULL;
            
            #Obtener registros ingresados
            $this->data["total_solicitudes"] = $this->Contenido_model->get('count', $filtros);
            $this->data["total_paginas"] = ceil($this->data["total_solicitudes"] / $custom["limit"]);
            $this->data["contenidos"] = array();
            if($contenidos = $this->Contenido_model->get('all', $filtros, $custom)){
                    foreach($contenidos as $contenido)
                    {
                            $temp["contenido"] = $contenido;
                            $temp["emisor"] = ($contenido->getIdUsuarioIng()) ? $this->Usuario_model->get('object', array('idusuario' => $contenido->getIdUsuarioIng())) : $this->Registro_model->get('object', array('idregistro' => $contenido->getIdRegistroIng()));
                            $this->data["contenidos"][] = $temp;	
                    }
            }
            
            #Llamar a la vista
            $data_frame["body"] = $this->load->view('logged/message', $this->data, true);
            $this->load->view('logged/frame', $data_frame); 
	}
        
        
        public function form($id = NULL)
        {   
            #Definir la accion del form
            $this->data[accion] = (empty($id)) ? "insert" : "update";
            
            #Cargar datos si viene el id
            if(!empty($id)){
                
                $this->data[emisor] = array();
            }else{
                $this->data[emisor][tipo] = ($this->session->userdata('perfil') == 'registro') ? "Registro Alumni" : "Equipo Alumni";
                $this->data[emisor][nombre] = $this->session->userdata('nombre');
                $this->data[emisor][email] = $this->session->userdata('email');
            }
            
            #Llamar a la vista
            $data_frame["body"] = $this->load->view('logged/message-form', $this->data, true);
            $this->load->view('logged/frame', $data_frame); 
        }
        
        public function submit(){
            #Validar datos de sistema
            $idregistro = ($this->session->userdata('perfil') == 'registro') ? $this->session->userdata('idregistro') : NULL;
            $idusuario = ($this->session->userdata('perfil') == 'usuario') ? $this->session->userdata('idusuario') : NULL;   
            
            #Validar formulario
            $this->load->library('form_validation');
            $this->form_validation->set_rules('accion', 'Acción', 'trim|required', array('required' => 'El formulario no puede ejecutarse debido a una ausencia de parámetros.'));
            $this->form_validation->set_rules('titulo', 'Asunto', 'trim|required');
            $this->form_validation->set_rules('bajada', 'Descripcion breve', 'trim|required');
            $this->form_validation->set_rules('cuerpo', 'Descripcion completa', 'trim|required');
            if ($this->form_validation->run() === TRUE){
                $insert[titulo] = $this->input->post('titulo');
                $insert[bajada] = $this->input->post('bajada');
                $insert[cuerpo] = $this->input->post('cuerpo');
                $insert[idregistro_ing] = $idregistro;
                $insert[idusuario_ing] = $idusuario;
                $this->load->model('Contenido_model');
                if($idcontenido = $this->Contenido_model->insert($insert)){
                    $this->data[message][message] = "El contenido ha sido ingresado exitosamente. ID: $idcontenido";
                    $this->data[message][status] = "sucess";
                    $this->index();
                }else{
                   $this->data[message][message] = "Ha ocurrido un error inesperado al insertar el contenido, favor inténtelo nuevamente.";
                   $this->data[message][status] = "danger";
                   $this->index(); 
                }
            }else{
                $errores = validation_errors('<i>', '</i>');
                if(!empty($errores)){
                    $this->data[message][message] = "Ha ocurrido un error al enviar el formulario. ".$errores;
                    $this->data[message][status] = "danger";
                    $this->index();
                }else{
                    redirect(base_url().'logged/message');
                }
            }
            
        } 
}

