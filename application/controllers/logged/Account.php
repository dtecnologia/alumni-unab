<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() 
	{
		$data_frame["body"] = $this->load->view('logged/account', '', true);
		$this->load->view('logged/frame', $data_frame); 
	}

	public function change_password()
	{
		$data_account["html"] = "";
		$data_frame["body"] = $this->load->view('logged/account', $data_account, true);
		$this->load->view('logged/frame', $data_frame); 
	}

}
