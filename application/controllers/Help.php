<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Help extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() 
	{
		#Envío del formulario
		$this->load->helper(array('rut', 'form'));
		$this->load->library('form_validation');

        $this->form_validation->set_rules('tipo_solicitud', 'Asunto', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('rut', 'Rut', 'trim|required|isRut');
        $this->form_validation->set_rules('nombre', 'Nombre completo', 'trim|required');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
        $this->form_validation->set_rules('descripcion', 'Descripción', 'trim|required');
		if ($this->form_validation->run() === TRUE) {
			#insertar la solicitud
			$message = $this->_insert();
			$this->_display($message);
		}
		else{
			$this->_display();
		}
	}


	function _display($message = NULL){
		$this->load->model('Tipo_solicitud_model');
		$data["message"] = $message;
		$data["tipo_solicitudes"] = $this->Tipo_solicitud_model->get('all');

		$data_frame["body"] = $this->load->view('unlogged/help', $data, true);
		$this->load->view('unlogged/frame', $data_frame);
	}

	function _insert(){
		$this->load->model('Solicitud_model');
		$this->load->model('Registro_model');
		$rut = isRut($this->input->post('rut'));

		if($this->Registro_model->get(false, array('rut' => limpiarRut($rut)))){
			$solicitud["idregistro"] = $this->Registro_model->getIdRegistro();
		}
		$solicitud["idtipo_solicitud"] = $this->input->post('tipo_solicitud');
		$solicitud["nombre"] = "Solicitud por ".$this->input->post('nombre'). " (". $rut .")";
		$solicitud["descripcion"] = $this->input->post('descripcion');
		$solicitud["email"] = $this->input->post('email');
		if($idsolicitud = $this->Solicitud_model->insert($solicitud)){
			#insertar el adjunto
			$message["status"] = "success";
			$this->load->library('folder');
			if(!empty($_FILES)){
				if($ruta = $this->folder->userFolder($solicitud["idregistro"], 'request')){
					$config['upload_path'] = $ruta;
					$config['allowed_types'] = 'gif|jpg|png|pdf|doc|docx|ppt|pptx';
					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload())
					{
						$message["message"] = "Solicitud ingresada, pero el archivo no se ha adjuntado ".$this->upload->display_errors();
					}
					else
					{
						#insertar adjunto en BD
						$this->load->model("Solicitud_adjunto_model");

						$data = $this->upload->data();
						$data_adjunto["idsolicitud"] = $idsolicitud;
						$data_adjunto["nombre"] = $data["file_name"];
						$data_adjunto["type"] = $data["file_type"];
						$data_adjunto["path"] = $data["file_path"];
						if(!$idadjunto = $this->Solicitud_adjunto_model->insert($data_adjunto))
						{
							$message["message"] = "La solicitud ha sido ingresada exitosamente, pero ha ocurrido un error al insertar el archivo adjunto";
						}
						else
						{
							$message["message"] = "La solicitud y su archivo adjunto han sido ingresados exitosamente";
						}
						
					}
				}
				else
					$message["message"] = "La solicitud ha sido ingresada exitosamente, pero hubo un error en la ruta del adjunto";
				
			}
			else
			{
				$message["message"] = "La solicitud ha sido ingresada exitosamente";
			}
		}
		else
		{
			$message["status"] = "danger";
			$message["message"] = "Ha ocurrido un error al ingresar la solicitud, favor pruebelo más tarde";
		}

		return $message;
	}

}
