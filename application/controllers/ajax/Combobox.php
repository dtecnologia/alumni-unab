<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Combobox extends CI_Controller {

	/* Obtener instituciones a través de la referencia id tipoinstitucion */
	public function institucion($idtipoinstitucion) 
	{
		$return["html"] = '';
		$return["status"] = "danger";

		if(is_numeric($idtipoinstitucion)){
			$this->load->model("Institucion_model");
			if($data["instituciones"] = $this->Institucion_model->get('all', array('idtipo_institucion' => $idtipoinstitucion)))
			{
				$return["html"] = $this->load->view("ajax/combo_instituciones", $data, true);
				$return["status"] = "success";	
			}
			else
			{
				$return["message"] = "Sin registros asignados, seleccione otra opción";
			}
		}
		else{
			$return["message"] = "Código identificador erróneo, vuelva a intentarlo";
		}

		echo json_encode($return);
	}


	public function carrera($idtipocarrera) 
	{
		$return["html"] = '';
		$return["status"] = "danger";

		if(is_numeric($idtipocarrera)){
			$this->load->model("Carrera_model");
			if($data["carreras"] = $this->Carrera_model->get('all', array('idtipocarrera' => $idtipocarrera)))
			{
				$return["html"] = $this->load->view("ajax/combo_carreras", $data, true);
				$return["status"] = "success";	
			}
			else
			{
				$return["message"] = "Sin registros asignados, seleccione otra opción";
			}
		}
		else{
			$return["message"] = "Código identificador erróneo, vuelva a intentarlo";
		}

		echo json_encode($return);
	}


	public function region($idpais = NULL){
		$return["status"] = "success";
		$data = array();

		if(is_numeric($idpais)){
			$this->load->model("Region_model");
			if($data["regiones"] = $this->Region_model->get('all', array('idpais' => $idpais)))
			{
				$return["message"] = "Ejecución realizada con éxito";
			}
			else
			{
				$return["message"] = "Sin registros asignados, seleccione otra opción";
			}
		}
		else{
			$return["message"] = "Código identificador erróneo, vuelva a intentarlo";
		}
		$return["html"] = $this->load->view("ajax/combo_region", $data, true);

		echo json_encode($return);
	}


	public function ciudad($idregion = NULL){
		$data = array();
		$return["status"] = "success";

		if(is_numeric($idregion)){
			$this->load->model("Ciudad_model");
			if($data["ciudades"] = $this->Ciudad_model->get('all', array('idregion' => $idregion)))
			{
				$return["message"] = "Ejecución realizada con éxito";
			}
			else
			{
				$return["message"] = "Sin registros asignados, seleccione otra opción";
			}
		}
		else{
			$return["message"] = "Código identificador erróneo, vuelva a intentarlo";
		}
		$return["html"] = $this->load->view("ajax/combo_ciudad", $data, true);

		echo json_encode($return);
	}


	public function comuna($idciudad = NULL){
		$data = array();
		$return["status"] = "success";

		if(is_numeric($idciudad)){
			$this->load->model("Comuna_model");
			if($data["comunas"] = $this->Comuna_model->get('all', array('idciudad' => $idciudad)))
			{
				$return["message"] = "Ejecución realizada con éxito";
			}
			else
			{
				$return["message"] = "Sin registros asignados, seleccione otra opción";
			}
		}
		else{
			$return["message"] = "Código identificador erróneo, vuelva a intentarlo";
		}
		
		$return["html"] = $this->load->view("ajax/combo_comuna", $data, true);
		echo json_encode($return);
	}
        
        public function idcampus($idcarrera){
            $data = array();
            $return["status"] = "success";
            if(is_numeric($idcarrera)){
                $this->load->model("Campus_carrera_model");
                $camps = $this->Campus_carrera_model->get('all', array('idcarrera' => $idcarrera));
                if(!empty($camps)) {
                    $_campus = array();
                    foreach ($camps as $campus) {
                        array_push($_campus, $campus->getIdCampus());
                    }
                    
                    $this->load->model("Campus_model");
                    $data["campus"] = $this->Campus_model->get('all', array('idcampus' => $_campus));
                    $return["message"] = "Ejecución realizada con éxito";
                } else {
                    $return["message"] = "Sin registros asignados, seleccione otra opción";
                } 
            } else {
                $return["message"] = "Código identificador erróneo, vuelva a intentarlo";
            }
            $return["html"] = $this->load->view("ajax/combo_campus", $data, true);
            echo json_encode($return);
            
        }

}
