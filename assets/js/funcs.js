function requestMessage(action){
	$.ajax({
		method: 'post',
		url: '/logged/request/message/' + action,
		dataType: 'json',
		data: $("#request-message-form").serialize(),
		success: function(data){
			if(data.status == 'success'){
				//mostrar el html
				$("#request-message-message").hide();
				$("#request-message-body").append(data.html);
				$("#descripcion").val('');
			}else{
				//mostrar el error
				$("#request-message-message").html(data.message);
				$("#request-message-message").addClass('text-' + data.status);
				$("#request-message-message").show();
			}
		}
	});
}

function combobox(origen, destino){
	var origen_val = $("#"+origen).val();
	$("#"+destino).attr('readonly', 'readonly');
	//if(destino == 'institucion')
	//	mostrar_carreras(origen_val);
	$.ajax({
		method: 'post',
		url: '/ajax/combobox/'+ destino +'/' + origen_val,
		dataType: 'json',
		data: null,
		success: function(data){
			$("#"+origen).parent().find('small').remove(); 
			$("#"+destino).html(data.html);
			if(data.status != 'success'){
				//mostrar el html
				$("#"+origen).after("<small class='text-" + data.status + "'>" + data.message + "</small>");
			}
		},
		complete: function(){
			$("#"+destino).removeAttr('readonly');
			if(origen == 'region')
				setTimeout(combobox('ciudad', 'comuna'),2000);
                        if(origen == idtipocarrera)
                                setTimeout(combobox('carrera', 'campus'),2000);
		}
	});
	
}

function mostrar_carreras(){
	var idtipoinstitucion = $('#tipoinstitucion').val();
	if(idtipoinstitucion == 1 || idtipoinstitucion == 2){
		$("#divtipocarrera").slideDown();	
		$("#divcarrera").slideDown();	
	}else{
		$("#divtipocarrera").slideUp();	
		$("#divcarrera").slideUp();	
	}
}


function mostrar_campus(){
	var idinstitucion = $("#institucion").val();
	if(idinstitucion == 1){
		$("#divcampus").slideDown();	
	}else{
		$("#divcampus").slideUp();	
	}
}


function actionPerfil(metodo, base_url){
	/* Reiniciar mensajes */
	$("#alert-"+metodo).fadeOut(400, 'swing', function(){
		$(this).removeClass('alert-success');
		$(this).removeClass('alert-danger');
		$(this).removeClass('alert-warning');	
	});
	
	/* Fin de mensajes */

	$.ajax({
		method: 'post',
		url: base_url+'logged/profile/'+ metodo,
		dataType: 'json',
		data: $("#form-"+metodo).serialize(),
		success: function(data){
			
			if(data.status == 'success'){
				
				if(data.html != undefined){
					$("#table-content-"+metodo).fadeIn();
					$("#no-content-"+metodo).fadeOut().remove();
					$("#content-" + metodo).html(data.html);
					cancelEdit(metodo);
				}
				if(data.data.grupo != undefined){

					if(data.data.grupo == 'laboral'){
						$("#rubro").val(data.data.idrubro);
						$("#empresa").val(data.data.empresa);
						$("#tipocargo").val(data.data.idtipocargo);
						$("#cargo").val(data.data.cargo);
						$("#email_laboral").val(data.data.email_laboral);
						$("#telefono_laboral").val(data.data.telefono_laboral);
						$("#fecha_"+data.data.grupo+"_inicio").val(data.data.fecha_inicio);
						$("#fecha_"+data.data.grupo+"_termino").val(data.data.fecha_termino);
						$("#descripcion_"+data.data.grupo).val(data.data.descripcion);

						$("#idregistro-"+data.data.grupo).val(data.data.idregistro);
						$("#idedit-"+data.data.grupo).val(data.data.idregistro_laboral);
						$("#accion-"+data.data.grupo).val(data.data.accion);

						$("#subtitle-"+data.data.grupo).html('Editando "'+data.data.empresa+'" ');
						$("#btn-"+data.data.grupo+"-submit").html('Actualizar datos');
						$("#btn-"+data.data.grupo+"-reset").hide();
						$("#btn-"+data.data.grupo+"-cancelar").show();
					}

					if(data.data.grupo == 'academico'){
						$("#tipoinstitucion").val(data.data.idtipoinstitucion);
						$("#tipocarrera").val(data.data.idtipocarrera);
						combobox('tipoinstitucion', 'institucion');
						combobox('tipocarrera', 'carrera');
						
						
						$("#campus").val(data.data.idcampus);
						$("#fecha_"+data.data.grupo+"_inicio").val(data.data.fecha_inicio);
						$("#fecha_"+data.data.grupo+"_termino").val(data.data.fecha_termino);
						$("#descripcion_"+data.data.grupo).val(data.data.descripcion);

						$("#idregistro-"+data.data.grupo).val(data.data.idregistro);
						$("#idedit-"+data.data.grupo).val(data.data.idregistro_academico);
						$("#accion-"+data.data.grupo).val(data.data.accion);

						$("#subtitle-"+data.data.grupo).html('Editando "'+data.data.institucion+'" ');
						$("#btn-"+data.data.grupo+"-submit").html('Actualizar datos');
						$("#btn-"+data.data.grupo+"-reset").hide();
						$("#btn-"+data.data.grupo+"-cancelar").fadeIn(550, function(){
							$("#institucion").val(data.data.idinstitucion);
							$("#carrera").val(data.data.idcarrera);	
							mostrar_carreras();
							mostrar_campus();
						});
						
					}

					if(data.data.accion == 'update'){
						$("#footer-"+data.data.grupo).find('button').attr('disabled', 'disabled');
					}
				}else{
					$(".panel-footer").find('button').removeAttr('disabled');
				}
			}
			/* Desplegar mensajes */
			$("#message-"+metodo).html(data.message);	
			$("#alert-"+metodo).addClass("alert-"+data.status).fadeIn();

		}
	});
	
}

function cancelEdit(grupo){
	document.getElementById("form-"+grupo).reset();
	$("#subtitle-"+grupo).html('');
	$("#btn-"+grupo+"-submit").html('Guardar datos');
	$("#btn-"+grupo+"-reset").show();
	$("#btn-"+grupo+"-cancelar").hide();
	$("#footer-"+grupo).fadeOut();
	$("#idedit-"+grupo).val('');
	$("#accion-"+grupo).val('insert');
	$(".panel-footer").find('button').removeAttr('disabled');

	if(grupo == 'academico'){
		mostrar_campus();
		mostrar_carreras();
	}
}

function checked(grupo, id){
	if($("#"+grupo+"-"+id).prop("checked"))
		$("#"+grupo+"-"+id).prop("checked", "");
	else
		$("#"+grupo+"-"+id).prop("checked", "checked");

	var activar = 0;
	$("input[data-name=idregistro_"+grupo+"]:checked").each(function(){
		if($(this).prop("checked"))
			activar++;
	});
	if(activar > 0){
		$("#footer-"+grupo).fadeIn();
		if(activar > 1)
			$("#btn-"+grupo+"-edit").hide();
		else
			$("#btn-"+grupo+"-edit").fadeIn();
	}
	else
		$("#footer-"+grupo).fadeOut();	
}